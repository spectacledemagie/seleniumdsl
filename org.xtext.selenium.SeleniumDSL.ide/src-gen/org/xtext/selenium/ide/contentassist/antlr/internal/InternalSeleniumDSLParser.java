package org.xtext.selenium.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import org.xtext.selenium.services.SeleniumDSLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalSeleniumDSLParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'close'", "'chrome'", "'firefox'", "'opera'", "'button'", "'link'", "'image'", "'checkbox'", "'input'", "'div'", "'span'", "'label'", "'text'", "'contains'", "'equals'", "'value'", "'check'", "'uncheck'", "'open'", "'get'", "'where'", "'select'", "'option'", "'in'", "'combo'", "'click'", "'store'", "'of'", "'set'", "'to'", "'isElementPresent'", "'count'", "'define'", "'('", "')'", "';'", "'assert'", "'all'", "'not'"
    };
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int RULE_ID=5;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalSeleniumDSLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalSeleniumDSLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalSeleniumDSLParser.tokenNames; }
    public String getGrammarFileName() { return "InternalSeleniumDSL.g"; }


    	private SeleniumDSLGrammarAccess grammarAccess;

    	public void setGrammarAccess(SeleniumDSLGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleSeleniumProgram"
    // InternalSeleniumDSL.g:53:1: entryRuleSeleniumProgram : ruleSeleniumProgram EOF ;
    public final void entryRuleSeleniumProgram() throws RecognitionException {
        try {
            // InternalSeleniumDSL.g:54:1: ( ruleSeleniumProgram EOF )
            // InternalSeleniumDSL.g:55:1: ruleSeleniumProgram EOF
            {
             before(grammarAccess.getSeleniumProgramRule()); 
            pushFollow(FOLLOW_1);
            ruleSeleniumProgram();

            state._fsp--;

             after(grammarAccess.getSeleniumProgramRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSeleniumProgram"


    // $ANTLR start "ruleSeleniumProgram"
    // InternalSeleniumDSL.g:62:1: ruleSeleniumProgram : ( ( rule__SeleniumProgram__Group__0 ) ) ;
    public final void ruleSeleniumProgram() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:66:2: ( ( ( rule__SeleniumProgram__Group__0 ) ) )
            // InternalSeleniumDSL.g:67:2: ( ( rule__SeleniumProgram__Group__0 ) )
            {
            // InternalSeleniumDSL.g:67:2: ( ( rule__SeleniumProgram__Group__0 ) )
            // InternalSeleniumDSL.g:68:3: ( rule__SeleniumProgram__Group__0 )
            {
             before(grammarAccess.getSeleniumProgramAccess().getGroup()); 
            // InternalSeleniumDSL.g:69:3: ( rule__SeleniumProgram__Group__0 )
            // InternalSeleniumDSL.g:69:4: rule__SeleniumProgram__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__SeleniumProgram__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSeleniumProgramAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSeleniumProgram"


    // $ANTLR start "entryRuleBrowser"
    // InternalSeleniumDSL.g:78:1: entryRuleBrowser : ruleBrowser EOF ;
    public final void entryRuleBrowser() throws RecognitionException {
        try {
            // InternalSeleniumDSL.g:79:1: ( ruleBrowser EOF )
            // InternalSeleniumDSL.g:80:1: ruleBrowser EOF
            {
             before(grammarAccess.getBrowserRule()); 
            pushFollow(FOLLOW_1);
            ruleBrowser();

            state._fsp--;

             after(grammarAccess.getBrowserRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBrowser"


    // $ANTLR start "ruleBrowser"
    // InternalSeleniumDSL.g:87:1: ruleBrowser : ( ( rule__Browser__Alternatives ) ) ;
    public final void ruleBrowser() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:91:2: ( ( ( rule__Browser__Alternatives ) ) )
            // InternalSeleniumDSL.g:92:2: ( ( rule__Browser__Alternatives ) )
            {
            // InternalSeleniumDSL.g:92:2: ( ( rule__Browser__Alternatives ) )
            // InternalSeleniumDSL.g:93:3: ( rule__Browser__Alternatives )
            {
             before(grammarAccess.getBrowserAccess().getAlternatives()); 
            // InternalSeleniumDSL.g:94:3: ( rule__Browser__Alternatives )
            // InternalSeleniumDSL.g:94:4: rule__Browser__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Browser__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getBrowserAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBrowser"


    // $ANTLR start "entryRuleOpen"
    // InternalSeleniumDSL.g:103:1: entryRuleOpen : ruleOpen EOF ;
    public final void entryRuleOpen() throws RecognitionException {
        try {
            // InternalSeleniumDSL.g:104:1: ( ruleOpen EOF )
            // InternalSeleniumDSL.g:105:1: ruleOpen EOF
            {
             before(grammarAccess.getOpenRule()); 
            pushFollow(FOLLOW_1);
            ruleOpen();

            state._fsp--;

             after(grammarAccess.getOpenRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOpen"


    // $ANTLR start "ruleOpen"
    // InternalSeleniumDSL.g:112:1: ruleOpen : ( ( rule__Open__Group__0 ) ) ;
    public final void ruleOpen() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:116:2: ( ( ( rule__Open__Group__0 ) ) )
            // InternalSeleniumDSL.g:117:2: ( ( rule__Open__Group__0 ) )
            {
            // InternalSeleniumDSL.g:117:2: ( ( rule__Open__Group__0 ) )
            // InternalSeleniumDSL.g:118:3: ( rule__Open__Group__0 )
            {
             before(grammarAccess.getOpenAccess().getGroup()); 
            // InternalSeleniumDSL.g:119:3: ( rule__Open__Group__0 )
            // InternalSeleniumDSL.g:119:4: rule__Open__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Open__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOpenAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOpen"


    // $ANTLR start "entryRuleClose"
    // InternalSeleniumDSL.g:128:1: entryRuleClose : ruleClose EOF ;
    public final void entryRuleClose() throws RecognitionException {
        try {
            // InternalSeleniumDSL.g:129:1: ( ruleClose EOF )
            // InternalSeleniumDSL.g:130:1: ruleClose EOF
            {
             before(grammarAccess.getCloseRule()); 
            pushFollow(FOLLOW_1);
            ruleClose();

            state._fsp--;

             after(grammarAccess.getCloseRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleClose"


    // $ANTLR start "ruleClose"
    // InternalSeleniumDSL.g:137:1: ruleClose : ( 'close' ) ;
    public final void ruleClose() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:141:2: ( ( 'close' ) )
            // InternalSeleniumDSL.g:142:2: ( 'close' )
            {
            // InternalSeleniumDSL.g:142:2: ( 'close' )
            // InternalSeleniumDSL.g:143:3: 'close'
            {
             before(grammarAccess.getCloseAccess().getCloseKeyword()); 
            match(input,11,FOLLOW_2); 
             after(grammarAccess.getCloseAccess().getCloseKeyword()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleClose"


    // $ANTLR start "entryRuleSeleniumInstruction"
    // InternalSeleniumDSL.g:153:1: entryRuleSeleniumInstruction : ruleSeleniumInstruction EOF ;
    public final void entryRuleSeleniumInstruction() throws RecognitionException {
        try {
            // InternalSeleniumDSL.g:154:1: ( ruleSeleniumInstruction EOF )
            // InternalSeleniumDSL.g:155:1: ruleSeleniumInstruction EOF
            {
             before(grammarAccess.getSeleniumInstructionRule()); 
            pushFollow(FOLLOW_1);
            ruleSeleniumInstruction();

            state._fsp--;

             after(grammarAccess.getSeleniumInstructionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSeleniumInstruction"


    // $ANTLR start "ruleSeleniumInstruction"
    // InternalSeleniumDSL.g:162:1: ruleSeleniumInstruction : ( ( rule__SeleniumInstruction__Alternatives ) ) ;
    public final void ruleSeleniumInstruction() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:166:2: ( ( ( rule__SeleniumInstruction__Alternatives ) ) )
            // InternalSeleniumDSL.g:167:2: ( ( rule__SeleniumInstruction__Alternatives ) )
            {
            // InternalSeleniumDSL.g:167:2: ( ( rule__SeleniumInstruction__Alternatives ) )
            // InternalSeleniumDSL.g:168:3: ( rule__SeleniumInstruction__Alternatives )
            {
             before(grammarAccess.getSeleniumInstructionAccess().getAlternatives()); 
            // InternalSeleniumDSL.g:169:3: ( rule__SeleniumInstruction__Alternatives )
            // InternalSeleniumDSL.g:169:4: rule__SeleniumInstruction__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__SeleniumInstruction__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getSeleniumInstructionAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSeleniumInstruction"


    // $ANTLR start "entryRuleValue"
    // InternalSeleniumDSL.g:178:1: entryRuleValue : ruleValue EOF ;
    public final void entryRuleValue() throws RecognitionException {
        try {
            // InternalSeleniumDSL.g:179:1: ( ruleValue EOF )
            // InternalSeleniumDSL.g:180:1: ruleValue EOF
            {
             before(grammarAccess.getValueRule()); 
            pushFollow(FOLLOW_1);
            ruleValue();

            state._fsp--;

             after(grammarAccess.getValueRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleValue"


    // $ANTLR start "ruleValue"
    // InternalSeleniumDSL.g:187:1: ruleValue : ( ( rule__Value__Alternatives ) ) ;
    public final void ruleValue() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:191:2: ( ( ( rule__Value__Alternatives ) ) )
            // InternalSeleniumDSL.g:192:2: ( ( rule__Value__Alternatives ) )
            {
            // InternalSeleniumDSL.g:192:2: ( ( rule__Value__Alternatives ) )
            // InternalSeleniumDSL.g:193:3: ( rule__Value__Alternatives )
            {
             before(grammarAccess.getValueAccess().getAlternatives()); 
            // InternalSeleniumDSL.g:194:3: ( rule__Value__Alternatives )
            // InternalSeleniumDSL.g:194:4: rule__Value__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Value__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getValueAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleValue"


    // $ANTLR start "entryRuleGet"
    // InternalSeleniumDSL.g:203:1: entryRuleGet : ruleGet EOF ;
    public final void entryRuleGet() throws RecognitionException {
        try {
            // InternalSeleniumDSL.g:204:1: ( ruleGet EOF )
            // InternalSeleniumDSL.g:205:1: ruleGet EOF
            {
             before(grammarAccess.getGetRule()); 
            pushFollow(FOLLOW_1);
            ruleGet();

            state._fsp--;

             after(grammarAccess.getGetRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGet"


    // $ANTLR start "ruleGet"
    // InternalSeleniumDSL.g:212:1: ruleGet : ( ( rule__Get__Group__0 ) ) ;
    public final void ruleGet() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:216:2: ( ( ( rule__Get__Group__0 ) ) )
            // InternalSeleniumDSL.g:217:2: ( ( rule__Get__Group__0 ) )
            {
            // InternalSeleniumDSL.g:217:2: ( ( rule__Get__Group__0 ) )
            // InternalSeleniumDSL.g:218:3: ( rule__Get__Group__0 )
            {
             before(grammarAccess.getGetAccess().getGroup()); 
            // InternalSeleniumDSL.g:219:3: ( rule__Get__Group__0 )
            // InternalSeleniumDSL.g:219:4: rule__Get__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Get__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getGetAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGet"


    // $ANTLR start "entryRuleTargetType"
    // InternalSeleniumDSL.g:228:1: entryRuleTargetType : ruleTargetType EOF ;
    public final void entryRuleTargetType() throws RecognitionException {
        try {
            // InternalSeleniumDSL.g:229:1: ( ruleTargetType EOF )
            // InternalSeleniumDSL.g:230:1: ruleTargetType EOF
            {
             before(grammarAccess.getTargetTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleTargetType();

            state._fsp--;

             after(grammarAccess.getTargetTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTargetType"


    // $ANTLR start "ruleTargetType"
    // InternalSeleniumDSL.g:237:1: ruleTargetType : ( ( rule__TargetType__TargetTypeAssignment ) ) ;
    public final void ruleTargetType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:241:2: ( ( ( rule__TargetType__TargetTypeAssignment ) ) )
            // InternalSeleniumDSL.g:242:2: ( ( rule__TargetType__TargetTypeAssignment ) )
            {
            // InternalSeleniumDSL.g:242:2: ( ( rule__TargetType__TargetTypeAssignment ) )
            // InternalSeleniumDSL.g:243:3: ( rule__TargetType__TargetTypeAssignment )
            {
             before(grammarAccess.getTargetTypeAccess().getTargetTypeAssignment()); 
            // InternalSeleniumDSL.g:244:3: ( rule__TargetType__TargetTypeAssignment )
            // InternalSeleniumDSL.g:244:4: rule__TargetType__TargetTypeAssignment
            {
            pushFollow(FOLLOW_2);
            rule__TargetType__TargetTypeAssignment();

            state._fsp--;


            }

             after(grammarAccess.getTargetTypeAccess().getTargetTypeAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTargetType"


    // $ANTLR start "entryRuleAttributeCheck"
    // InternalSeleniumDSL.g:253:1: entryRuleAttributeCheck : ruleAttributeCheck EOF ;
    public final void entryRuleAttributeCheck() throws RecognitionException {
        try {
            // InternalSeleniumDSL.g:254:1: ( ruleAttributeCheck EOF )
            // InternalSeleniumDSL.g:255:1: ruleAttributeCheck EOF
            {
             before(grammarAccess.getAttributeCheckRule()); 
            pushFollow(FOLLOW_1);
            ruleAttributeCheck();

            state._fsp--;

             after(grammarAccess.getAttributeCheckRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAttributeCheck"


    // $ANTLR start "ruleAttributeCheck"
    // InternalSeleniumDSL.g:262:1: ruleAttributeCheck : ( ( rule__AttributeCheck__AttributeNameAssignment ) ) ;
    public final void ruleAttributeCheck() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:266:2: ( ( ( rule__AttributeCheck__AttributeNameAssignment ) ) )
            // InternalSeleniumDSL.g:267:2: ( ( rule__AttributeCheck__AttributeNameAssignment ) )
            {
            // InternalSeleniumDSL.g:267:2: ( ( rule__AttributeCheck__AttributeNameAssignment ) )
            // InternalSeleniumDSL.g:268:3: ( rule__AttributeCheck__AttributeNameAssignment )
            {
             before(grammarAccess.getAttributeCheckAccess().getAttributeNameAssignment()); 
            // InternalSeleniumDSL.g:269:3: ( rule__AttributeCheck__AttributeNameAssignment )
            // InternalSeleniumDSL.g:269:4: rule__AttributeCheck__AttributeNameAssignment
            {
            pushFollow(FOLLOW_2);
            rule__AttributeCheck__AttributeNameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getAttributeCheckAccess().getAttributeNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAttributeCheck"


    // $ANTLR start "entryRuleVariableName"
    // InternalSeleniumDSL.g:278:1: entryRuleVariableName : ruleVariableName EOF ;
    public final void entryRuleVariableName() throws RecognitionException {
        try {
            // InternalSeleniumDSL.g:279:1: ( ruleVariableName EOF )
            // InternalSeleniumDSL.g:280:1: ruleVariableName EOF
            {
             before(grammarAccess.getVariableNameRule()); 
            pushFollow(FOLLOW_1);
            ruleVariableName();

            state._fsp--;

             after(grammarAccess.getVariableNameRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVariableName"


    // $ANTLR start "ruleVariableName"
    // InternalSeleniumDSL.g:287:1: ruleVariableName : ( ( rule__VariableName__NameAssignment ) ) ;
    public final void ruleVariableName() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:291:2: ( ( ( rule__VariableName__NameAssignment ) ) )
            // InternalSeleniumDSL.g:292:2: ( ( rule__VariableName__NameAssignment ) )
            {
            // InternalSeleniumDSL.g:292:2: ( ( rule__VariableName__NameAssignment ) )
            // InternalSeleniumDSL.g:293:3: ( rule__VariableName__NameAssignment )
            {
             before(grammarAccess.getVariableNameAccess().getNameAssignment()); 
            // InternalSeleniumDSL.g:294:3: ( rule__VariableName__NameAssignment )
            // InternalSeleniumDSL.g:294:4: rule__VariableName__NameAssignment
            {
            pushFollow(FOLLOW_2);
            rule__VariableName__NameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getVariableNameAccess().getNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVariableName"


    // $ANTLR start "entryRuleCondition"
    // InternalSeleniumDSL.g:303:1: entryRuleCondition : ruleCondition EOF ;
    public final void entryRuleCondition() throws RecognitionException {
        try {
            // InternalSeleniumDSL.g:304:1: ( ruleCondition EOF )
            // InternalSeleniumDSL.g:305:1: ruleCondition EOF
            {
             before(grammarAccess.getConditionRule()); 
            pushFollow(FOLLOW_1);
            ruleCondition();

            state._fsp--;

             after(grammarAccess.getConditionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCondition"


    // $ANTLR start "ruleCondition"
    // InternalSeleniumDSL.g:312:1: ruleCondition : ( ( rule__Condition__Group__0 ) ) ;
    public final void ruleCondition() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:316:2: ( ( ( rule__Condition__Group__0 ) ) )
            // InternalSeleniumDSL.g:317:2: ( ( rule__Condition__Group__0 ) )
            {
            // InternalSeleniumDSL.g:317:2: ( ( rule__Condition__Group__0 ) )
            // InternalSeleniumDSL.g:318:3: ( rule__Condition__Group__0 )
            {
             before(grammarAccess.getConditionAccess().getGroup()); 
            // InternalSeleniumDSL.g:319:3: ( rule__Condition__Group__0 )
            // InternalSeleniumDSL.g:319:4: rule__Condition__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Condition__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getConditionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCondition"


    // $ANTLR start "entryRuleTypeComboSelection"
    // InternalSeleniumDSL.g:328:1: entryRuleTypeComboSelection : ruleTypeComboSelection EOF ;
    public final void entryRuleTypeComboSelection() throws RecognitionException {
        try {
            // InternalSeleniumDSL.g:329:1: ( ruleTypeComboSelection EOF )
            // InternalSeleniumDSL.g:330:1: ruleTypeComboSelection EOF
            {
             before(grammarAccess.getTypeComboSelectionRule()); 
            pushFollow(FOLLOW_1);
            ruleTypeComboSelection();

            state._fsp--;

             after(grammarAccess.getTypeComboSelectionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTypeComboSelection"


    // $ANTLR start "ruleTypeComboSelection"
    // InternalSeleniumDSL.g:337:1: ruleTypeComboSelection : ( ( rule__TypeComboSelection__Alternatives ) ) ;
    public final void ruleTypeComboSelection() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:341:2: ( ( ( rule__TypeComboSelection__Alternatives ) ) )
            // InternalSeleniumDSL.g:342:2: ( ( rule__TypeComboSelection__Alternatives ) )
            {
            // InternalSeleniumDSL.g:342:2: ( ( rule__TypeComboSelection__Alternatives ) )
            // InternalSeleniumDSL.g:343:3: ( rule__TypeComboSelection__Alternatives )
            {
             before(grammarAccess.getTypeComboSelectionAccess().getAlternatives()); 
            // InternalSeleniumDSL.g:344:3: ( rule__TypeComboSelection__Alternatives )
            // InternalSeleniumDSL.g:344:4: rule__TypeComboSelection__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__TypeComboSelection__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getTypeComboSelectionAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTypeComboSelection"


    // $ANTLR start "entryRuleSelect"
    // InternalSeleniumDSL.g:353:1: entryRuleSelect : ruleSelect EOF ;
    public final void entryRuleSelect() throws RecognitionException {
        try {
            // InternalSeleniumDSL.g:354:1: ( ruleSelect EOF )
            // InternalSeleniumDSL.g:355:1: ruleSelect EOF
            {
             before(grammarAccess.getSelectRule()); 
            pushFollow(FOLLOW_1);
            ruleSelect();

            state._fsp--;

             after(grammarAccess.getSelectRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSelect"


    // $ANTLR start "ruleSelect"
    // InternalSeleniumDSL.g:362:1: ruleSelect : ( ( rule__Select__Group__0 ) ) ;
    public final void ruleSelect() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:366:2: ( ( ( rule__Select__Group__0 ) ) )
            // InternalSeleniumDSL.g:367:2: ( ( rule__Select__Group__0 ) )
            {
            // InternalSeleniumDSL.g:367:2: ( ( rule__Select__Group__0 ) )
            // InternalSeleniumDSL.g:368:3: ( rule__Select__Group__0 )
            {
             before(grammarAccess.getSelectAccess().getGroup()); 
            // InternalSeleniumDSL.g:369:3: ( rule__Select__Group__0 )
            // InternalSeleniumDSL.g:369:4: rule__Select__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Select__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSelectAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSelect"


    // $ANTLR start "entryRuleCheck"
    // InternalSeleniumDSL.g:378:1: entryRuleCheck : ruleCheck EOF ;
    public final void entryRuleCheck() throws RecognitionException {
        try {
            // InternalSeleniumDSL.g:379:1: ( ruleCheck EOF )
            // InternalSeleniumDSL.g:380:1: ruleCheck EOF
            {
             before(grammarAccess.getCheckRule()); 
            pushFollow(FOLLOW_1);
            ruleCheck();

            state._fsp--;

             after(grammarAccess.getCheckRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCheck"


    // $ANTLR start "ruleCheck"
    // InternalSeleniumDSL.g:387:1: ruleCheck : ( ( rule__Check__Group__0 ) ) ;
    public final void ruleCheck() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:391:2: ( ( ( rule__Check__Group__0 ) ) )
            // InternalSeleniumDSL.g:392:2: ( ( rule__Check__Group__0 ) )
            {
            // InternalSeleniumDSL.g:392:2: ( ( rule__Check__Group__0 ) )
            // InternalSeleniumDSL.g:393:3: ( rule__Check__Group__0 )
            {
             before(grammarAccess.getCheckAccess().getGroup()); 
            // InternalSeleniumDSL.g:394:3: ( rule__Check__Group__0 )
            // InternalSeleniumDSL.g:394:4: rule__Check__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Check__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCheckAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCheck"


    // $ANTLR start "entryRuleClick"
    // InternalSeleniumDSL.g:403:1: entryRuleClick : ruleClick EOF ;
    public final void entryRuleClick() throws RecognitionException {
        try {
            // InternalSeleniumDSL.g:404:1: ( ruleClick EOF )
            // InternalSeleniumDSL.g:405:1: ruleClick EOF
            {
             before(grammarAccess.getClickRule()); 
            pushFollow(FOLLOW_1);
            ruleClick();

            state._fsp--;

             after(grammarAccess.getClickRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleClick"


    // $ANTLR start "ruleClick"
    // InternalSeleniumDSL.g:412:1: ruleClick : ( ( rule__Click__Group__0 ) ) ;
    public final void ruleClick() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:416:2: ( ( ( rule__Click__Group__0 ) ) )
            // InternalSeleniumDSL.g:417:2: ( ( rule__Click__Group__0 ) )
            {
            // InternalSeleniumDSL.g:417:2: ( ( rule__Click__Group__0 ) )
            // InternalSeleniumDSL.g:418:3: ( rule__Click__Group__0 )
            {
             before(grammarAccess.getClickAccess().getGroup()); 
            // InternalSeleniumDSL.g:419:3: ( rule__Click__Group__0 )
            // InternalSeleniumDSL.g:419:4: rule__Click__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Click__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getClickAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleClick"


    // $ANTLR start "entryRuleStore"
    // InternalSeleniumDSL.g:428:1: entryRuleStore : ruleStore EOF ;
    public final void entryRuleStore() throws RecognitionException {
        try {
            // InternalSeleniumDSL.g:429:1: ( ruleStore EOF )
            // InternalSeleniumDSL.g:430:1: ruleStore EOF
            {
             before(grammarAccess.getStoreRule()); 
            pushFollow(FOLLOW_1);
            ruleStore();

            state._fsp--;

             after(grammarAccess.getStoreRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStore"


    // $ANTLR start "ruleStore"
    // InternalSeleniumDSL.g:437:1: ruleStore : ( ( rule__Store__Group__0 ) ) ;
    public final void ruleStore() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:441:2: ( ( ( rule__Store__Group__0 ) ) )
            // InternalSeleniumDSL.g:442:2: ( ( rule__Store__Group__0 ) )
            {
            // InternalSeleniumDSL.g:442:2: ( ( rule__Store__Group__0 ) )
            // InternalSeleniumDSL.g:443:3: ( rule__Store__Group__0 )
            {
             before(grammarAccess.getStoreAccess().getGroup()); 
            // InternalSeleniumDSL.g:444:3: ( rule__Store__Group__0 )
            // InternalSeleniumDSL.g:444:4: rule__Store__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Store__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getStoreAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStore"


    // $ANTLR start "entryRuleSet"
    // InternalSeleniumDSL.g:453:1: entryRuleSet : ruleSet EOF ;
    public final void entryRuleSet() throws RecognitionException {
        try {
            // InternalSeleniumDSL.g:454:1: ( ruleSet EOF )
            // InternalSeleniumDSL.g:455:1: ruleSet EOF
            {
             before(grammarAccess.getSetRule()); 
            pushFollow(FOLLOW_1);
            ruleSet();

            state._fsp--;

             after(grammarAccess.getSetRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSet"


    // $ANTLR start "ruleSet"
    // InternalSeleniumDSL.g:462:1: ruleSet : ( ( rule__Set__Group__0 ) ) ;
    public final void ruleSet() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:466:2: ( ( ( rule__Set__Group__0 ) ) )
            // InternalSeleniumDSL.g:467:2: ( ( rule__Set__Group__0 ) )
            {
            // InternalSeleniumDSL.g:467:2: ( ( rule__Set__Group__0 ) )
            // InternalSeleniumDSL.g:468:3: ( rule__Set__Group__0 )
            {
             before(grammarAccess.getSetAccess().getGroup()); 
            // InternalSeleniumDSL.g:469:3: ( rule__Set__Group__0 )
            // InternalSeleniumDSL.g:469:4: rule__Set__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Set__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSetAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSet"


    // $ANTLR start "entryRuleIsElementPresent"
    // InternalSeleniumDSL.g:478:1: entryRuleIsElementPresent : ruleIsElementPresent EOF ;
    public final void entryRuleIsElementPresent() throws RecognitionException {
        try {
            // InternalSeleniumDSL.g:479:1: ( ruleIsElementPresent EOF )
            // InternalSeleniumDSL.g:480:1: ruleIsElementPresent EOF
            {
             before(grammarAccess.getIsElementPresentRule()); 
            pushFollow(FOLLOW_1);
            ruleIsElementPresent();

            state._fsp--;

             after(grammarAccess.getIsElementPresentRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIsElementPresent"


    // $ANTLR start "ruleIsElementPresent"
    // InternalSeleniumDSL.g:487:1: ruleIsElementPresent : ( ( rule__IsElementPresent__Group__0 ) ) ;
    public final void ruleIsElementPresent() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:491:2: ( ( ( rule__IsElementPresent__Group__0 ) ) )
            // InternalSeleniumDSL.g:492:2: ( ( rule__IsElementPresent__Group__0 ) )
            {
            // InternalSeleniumDSL.g:492:2: ( ( rule__IsElementPresent__Group__0 ) )
            // InternalSeleniumDSL.g:493:3: ( rule__IsElementPresent__Group__0 )
            {
             before(grammarAccess.getIsElementPresentAccess().getGroup()); 
            // InternalSeleniumDSL.g:494:3: ( rule__IsElementPresent__Group__0 )
            // InternalSeleniumDSL.g:494:4: rule__IsElementPresent__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__IsElementPresent__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getIsElementPresentAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIsElementPresent"


    // $ANTLR start "entryRuleCount"
    // InternalSeleniumDSL.g:503:1: entryRuleCount : ruleCount EOF ;
    public final void entryRuleCount() throws RecognitionException {
        try {
            // InternalSeleniumDSL.g:504:1: ( ruleCount EOF )
            // InternalSeleniumDSL.g:505:1: ruleCount EOF
            {
             before(grammarAccess.getCountRule()); 
            pushFollow(FOLLOW_1);
            ruleCount();

            state._fsp--;

             after(grammarAccess.getCountRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCount"


    // $ANTLR start "ruleCount"
    // InternalSeleniumDSL.g:512:1: ruleCount : ( ( rule__Count__Group__0 ) ) ;
    public final void ruleCount() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:516:2: ( ( ( rule__Count__Group__0 ) ) )
            // InternalSeleniumDSL.g:517:2: ( ( rule__Count__Group__0 ) )
            {
            // InternalSeleniumDSL.g:517:2: ( ( rule__Count__Group__0 ) )
            // InternalSeleniumDSL.g:518:3: ( rule__Count__Group__0 )
            {
             before(grammarAccess.getCountAccess().getGroup()); 
            // InternalSeleniumDSL.g:519:3: ( rule__Count__Group__0 )
            // InternalSeleniumDSL.g:519:4: rule__Count__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Count__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCountAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCount"


    // $ANTLR start "entryRuleArgument"
    // InternalSeleniumDSL.g:528:1: entryRuleArgument : ruleArgument EOF ;
    public final void entryRuleArgument() throws RecognitionException {
        try {
            // InternalSeleniumDSL.g:529:1: ( ruleArgument EOF )
            // InternalSeleniumDSL.g:530:1: ruleArgument EOF
            {
             before(grammarAccess.getArgumentRule()); 
            pushFollow(FOLLOW_1);
            ruleArgument();

            state._fsp--;

             after(grammarAccess.getArgumentRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleArgument"


    // $ANTLR start "ruleArgument"
    // InternalSeleniumDSL.g:537:1: ruleArgument : ( ( rule__Argument__NameAssignment ) ) ;
    public final void ruleArgument() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:541:2: ( ( ( rule__Argument__NameAssignment ) ) )
            // InternalSeleniumDSL.g:542:2: ( ( rule__Argument__NameAssignment ) )
            {
            // InternalSeleniumDSL.g:542:2: ( ( rule__Argument__NameAssignment ) )
            // InternalSeleniumDSL.g:543:3: ( rule__Argument__NameAssignment )
            {
             before(grammarAccess.getArgumentAccess().getNameAssignment()); 
            // InternalSeleniumDSL.g:544:3: ( rule__Argument__NameAssignment )
            // InternalSeleniumDSL.g:544:4: rule__Argument__NameAssignment
            {
            pushFollow(FOLLOW_2);
            rule__Argument__NameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getArgumentAccess().getNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleArgument"


    // $ANTLR start "entryRuleFunctionName"
    // InternalSeleniumDSL.g:553:1: entryRuleFunctionName : ruleFunctionName EOF ;
    public final void entryRuleFunctionName() throws RecognitionException {
        try {
            // InternalSeleniumDSL.g:554:1: ( ruleFunctionName EOF )
            // InternalSeleniumDSL.g:555:1: ruleFunctionName EOF
            {
             before(grammarAccess.getFunctionNameRule()); 
            pushFollow(FOLLOW_1);
            ruleFunctionName();

            state._fsp--;

             after(grammarAccess.getFunctionNameRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFunctionName"


    // $ANTLR start "ruleFunctionName"
    // InternalSeleniumDSL.g:562:1: ruleFunctionName : ( ( rule__FunctionName__NameAssignment ) ) ;
    public final void ruleFunctionName() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:566:2: ( ( ( rule__FunctionName__NameAssignment ) ) )
            // InternalSeleniumDSL.g:567:2: ( ( rule__FunctionName__NameAssignment ) )
            {
            // InternalSeleniumDSL.g:567:2: ( ( rule__FunctionName__NameAssignment ) )
            // InternalSeleniumDSL.g:568:3: ( rule__FunctionName__NameAssignment )
            {
             before(grammarAccess.getFunctionNameAccess().getNameAssignment()); 
            // InternalSeleniumDSL.g:569:3: ( rule__FunctionName__NameAssignment )
            // InternalSeleniumDSL.g:569:4: rule__FunctionName__NameAssignment
            {
            pushFollow(FOLLOW_2);
            rule__FunctionName__NameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getFunctionNameAccess().getNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFunctionName"


    // $ANTLR start "entryRuleFunction"
    // InternalSeleniumDSL.g:578:1: entryRuleFunction : ruleFunction EOF ;
    public final void entryRuleFunction() throws RecognitionException {
        try {
            // InternalSeleniumDSL.g:579:1: ( ruleFunction EOF )
            // InternalSeleniumDSL.g:580:1: ruleFunction EOF
            {
             before(grammarAccess.getFunctionRule()); 
            pushFollow(FOLLOW_1);
            ruleFunction();

            state._fsp--;

             after(grammarAccess.getFunctionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFunction"


    // $ANTLR start "ruleFunction"
    // InternalSeleniumDSL.g:587:1: ruleFunction : ( ( rule__Function__Group__0 ) ) ;
    public final void ruleFunction() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:591:2: ( ( ( rule__Function__Group__0 ) ) )
            // InternalSeleniumDSL.g:592:2: ( ( rule__Function__Group__0 ) )
            {
            // InternalSeleniumDSL.g:592:2: ( ( rule__Function__Group__0 ) )
            // InternalSeleniumDSL.g:593:3: ( rule__Function__Group__0 )
            {
             before(grammarAccess.getFunctionAccess().getGroup()); 
            // InternalSeleniumDSL.g:594:3: ( rule__Function__Group__0 )
            // InternalSeleniumDSL.g:594:4: rule__Function__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Function__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFunctionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFunction"


    // $ANTLR start "entryRuleFunctionCall"
    // InternalSeleniumDSL.g:603:1: entryRuleFunctionCall : ruleFunctionCall EOF ;
    public final void entryRuleFunctionCall() throws RecognitionException {
        try {
            // InternalSeleniumDSL.g:604:1: ( ruleFunctionCall EOF )
            // InternalSeleniumDSL.g:605:1: ruleFunctionCall EOF
            {
             before(grammarAccess.getFunctionCallRule()); 
            pushFollow(FOLLOW_1);
            ruleFunctionCall();

            state._fsp--;

             after(grammarAccess.getFunctionCallRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFunctionCall"


    // $ANTLR start "ruleFunctionCall"
    // InternalSeleniumDSL.g:612:1: ruleFunctionCall : ( ( rule__FunctionCall__Group__0 ) ) ;
    public final void ruleFunctionCall() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:616:2: ( ( ( rule__FunctionCall__Group__0 ) ) )
            // InternalSeleniumDSL.g:617:2: ( ( rule__FunctionCall__Group__0 ) )
            {
            // InternalSeleniumDSL.g:617:2: ( ( rule__FunctionCall__Group__0 ) )
            // InternalSeleniumDSL.g:618:3: ( rule__FunctionCall__Group__0 )
            {
             before(grammarAccess.getFunctionCallAccess().getGroup()); 
            // InternalSeleniumDSL.g:619:3: ( rule__FunctionCall__Group__0 )
            // InternalSeleniumDSL.g:619:4: rule__FunctionCall__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__FunctionCall__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFunctionCallAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFunctionCall"


    // $ANTLR start "entryRuleAssert"
    // InternalSeleniumDSL.g:628:1: entryRuleAssert : ruleAssert EOF ;
    public final void entryRuleAssert() throws RecognitionException {
        try {
            // InternalSeleniumDSL.g:629:1: ( ruleAssert EOF )
            // InternalSeleniumDSL.g:630:1: ruleAssert EOF
            {
             before(grammarAccess.getAssertRule()); 
            pushFollow(FOLLOW_1);
            ruleAssert();

            state._fsp--;

             after(grammarAccess.getAssertRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAssert"


    // $ANTLR start "ruleAssert"
    // InternalSeleniumDSL.g:637:1: ruleAssert : ( ( rule__Assert__Group__0 ) ) ;
    public final void ruleAssert() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:641:2: ( ( ( rule__Assert__Group__0 ) ) )
            // InternalSeleniumDSL.g:642:2: ( ( rule__Assert__Group__0 ) )
            {
            // InternalSeleniumDSL.g:642:2: ( ( rule__Assert__Group__0 ) )
            // InternalSeleniumDSL.g:643:3: ( rule__Assert__Group__0 )
            {
             before(grammarAccess.getAssertAccess().getGroup()); 
            // InternalSeleniumDSL.g:644:3: ( rule__Assert__Group__0 )
            // InternalSeleniumDSL.g:644:4: rule__Assert__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Assert__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAssertAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAssert"


    // $ANTLR start "rule__Browser__Alternatives"
    // InternalSeleniumDSL.g:652:1: rule__Browser__Alternatives : ( ( 'chrome' ) | ( 'firefox' ) | ( 'opera' ) );
    public final void rule__Browser__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:656:1: ( ( 'chrome' ) | ( 'firefox' ) | ( 'opera' ) )
            int alt1=3;
            switch ( input.LA(1) ) {
            case 12:
                {
                alt1=1;
                }
                break;
            case 13:
                {
                alt1=2;
                }
                break;
            case 14:
                {
                alt1=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // InternalSeleniumDSL.g:657:2: ( 'chrome' )
                    {
                    // InternalSeleniumDSL.g:657:2: ( 'chrome' )
                    // InternalSeleniumDSL.g:658:3: 'chrome'
                    {
                     before(grammarAccess.getBrowserAccess().getChromeKeyword_0()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getBrowserAccess().getChromeKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSeleniumDSL.g:663:2: ( 'firefox' )
                    {
                    // InternalSeleniumDSL.g:663:2: ( 'firefox' )
                    // InternalSeleniumDSL.g:664:3: 'firefox'
                    {
                     before(grammarAccess.getBrowserAccess().getFirefoxKeyword_1()); 
                    match(input,13,FOLLOW_2); 
                     after(grammarAccess.getBrowserAccess().getFirefoxKeyword_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalSeleniumDSL.g:669:2: ( 'opera' )
                    {
                    // InternalSeleniumDSL.g:669:2: ( 'opera' )
                    // InternalSeleniumDSL.g:670:3: 'opera'
                    {
                     before(grammarAccess.getBrowserAccess().getOperaKeyword_2()); 
                    match(input,14,FOLLOW_2); 
                     after(grammarAccess.getBrowserAccess().getOperaKeyword_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Browser__Alternatives"


    // $ANTLR start "rule__SeleniumInstruction__Alternatives"
    // InternalSeleniumDSL.g:679:1: rule__SeleniumInstruction__Alternatives : ( ( ruleGet ) | ( ruleClick ) | ( ruleStore ) | ( ruleIsElementPresent ) | ( ruleSet ) | ( ruleFunctionCall ) | ( ruleCount ) | ( ruleAssert ) | ( ruleSelect ) | ( ruleCheck ) );
    public final void rule__SeleniumInstruction__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:683:1: ( ( ruleGet ) | ( ruleClick ) | ( ruleStore ) | ( ruleIsElementPresent ) | ( ruleSet ) | ( ruleFunctionCall ) | ( ruleCount ) | ( ruleAssert ) | ( ruleSelect ) | ( ruleCheck ) )
            int alt2=10;
            switch ( input.LA(1) ) {
            case 30:
                {
                alt2=1;
                }
                break;
            case 36:
                {
                alt2=2;
                }
                break;
            case 37:
                {
                alt2=3;
                }
                break;
            case 41:
                {
                alt2=4;
                }
                break;
            case 39:
                {
                alt2=5;
                }
                break;
            case RULE_ID:
                {
                alt2=6;
                }
                break;
            case 42:
                {
                alt2=7;
                }
                break;
            case 47:
                {
                alt2=8;
                }
                break;
            case 32:
                {
                alt2=9;
                }
                break;
            case 27:
            case 28:
                {
                alt2=10;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // InternalSeleniumDSL.g:684:2: ( ruleGet )
                    {
                    // InternalSeleniumDSL.g:684:2: ( ruleGet )
                    // InternalSeleniumDSL.g:685:3: ruleGet
                    {
                     before(grammarAccess.getSeleniumInstructionAccess().getGetParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleGet();

                    state._fsp--;

                     after(grammarAccess.getSeleniumInstructionAccess().getGetParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSeleniumDSL.g:690:2: ( ruleClick )
                    {
                    // InternalSeleniumDSL.g:690:2: ( ruleClick )
                    // InternalSeleniumDSL.g:691:3: ruleClick
                    {
                     before(grammarAccess.getSeleniumInstructionAccess().getClickParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleClick();

                    state._fsp--;

                     after(grammarAccess.getSeleniumInstructionAccess().getClickParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalSeleniumDSL.g:696:2: ( ruleStore )
                    {
                    // InternalSeleniumDSL.g:696:2: ( ruleStore )
                    // InternalSeleniumDSL.g:697:3: ruleStore
                    {
                     before(grammarAccess.getSeleniumInstructionAccess().getStoreParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleStore();

                    state._fsp--;

                     after(grammarAccess.getSeleniumInstructionAccess().getStoreParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalSeleniumDSL.g:702:2: ( ruleIsElementPresent )
                    {
                    // InternalSeleniumDSL.g:702:2: ( ruleIsElementPresent )
                    // InternalSeleniumDSL.g:703:3: ruleIsElementPresent
                    {
                     before(grammarAccess.getSeleniumInstructionAccess().getIsElementPresentParserRuleCall_3()); 
                    pushFollow(FOLLOW_2);
                    ruleIsElementPresent();

                    state._fsp--;

                     after(grammarAccess.getSeleniumInstructionAccess().getIsElementPresentParserRuleCall_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalSeleniumDSL.g:708:2: ( ruleSet )
                    {
                    // InternalSeleniumDSL.g:708:2: ( ruleSet )
                    // InternalSeleniumDSL.g:709:3: ruleSet
                    {
                     before(grammarAccess.getSeleniumInstructionAccess().getSetParserRuleCall_4()); 
                    pushFollow(FOLLOW_2);
                    ruleSet();

                    state._fsp--;

                     after(grammarAccess.getSeleniumInstructionAccess().getSetParserRuleCall_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalSeleniumDSL.g:714:2: ( ruleFunctionCall )
                    {
                    // InternalSeleniumDSL.g:714:2: ( ruleFunctionCall )
                    // InternalSeleniumDSL.g:715:3: ruleFunctionCall
                    {
                     before(grammarAccess.getSeleniumInstructionAccess().getFunctionCallParserRuleCall_5()); 
                    pushFollow(FOLLOW_2);
                    ruleFunctionCall();

                    state._fsp--;

                     after(grammarAccess.getSeleniumInstructionAccess().getFunctionCallParserRuleCall_5()); 

                    }


                    }
                    break;
                case 7 :
                    // InternalSeleniumDSL.g:720:2: ( ruleCount )
                    {
                    // InternalSeleniumDSL.g:720:2: ( ruleCount )
                    // InternalSeleniumDSL.g:721:3: ruleCount
                    {
                     before(grammarAccess.getSeleniumInstructionAccess().getCountParserRuleCall_6()); 
                    pushFollow(FOLLOW_2);
                    ruleCount();

                    state._fsp--;

                     after(grammarAccess.getSeleniumInstructionAccess().getCountParserRuleCall_6()); 

                    }


                    }
                    break;
                case 8 :
                    // InternalSeleniumDSL.g:726:2: ( ruleAssert )
                    {
                    // InternalSeleniumDSL.g:726:2: ( ruleAssert )
                    // InternalSeleniumDSL.g:727:3: ruleAssert
                    {
                     before(grammarAccess.getSeleniumInstructionAccess().getAssertParserRuleCall_7()); 
                    pushFollow(FOLLOW_2);
                    ruleAssert();

                    state._fsp--;

                     after(grammarAccess.getSeleniumInstructionAccess().getAssertParserRuleCall_7()); 

                    }


                    }
                    break;
                case 9 :
                    // InternalSeleniumDSL.g:732:2: ( ruleSelect )
                    {
                    // InternalSeleniumDSL.g:732:2: ( ruleSelect )
                    // InternalSeleniumDSL.g:733:3: ruleSelect
                    {
                     before(grammarAccess.getSeleniumInstructionAccess().getSelectParserRuleCall_8()); 
                    pushFollow(FOLLOW_2);
                    ruleSelect();

                    state._fsp--;

                     after(grammarAccess.getSeleniumInstructionAccess().getSelectParserRuleCall_8()); 

                    }


                    }
                    break;
                case 10 :
                    // InternalSeleniumDSL.g:738:2: ( ruleCheck )
                    {
                    // InternalSeleniumDSL.g:738:2: ( ruleCheck )
                    // InternalSeleniumDSL.g:739:3: ruleCheck
                    {
                     before(grammarAccess.getSeleniumInstructionAccess().getCheckParserRuleCall_9()); 
                    pushFollow(FOLLOW_2);
                    ruleCheck();

                    state._fsp--;

                     after(grammarAccess.getSeleniumInstructionAccess().getCheckParserRuleCall_9()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SeleniumInstruction__Alternatives"


    // $ANTLR start "rule__Value__Alternatives"
    // InternalSeleniumDSL.g:748:1: rule__Value__Alternatives : ( ( ( rule__Value__VariableNameAssignment_0 ) ) | ( ( rule__Value__ValueAssignment_1 ) ) );
    public final void rule__Value__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:752:1: ( ( ( rule__Value__VariableNameAssignment_0 ) ) | ( ( rule__Value__ValueAssignment_1 ) ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==RULE_ID) ) {
                alt3=1;
            }
            else if ( (LA3_0==RULE_STRING) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalSeleniumDSL.g:753:2: ( ( rule__Value__VariableNameAssignment_0 ) )
                    {
                    // InternalSeleniumDSL.g:753:2: ( ( rule__Value__VariableNameAssignment_0 ) )
                    // InternalSeleniumDSL.g:754:3: ( rule__Value__VariableNameAssignment_0 )
                    {
                     before(grammarAccess.getValueAccess().getVariableNameAssignment_0()); 
                    // InternalSeleniumDSL.g:755:3: ( rule__Value__VariableNameAssignment_0 )
                    // InternalSeleniumDSL.g:755:4: rule__Value__VariableNameAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Value__VariableNameAssignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getValueAccess().getVariableNameAssignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSeleniumDSL.g:759:2: ( ( rule__Value__ValueAssignment_1 ) )
                    {
                    // InternalSeleniumDSL.g:759:2: ( ( rule__Value__ValueAssignment_1 ) )
                    // InternalSeleniumDSL.g:760:3: ( rule__Value__ValueAssignment_1 )
                    {
                     before(grammarAccess.getValueAccess().getValueAssignment_1()); 
                    // InternalSeleniumDSL.g:761:3: ( rule__Value__ValueAssignment_1 )
                    // InternalSeleniumDSL.g:761:4: rule__Value__ValueAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Value__ValueAssignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getValueAccess().getValueAssignment_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value__Alternatives"


    // $ANTLR start "rule__TargetType__TargetTypeAlternatives_0"
    // InternalSeleniumDSL.g:769:1: rule__TargetType__TargetTypeAlternatives_0 : ( ( 'button' ) | ( 'link' ) | ( 'image' ) | ( 'checkbox' ) | ( 'input' ) | ( 'div' ) | ( 'span' ) | ( 'label' ) );
    public final void rule__TargetType__TargetTypeAlternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:773:1: ( ( 'button' ) | ( 'link' ) | ( 'image' ) | ( 'checkbox' ) | ( 'input' ) | ( 'div' ) | ( 'span' ) | ( 'label' ) )
            int alt4=8;
            switch ( input.LA(1) ) {
            case 15:
                {
                alt4=1;
                }
                break;
            case 16:
                {
                alt4=2;
                }
                break;
            case 17:
                {
                alt4=3;
                }
                break;
            case 18:
                {
                alt4=4;
                }
                break;
            case 19:
                {
                alt4=5;
                }
                break;
            case 20:
                {
                alt4=6;
                }
                break;
            case 21:
                {
                alt4=7;
                }
                break;
            case 22:
                {
                alt4=8;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // InternalSeleniumDSL.g:774:2: ( 'button' )
                    {
                    // InternalSeleniumDSL.g:774:2: ( 'button' )
                    // InternalSeleniumDSL.g:775:3: 'button'
                    {
                     before(grammarAccess.getTargetTypeAccess().getTargetTypeButtonKeyword_0_0()); 
                    match(input,15,FOLLOW_2); 
                     after(grammarAccess.getTargetTypeAccess().getTargetTypeButtonKeyword_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSeleniumDSL.g:780:2: ( 'link' )
                    {
                    // InternalSeleniumDSL.g:780:2: ( 'link' )
                    // InternalSeleniumDSL.g:781:3: 'link'
                    {
                     before(grammarAccess.getTargetTypeAccess().getTargetTypeLinkKeyword_0_1()); 
                    match(input,16,FOLLOW_2); 
                     after(grammarAccess.getTargetTypeAccess().getTargetTypeLinkKeyword_0_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalSeleniumDSL.g:786:2: ( 'image' )
                    {
                    // InternalSeleniumDSL.g:786:2: ( 'image' )
                    // InternalSeleniumDSL.g:787:3: 'image'
                    {
                     before(grammarAccess.getTargetTypeAccess().getTargetTypeImageKeyword_0_2()); 
                    match(input,17,FOLLOW_2); 
                     after(grammarAccess.getTargetTypeAccess().getTargetTypeImageKeyword_0_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalSeleniumDSL.g:792:2: ( 'checkbox' )
                    {
                    // InternalSeleniumDSL.g:792:2: ( 'checkbox' )
                    // InternalSeleniumDSL.g:793:3: 'checkbox'
                    {
                     before(grammarAccess.getTargetTypeAccess().getTargetTypeCheckboxKeyword_0_3()); 
                    match(input,18,FOLLOW_2); 
                     after(grammarAccess.getTargetTypeAccess().getTargetTypeCheckboxKeyword_0_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalSeleniumDSL.g:798:2: ( 'input' )
                    {
                    // InternalSeleniumDSL.g:798:2: ( 'input' )
                    // InternalSeleniumDSL.g:799:3: 'input'
                    {
                     before(grammarAccess.getTargetTypeAccess().getTargetTypeInputKeyword_0_4()); 
                    match(input,19,FOLLOW_2); 
                     after(grammarAccess.getTargetTypeAccess().getTargetTypeInputKeyword_0_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalSeleniumDSL.g:804:2: ( 'div' )
                    {
                    // InternalSeleniumDSL.g:804:2: ( 'div' )
                    // InternalSeleniumDSL.g:805:3: 'div'
                    {
                     before(grammarAccess.getTargetTypeAccess().getTargetTypeDivKeyword_0_5()); 
                    match(input,20,FOLLOW_2); 
                     after(grammarAccess.getTargetTypeAccess().getTargetTypeDivKeyword_0_5()); 

                    }


                    }
                    break;
                case 7 :
                    // InternalSeleniumDSL.g:810:2: ( 'span' )
                    {
                    // InternalSeleniumDSL.g:810:2: ( 'span' )
                    // InternalSeleniumDSL.g:811:3: 'span'
                    {
                     before(grammarAccess.getTargetTypeAccess().getTargetTypeSpanKeyword_0_6()); 
                    match(input,21,FOLLOW_2); 
                     after(grammarAccess.getTargetTypeAccess().getTargetTypeSpanKeyword_0_6()); 

                    }


                    }
                    break;
                case 8 :
                    // InternalSeleniumDSL.g:816:2: ( 'label' )
                    {
                    // InternalSeleniumDSL.g:816:2: ( 'label' )
                    // InternalSeleniumDSL.g:817:3: 'label'
                    {
                     before(grammarAccess.getTargetTypeAccess().getTargetTypeLabelKeyword_0_7()); 
                    match(input,22,FOLLOW_2); 
                     after(grammarAccess.getTargetTypeAccess().getTargetTypeLabelKeyword_0_7()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TargetType__TargetTypeAlternatives_0"


    // $ANTLR start "rule__AttributeCheck__AttributeNameAlternatives_0"
    // InternalSeleniumDSL.g:826:1: rule__AttributeCheck__AttributeNameAlternatives_0 : ( ( RULE_STRING ) | ( 'text' ) );
    public final void rule__AttributeCheck__AttributeNameAlternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:830:1: ( ( RULE_STRING ) | ( 'text' ) )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==RULE_STRING) ) {
                alt5=1;
            }
            else if ( (LA5_0==23) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // InternalSeleniumDSL.g:831:2: ( RULE_STRING )
                    {
                    // InternalSeleniumDSL.g:831:2: ( RULE_STRING )
                    // InternalSeleniumDSL.g:832:3: RULE_STRING
                    {
                     before(grammarAccess.getAttributeCheckAccess().getAttributeNameSTRINGTerminalRuleCall_0_0()); 
                    match(input,RULE_STRING,FOLLOW_2); 
                     after(grammarAccess.getAttributeCheckAccess().getAttributeNameSTRINGTerminalRuleCall_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSeleniumDSL.g:837:2: ( 'text' )
                    {
                    // InternalSeleniumDSL.g:837:2: ( 'text' )
                    // InternalSeleniumDSL.g:838:3: 'text'
                    {
                     before(grammarAccess.getAttributeCheckAccess().getAttributeNameTextKeyword_0_1()); 
                    match(input,23,FOLLOW_2); 
                     after(grammarAccess.getAttributeCheckAccess().getAttributeNameTextKeyword_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeCheck__AttributeNameAlternatives_0"


    // $ANTLR start "rule__Condition__OperatorAlternatives_2_0"
    // InternalSeleniumDSL.g:847:1: rule__Condition__OperatorAlternatives_2_0 : ( ( 'contains' ) | ( 'equals' ) );
    public final void rule__Condition__OperatorAlternatives_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:851:1: ( ( 'contains' ) | ( 'equals' ) )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==24) ) {
                alt6=1;
            }
            else if ( (LA6_0==25) ) {
                alt6=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // InternalSeleniumDSL.g:852:2: ( 'contains' )
                    {
                    // InternalSeleniumDSL.g:852:2: ( 'contains' )
                    // InternalSeleniumDSL.g:853:3: 'contains'
                    {
                     before(grammarAccess.getConditionAccess().getOperatorContainsKeyword_2_0_0()); 
                    match(input,24,FOLLOW_2); 
                     after(grammarAccess.getConditionAccess().getOperatorContainsKeyword_2_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSeleniumDSL.g:858:2: ( 'equals' )
                    {
                    // InternalSeleniumDSL.g:858:2: ( 'equals' )
                    // InternalSeleniumDSL.g:859:3: 'equals'
                    {
                     before(grammarAccess.getConditionAccess().getOperatorEqualsKeyword_2_0_1()); 
                    match(input,25,FOLLOW_2); 
                     after(grammarAccess.getConditionAccess().getOperatorEqualsKeyword_2_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__OperatorAlternatives_2_0"


    // $ANTLR start "rule__TypeComboSelection__Alternatives"
    // InternalSeleniumDSL.g:868:1: rule__TypeComboSelection__Alternatives : ( ( 'text' ) | ( 'value' ) );
    public final void rule__TypeComboSelection__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:872:1: ( ( 'text' ) | ( 'value' ) )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==23) ) {
                alt7=1;
            }
            else if ( (LA7_0==26) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalSeleniumDSL.g:873:2: ( 'text' )
                    {
                    // InternalSeleniumDSL.g:873:2: ( 'text' )
                    // InternalSeleniumDSL.g:874:3: 'text'
                    {
                     before(grammarAccess.getTypeComboSelectionAccess().getTextKeyword_0()); 
                    match(input,23,FOLLOW_2); 
                     after(grammarAccess.getTypeComboSelectionAccess().getTextKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSeleniumDSL.g:879:2: ( 'value' )
                    {
                    // InternalSeleniumDSL.g:879:2: ( 'value' )
                    // InternalSeleniumDSL.g:880:3: 'value'
                    {
                     before(grammarAccess.getTypeComboSelectionAccess().getValueKeyword_1()); 
                    match(input,26,FOLLOW_2); 
                     after(grammarAccess.getTypeComboSelectionAccess().getValueKeyword_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeComboSelection__Alternatives"


    // $ANTLR start "rule__Check__ToCheckAlternatives_0_0"
    // InternalSeleniumDSL.g:889:1: rule__Check__ToCheckAlternatives_0_0 : ( ( 'check' ) | ( 'uncheck' ) );
    public final void rule__Check__ToCheckAlternatives_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:893:1: ( ( 'check' ) | ( 'uncheck' ) )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==27) ) {
                alt8=1;
            }
            else if ( (LA8_0==28) ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // InternalSeleniumDSL.g:894:2: ( 'check' )
                    {
                    // InternalSeleniumDSL.g:894:2: ( 'check' )
                    // InternalSeleniumDSL.g:895:3: 'check'
                    {
                     before(grammarAccess.getCheckAccess().getToCheckCheckKeyword_0_0_0()); 
                    match(input,27,FOLLOW_2); 
                     after(grammarAccess.getCheckAccess().getToCheckCheckKeyword_0_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSeleniumDSL.g:900:2: ( 'uncheck' )
                    {
                    // InternalSeleniumDSL.g:900:2: ( 'uncheck' )
                    // InternalSeleniumDSL.g:901:3: 'uncheck'
                    {
                     before(grammarAccess.getCheckAccess().getToCheckUncheckKeyword_0_0_1()); 
                    match(input,28,FOLLOW_2); 
                     after(grammarAccess.getCheckAccess().getToCheckUncheckKeyword_0_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Check__ToCheckAlternatives_0_0"


    // $ANTLR start "rule__Store__Alternatives_1"
    // InternalSeleniumDSL.g:910:1: rule__Store__Alternatives_1 : ( ( ( rule__Store__Group_1_0__0 ) ) | ( ( rule__Store__ValueAssignment_1_1 ) ) );
    public final void rule__Store__Alternatives_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:914:1: ( ( ( rule__Store__Group_1_0__0 ) ) | ( ( rule__Store__ValueAssignment_1_1 ) ) )
            int alt9=2;
            switch ( input.LA(1) ) {
            case RULE_STRING:
                {
                int LA9_1 = input.LA(2);

                if ( (LA9_1==34) ) {
                    alt9=2;
                }
                else if ( (LA9_1==38) ) {
                    alt9=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 9, 1, input);

                    throw nvae;
                }
                }
                break;
            case 23:
                {
                alt9=1;
                }
                break;
            case RULE_ID:
                {
                alt9=2;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }

            switch (alt9) {
                case 1 :
                    // InternalSeleniumDSL.g:915:2: ( ( rule__Store__Group_1_0__0 ) )
                    {
                    // InternalSeleniumDSL.g:915:2: ( ( rule__Store__Group_1_0__0 ) )
                    // InternalSeleniumDSL.g:916:3: ( rule__Store__Group_1_0__0 )
                    {
                     before(grammarAccess.getStoreAccess().getGroup_1_0()); 
                    // InternalSeleniumDSL.g:917:3: ( rule__Store__Group_1_0__0 )
                    // InternalSeleniumDSL.g:917:4: rule__Store__Group_1_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Store__Group_1_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getStoreAccess().getGroup_1_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSeleniumDSL.g:921:2: ( ( rule__Store__ValueAssignment_1_1 ) )
                    {
                    // InternalSeleniumDSL.g:921:2: ( ( rule__Store__ValueAssignment_1_1 ) )
                    // InternalSeleniumDSL.g:922:3: ( rule__Store__ValueAssignment_1_1 )
                    {
                     before(grammarAccess.getStoreAccess().getValueAssignment_1_1()); 
                    // InternalSeleniumDSL.g:923:3: ( rule__Store__ValueAssignment_1_1 )
                    // InternalSeleniumDSL.g:923:4: rule__Store__ValueAssignment_1_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Store__ValueAssignment_1_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getStoreAccess().getValueAssignment_1_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Store__Alternatives_1"


    // $ANTLR start "rule__SeleniumProgram__Group__0"
    // InternalSeleniumDSL.g:931:1: rule__SeleniumProgram__Group__0 : rule__SeleniumProgram__Group__0__Impl rule__SeleniumProgram__Group__1 ;
    public final void rule__SeleniumProgram__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:935:1: ( rule__SeleniumProgram__Group__0__Impl rule__SeleniumProgram__Group__1 )
            // InternalSeleniumDSL.g:936:2: rule__SeleniumProgram__Group__0__Impl rule__SeleniumProgram__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__SeleniumProgram__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SeleniumProgram__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SeleniumProgram__Group__0"


    // $ANTLR start "rule__SeleniumProgram__Group__0__Impl"
    // InternalSeleniumDSL.g:943:1: rule__SeleniumProgram__Group__0__Impl : ( ( rule__SeleniumProgram__FunctionsAssignment_0 )* ) ;
    public final void rule__SeleniumProgram__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:947:1: ( ( ( rule__SeleniumProgram__FunctionsAssignment_0 )* ) )
            // InternalSeleniumDSL.g:948:1: ( ( rule__SeleniumProgram__FunctionsAssignment_0 )* )
            {
            // InternalSeleniumDSL.g:948:1: ( ( rule__SeleniumProgram__FunctionsAssignment_0 )* )
            // InternalSeleniumDSL.g:949:2: ( rule__SeleniumProgram__FunctionsAssignment_0 )*
            {
             before(grammarAccess.getSeleniumProgramAccess().getFunctionsAssignment_0()); 
            // InternalSeleniumDSL.g:950:2: ( rule__SeleniumProgram__FunctionsAssignment_0 )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==43) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalSeleniumDSL.g:950:3: rule__SeleniumProgram__FunctionsAssignment_0
            	    {
            	    pushFollow(FOLLOW_4);
            	    rule__SeleniumProgram__FunctionsAssignment_0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

             after(grammarAccess.getSeleniumProgramAccess().getFunctionsAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SeleniumProgram__Group__0__Impl"


    // $ANTLR start "rule__SeleniumProgram__Group__1"
    // InternalSeleniumDSL.g:958:1: rule__SeleniumProgram__Group__1 : rule__SeleniumProgram__Group__1__Impl rule__SeleniumProgram__Group__2 ;
    public final void rule__SeleniumProgram__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:962:1: ( rule__SeleniumProgram__Group__1__Impl rule__SeleniumProgram__Group__2 )
            // InternalSeleniumDSL.g:963:2: rule__SeleniumProgram__Group__1__Impl rule__SeleniumProgram__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__SeleniumProgram__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SeleniumProgram__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SeleniumProgram__Group__1"


    // $ANTLR start "rule__SeleniumProgram__Group__1__Impl"
    // InternalSeleniumDSL.g:970:1: rule__SeleniumProgram__Group__1__Impl : ( ( rule__SeleniumProgram__OpenAssignment_1 ) ) ;
    public final void rule__SeleniumProgram__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:974:1: ( ( ( rule__SeleniumProgram__OpenAssignment_1 ) ) )
            // InternalSeleniumDSL.g:975:1: ( ( rule__SeleniumProgram__OpenAssignment_1 ) )
            {
            // InternalSeleniumDSL.g:975:1: ( ( rule__SeleniumProgram__OpenAssignment_1 ) )
            // InternalSeleniumDSL.g:976:2: ( rule__SeleniumProgram__OpenAssignment_1 )
            {
             before(grammarAccess.getSeleniumProgramAccess().getOpenAssignment_1()); 
            // InternalSeleniumDSL.g:977:2: ( rule__SeleniumProgram__OpenAssignment_1 )
            // InternalSeleniumDSL.g:977:3: rule__SeleniumProgram__OpenAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__SeleniumProgram__OpenAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getSeleniumProgramAccess().getOpenAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SeleniumProgram__Group__1__Impl"


    // $ANTLR start "rule__SeleniumProgram__Group__2"
    // InternalSeleniumDSL.g:985:1: rule__SeleniumProgram__Group__2 : rule__SeleniumProgram__Group__2__Impl rule__SeleniumProgram__Group__3 ;
    public final void rule__SeleniumProgram__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:989:1: ( rule__SeleniumProgram__Group__2__Impl rule__SeleniumProgram__Group__3 )
            // InternalSeleniumDSL.g:990:2: rule__SeleniumProgram__Group__2__Impl rule__SeleniumProgram__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__SeleniumProgram__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SeleniumProgram__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SeleniumProgram__Group__2"


    // $ANTLR start "rule__SeleniumProgram__Group__2__Impl"
    // InternalSeleniumDSL.g:997:1: rule__SeleniumProgram__Group__2__Impl : ( ( rule__SeleniumProgram__SeleniumInstructionAssignment_2 )* ) ;
    public final void rule__SeleniumProgram__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1001:1: ( ( ( rule__SeleniumProgram__SeleniumInstructionAssignment_2 )* ) )
            // InternalSeleniumDSL.g:1002:1: ( ( rule__SeleniumProgram__SeleniumInstructionAssignment_2 )* )
            {
            // InternalSeleniumDSL.g:1002:1: ( ( rule__SeleniumProgram__SeleniumInstructionAssignment_2 )* )
            // InternalSeleniumDSL.g:1003:2: ( rule__SeleniumProgram__SeleniumInstructionAssignment_2 )*
            {
             before(grammarAccess.getSeleniumProgramAccess().getSeleniumInstructionAssignment_2()); 
            // InternalSeleniumDSL.g:1004:2: ( rule__SeleniumProgram__SeleniumInstructionAssignment_2 )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==RULE_ID||(LA11_0>=27 && LA11_0<=28)||LA11_0==30||LA11_0==32||(LA11_0>=36 && LA11_0<=37)||LA11_0==39||(LA11_0>=41 && LA11_0<=42)||LA11_0==47) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalSeleniumDSL.g:1004:3: rule__SeleniumProgram__SeleniumInstructionAssignment_2
            	    {
            	    pushFollow(FOLLOW_6);
            	    rule__SeleniumProgram__SeleniumInstructionAssignment_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

             after(grammarAccess.getSeleniumProgramAccess().getSeleniumInstructionAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SeleniumProgram__Group__2__Impl"


    // $ANTLR start "rule__SeleniumProgram__Group__3"
    // InternalSeleniumDSL.g:1012:1: rule__SeleniumProgram__Group__3 : rule__SeleniumProgram__Group__3__Impl ;
    public final void rule__SeleniumProgram__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1016:1: ( rule__SeleniumProgram__Group__3__Impl )
            // InternalSeleniumDSL.g:1017:2: rule__SeleniumProgram__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SeleniumProgram__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SeleniumProgram__Group__3"


    // $ANTLR start "rule__SeleniumProgram__Group__3__Impl"
    // InternalSeleniumDSL.g:1023:1: rule__SeleniumProgram__Group__3__Impl : ( ( rule__SeleniumProgram__CloseAssignment_3 ) ) ;
    public final void rule__SeleniumProgram__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1027:1: ( ( ( rule__SeleniumProgram__CloseAssignment_3 ) ) )
            // InternalSeleniumDSL.g:1028:1: ( ( rule__SeleniumProgram__CloseAssignment_3 ) )
            {
            // InternalSeleniumDSL.g:1028:1: ( ( rule__SeleniumProgram__CloseAssignment_3 ) )
            // InternalSeleniumDSL.g:1029:2: ( rule__SeleniumProgram__CloseAssignment_3 )
            {
             before(grammarAccess.getSeleniumProgramAccess().getCloseAssignment_3()); 
            // InternalSeleniumDSL.g:1030:2: ( rule__SeleniumProgram__CloseAssignment_3 )
            // InternalSeleniumDSL.g:1030:3: rule__SeleniumProgram__CloseAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__SeleniumProgram__CloseAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getSeleniumProgramAccess().getCloseAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SeleniumProgram__Group__3__Impl"


    // $ANTLR start "rule__Open__Group__0"
    // InternalSeleniumDSL.g:1039:1: rule__Open__Group__0 : rule__Open__Group__0__Impl rule__Open__Group__1 ;
    public final void rule__Open__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1043:1: ( rule__Open__Group__0__Impl rule__Open__Group__1 )
            // InternalSeleniumDSL.g:1044:2: rule__Open__Group__0__Impl rule__Open__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__Open__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Open__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Open__Group__0"


    // $ANTLR start "rule__Open__Group__0__Impl"
    // InternalSeleniumDSL.g:1051:1: rule__Open__Group__0__Impl : ( 'open' ) ;
    public final void rule__Open__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1055:1: ( ( 'open' ) )
            // InternalSeleniumDSL.g:1056:1: ( 'open' )
            {
            // InternalSeleniumDSL.g:1056:1: ( 'open' )
            // InternalSeleniumDSL.g:1057:2: 'open'
            {
             before(grammarAccess.getOpenAccess().getOpenKeyword_0()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getOpenAccess().getOpenKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Open__Group__0__Impl"


    // $ANTLR start "rule__Open__Group__1"
    // InternalSeleniumDSL.g:1066:1: rule__Open__Group__1 : rule__Open__Group__1__Impl ;
    public final void rule__Open__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1070:1: ( rule__Open__Group__1__Impl )
            // InternalSeleniumDSL.g:1071:2: rule__Open__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Open__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Open__Group__1"


    // $ANTLR start "rule__Open__Group__1__Impl"
    // InternalSeleniumDSL.g:1077:1: rule__Open__Group__1__Impl : ( ( rule__Open__BrowserAssignment_1 ) ) ;
    public final void rule__Open__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1081:1: ( ( ( rule__Open__BrowserAssignment_1 ) ) )
            // InternalSeleniumDSL.g:1082:1: ( ( rule__Open__BrowserAssignment_1 ) )
            {
            // InternalSeleniumDSL.g:1082:1: ( ( rule__Open__BrowserAssignment_1 ) )
            // InternalSeleniumDSL.g:1083:2: ( rule__Open__BrowserAssignment_1 )
            {
             before(grammarAccess.getOpenAccess().getBrowserAssignment_1()); 
            // InternalSeleniumDSL.g:1084:2: ( rule__Open__BrowserAssignment_1 )
            // InternalSeleniumDSL.g:1084:3: rule__Open__BrowserAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Open__BrowserAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getOpenAccess().getBrowserAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Open__Group__1__Impl"


    // $ANTLR start "rule__Get__Group__0"
    // InternalSeleniumDSL.g:1093:1: rule__Get__Group__0 : rule__Get__Group__0__Impl rule__Get__Group__1 ;
    public final void rule__Get__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1097:1: ( rule__Get__Group__0__Impl rule__Get__Group__1 )
            // InternalSeleniumDSL.g:1098:2: rule__Get__Group__0__Impl rule__Get__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__Get__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Get__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Get__Group__0"


    // $ANTLR start "rule__Get__Group__0__Impl"
    // InternalSeleniumDSL.g:1105:1: rule__Get__Group__0__Impl : ( 'get' ) ;
    public final void rule__Get__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1109:1: ( ( 'get' ) )
            // InternalSeleniumDSL.g:1110:1: ( 'get' )
            {
            // InternalSeleniumDSL.g:1110:1: ( 'get' )
            // InternalSeleniumDSL.g:1111:2: 'get'
            {
             before(grammarAccess.getGetAccess().getGetKeyword_0()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getGetAccess().getGetKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Get__Group__0__Impl"


    // $ANTLR start "rule__Get__Group__1"
    // InternalSeleniumDSL.g:1120:1: rule__Get__Group__1 : rule__Get__Group__1__Impl ;
    public final void rule__Get__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1124:1: ( rule__Get__Group__1__Impl )
            // InternalSeleniumDSL.g:1125:2: rule__Get__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Get__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Get__Group__1"


    // $ANTLR start "rule__Get__Group__1__Impl"
    // InternalSeleniumDSL.g:1131:1: rule__Get__Group__1__Impl : ( ( rule__Get__UrlAssignment_1 ) ) ;
    public final void rule__Get__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1135:1: ( ( ( rule__Get__UrlAssignment_1 ) ) )
            // InternalSeleniumDSL.g:1136:1: ( ( rule__Get__UrlAssignment_1 ) )
            {
            // InternalSeleniumDSL.g:1136:1: ( ( rule__Get__UrlAssignment_1 ) )
            // InternalSeleniumDSL.g:1137:2: ( rule__Get__UrlAssignment_1 )
            {
             before(grammarAccess.getGetAccess().getUrlAssignment_1()); 
            // InternalSeleniumDSL.g:1138:2: ( rule__Get__UrlAssignment_1 )
            // InternalSeleniumDSL.g:1138:3: rule__Get__UrlAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Get__UrlAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getGetAccess().getUrlAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Get__Group__1__Impl"


    // $ANTLR start "rule__Condition__Group__0"
    // InternalSeleniumDSL.g:1147:1: rule__Condition__Group__0 : rule__Condition__Group__0__Impl rule__Condition__Group__1 ;
    public final void rule__Condition__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1151:1: ( rule__Condition__Group__0__Impl rule__Condition__Group__1 )
            // InternalSeleniumDSL.g:1152:2: rule__Condition__Group__0__Impl rule__Condition__Group__1
            {
            pushFollow(FOLLOW_9);
            rule__Condition__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Condition__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group__0"


    // $ANTLR start "rule__Condition__Group__0__Impl"
    // InternalSeleniumDSL.g:1159:1: rule__Condition__Group__0__Impl : ( 'where' ) ;
    public final void rule__Condition__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1163:1: ( ( 'where' ) )
            // InternalSeleniumDSL.g:1164:1: ( 'where' )
            {
            // InternalSeleniumDSL.g:1164:1: ( 'where' )
            // InternalSeleniumDSL.g:1165:2: 'where'
            {
             before(grammarAccess.getConditionAccess().getWhereKeyword_0()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getConditionAccess().getWhereKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group__0__Impl"


    // $ANTLR start "rule__Condition__Group__1"
    // InternalSeleniumDSL.g:1174:1: rule__Condition__Group__1 : rule__Condition__Group__1__Impl rule__Condition__Group__2 ;
    public final void rule__Condition__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1178:1: ( rule__Condition__Group__1__Impl rule__Condition__Group__2 )
            // InternalSeleniumDSL.g:1179:2: rule__Condition__Group__1__Impl rule__Condition__Group__2
            {
            pushFollow(FOLLOW_10);
            rule__Condition__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Condition__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group__1"


    // $ANTLR start "rule__Condition__Group__1__Impl"
    // InternalSeleniumDSL.g:1186:1: rule__Condition__Group__1__Impl : ( ( rule__Condition__AttributeCheckAssignment_1 ) ) ;
    public final void rule__Condition__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1190:1: ( ( ( rule__Condition__AttributeCheckAssignment_1 ) ) )
            // InternalSeleniumDSL.g:1191:1: ( ( rule__Condition__AttributeCheckAssignment_1 ) )
            {
            // InternalSeleniumDSL.g:1191:1: ( ( rule__Condition__AttributeCheckAssignment_1 ) )
            // InternalSeleniumDSL.g:1192:2: ( rule__Condition__AttributeCheckAssignment_1 )
            {
             before(grammarAccess.getConditionAccess().getAttributeCheckAssignment_1()); 
            // InternalSeleniumDSL.g:1193:2: ( rule__Condition__AttributeCheckAssignment_1 )
            // InternalSeleniumDSL.g:1193:3: rule__Condition__AttributeCheckAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Condition__AttributeCheckAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getConditionAccess().getAttributeCheckAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group__1__Impl"


    // $ANTLR start "rule__Condition__Group__2"
    // InternalSeleniumDSL.g:1201:1: rule__Condition__Group__2 : rule__Condition__Group__2__Impl rule__Condition__Group__3 ;
    public final void rule__Condition__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1205:1: ( rule__Condition__Group__2__Impl rule__Condition__Group__3 )
            // InternalSeleniumDSL.g:1206:2: rule__Condition__Group__2__Impl rule__Condition__Group__3
            {
            pushFollow(FOLLOW_8);
            rule__Condition__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Condition__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group__2"


    // $ANTLR start "rule__Condition__Group__2__Impl"
    // InternalSeleniumDSL.g:1213:1: rule__Condition__Group__2__Impl : ( ( rule__Condition__OperatorAssignment_2 ) ) ;
    public final void rule__Condition__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1217:1: ( ( ( rule__Condition__OperatorAssignment_2 ) ) )
            // InternalSeleniumDSL.g:1218:1: ( ( rule__Condition__OperatorAssignment_2 ) )
            {
            // InternalSeleniumDSL.g:1218:1: ( ( rule__Condition__OperatorAssignment_2 ) )
            // InternalSeleniumDSL.g:1219:2: ( rule__Condition__OperatorAssignment_2 )
            {
             before(grammarAccess.getConditionAccess().getOperatorAssignment_2()); 
            // InternalSeleniumDSL.g:1220:2: ( rule__Condition__OperatorAssignment_2 )
            // InternalSeleniumDSL.g:1220:3: rule__Condition__OperatorAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Condition__OperatorAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getConditionAccess().getOperatorAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group__2__Impl"


    // $ANTLR start "rule__Condition__Group__3"
    // InternalSeleniumDSL.g:1228:1: rule__Condition__Group__3 : rule__Condition__Group__3__Impl ;
    public final void rule__Condition__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1232:1: ( rule__Condition__Group__3__Impl )
            // InternalSeleniumDSL.g:1233:2: rule__Condition__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Condition__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group__3"


    // $ANTLR start "rule__Condition__Group__3__Impl"
    // InternalSeleniumDSL.g:1239:1: rule__Condition__Group__3__Impl : ( ( rule__Condition__ValueAssignment_3 ) ) ;
    public final void rule__Condition__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1243:1: ( ( ( rule__Condition__ValueAssignment_3 ) ) )
            // InternalSeleniumDSL.g:1244:1: ( ( rule__Condition__ValueAssignment_3 ) )
            {
            // InternalSeleniumDSL.g:1244:1: ( ( rule__Condition__ValueAssignment_3 ) )
            // InternalSeleniumDSL.g:1245:2: ( rule__Condition__ValueAssignment_3 )
            {
             before(grammarAccess.getConditionAccess().getValueAssignment_3()); 
            // InternalSeleniumDSL.g:1246:2: ( rule__Condition__ValueAssignment_3 )
            // InternalSeleniumDSL.g:1246:3: rule__Condition__ValueAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Condition__ValueAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getConditionAccess().getValueAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group__3__Impl"


    // $ANTLR start "rule__Select__Group__0"
    // InternalSeleniumDSL.g:1255:1: rule__Select__Group__0 : rule__Select__Group__0__Impl rule__Select__Group__1 ;
    public final void rule__Select__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1259:1: ( rule__Select__Group__0__Impl rule__Select__Group__1 )
            // InternalSeleniumDSL.g:1260:2: rule__Select__Group__0__Impl rule__Select__Group__1
            {
            pushFollow(FOLLOW_11);
            rule__Select__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Select__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Select__Group__0"


    // $ANTLR start "rule__Select__Group__0__Impl"
    // InternalSeleniumDSL.g:1267:1: rule__Select__Group__0__Impl : ( 'select' ) ;
    public final void rule__Select__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1271:1: ( ( 'select' ) )
            // InternalSeleniumDSL.g:1272:1: ( 'select' )
            {
            // InternalSeleniumDSL.g:1272:1: ( 'select' )
            // InternalSeleniumDSL.g:1273:2: 'select'
            {
             before(grammarAccess.getSelectAccess().getSelectKeyword_0()); 
            match(input,32,FOLLOW_2); 
             after(grammarAccess.getSelectAccess().getSelectKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Select__Group__0__Impl"


    // $ANTLR start "rule__Select__Group__1"
    // InternalSeleniumDSL.g:1282:1: rule__Select__Group__1 : rule__Select__Group__1__Impl rule__Select__Group__2 ;
    public final void rule__Select__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1286:1: ( rule__Select__Group__1__Impl rule__Select__Group__2 )
            // InternalSeleniumDSL.g:1287:2: rule__Select__Group__1__Impl rule__Select__Group__2
            {
            pushFollow(FOLLOW_12);
            rule__Select__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Select__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Select__Group__1"


    // $ANTLR start "rule__Select__Group__1__Impl"
    // InternalSeleniumDSL.g:1294:1: rule__Select__Group__1__Impl : ( 'option' ) ;
    public final void rule__Select__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1298:1: ( ( 'option' ) )
            // InternalSeleniumDSL.g:1299:1: ( 'option' )
            {
            // InternalSeleniumDSL.g:1299:1: ( 'option' )
            // InternalSeleniumDSL.g:1300:2: 'option'
            {
             before(grammarAccess.getSelectAccess().getOptionKeyword_1()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getSelectAccess().getOptionKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Select__Group__1__Impl"


    // $ANTLR start "rule__Select__Group__2"
    // InternalSeleniumDSL.g:1309:1: rule__Select__Group__2 : rule__Select__Group__2__Impl rule__Select__Group__3 ;
    public final void rule__Select__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1313:1: ( rule__Select__Group__2__Impl rule__Select__Group__3 )
            // InternalSeleniumDSL.g:1314:2: rule__Select__Group__2__Impl rule__Select__Group__3
            {
            pushFollow(FOLLOW_8);
            rule__Select__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Select__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Select__Group__2"


    // $ANTLR start "rule__Select__Group__2__Impl"
    // InternalSeleniumDSL.g:1321:1: rule__Select__Group__2__Impl : ( ( rule__Select__TypeComboSelectionAssignment_2 ) ) ;
    public final void rule__Select__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1325:1: ( ( ( rule__Select__TypeComboSelectionAssignment_2 ) ) )
            // InternalSeleniumDSL.g:1326:1: ( ( rule__Select__TypeComboSelectionAssignment_2 ) )
            {
            // InternalSeleniumDSL.g:1326:1: ( ( rule__Select__TypeComboSelectionAssignment_2 ) )
            // InternalSeleniumDSL.g:1327:2: ( rule__Select__TypeComboSelectionAssignment_2 )
            {
             before(grammarAccess.getSelectAccess().getTypeComboSelectionAssignment_2()); 
            // InternalSeleniumDSL.g:1328:2: ( rule__Select__TypeComboSelectionAssignment_2 )
            // InternalSeleniumDSL.g:1328:3: rule__Select__TypeComboSelectionAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Select__TypeComboSelectionAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getSelectAccess().getTypeComboSelectionAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Select__Group__2__Impl"


    // $ANTLR start "rule__Select__Group__3"
    // InternalSeleniumDSL.g:1336:1: rule__Select__Group__3 : rule__Select__Group__3__Impl rule__Select__Group__4 ;
    public final void rule__Select__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1340:1: ( rule__Select__Group__3__Impl rule__Select__Group__4 )
            // InternalSeleniumDSL.g:1341:2: rule__Select__Group__3__Impl rule__Select__Group__4
            {
            pushFollow(FOLLOW_13);
            rule__Select__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Select__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Select__Group__3"


    // $ANTLR start "rule__Select__Group__3__Impl"
    // InternalSeleniumDSL.g:1348:1: rule__Select__Group__3__Impl : ( ( rule__Select__ValueAssignment_3 ) ) ;
    public final void rule__Select__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1352:1: ( ( ( rule__Select__ValueAssignment_3 ) ) )
            // InternalSeleniumDSL.g:1353:1: ( ( rule__Select__ValueAssignment_3 ) )
            {
            // InternalSeleniumDSL.g:1353:1: ( ( rule__Select__ValueAssignment_3 ) )
            // InternalSeleniumDSL.g:1354:2: ( rule__Select__ValueAssignment_3 )
            {
             before(grammarAccess.getSelectAccess().getValueAssignment_3()); 
            // InternalSeleniumDSL.g:1355:2: ( rule__Select__ValueAssignment_3 )
            // InternalSeleniumDSL.g:1355:3: rule__Select__ValueAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Select__ValueAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getSelectAccess().getValueAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Select__Group__3__Impl"


    // $ANTLR start "rule__Select__Group__4"
    // InternalSeleniumDSL.g:1363:1: rule__Select__Group__4 : rule__Select__Group__4__Impl rule__Select__Group__5 ;
    public final void rule__Select__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1367:1: ( rule__Select__Group__4__Impl rule__Select__Group__5 )
            // InternalSeleniumDSL.g:1368:2: rule__Select__Group__4__Impl rule__Select__Group__5
            {
            pushFollow(FOLLOW_14);
            rule__Select__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Select__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Select__Group__4"


    // $ANTLR start "rule__Select__Group__4__Impl"
    // InternalSeleniumDSL.g:1375:1: rule__Select__Group__4__Impl : ( 'in' ) ;
    public final void rule__Select__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1379:1: ( ( 'in' ) )
            // InternalSeleniumDSL.g:1380:1: ( 'in' )
            {
            // InternalSeleniumDSL.g:1380:1: ( 'in' )
            // InternalSeleniumDSL.g:1381:2: 'in'
            {
             before(grammarAccess.getSelectAccess().getInKeyword_4()); 
            match(input,34,FOLLOW_2); 
             after(grammarAccess.getSelectAccess().getInKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Select__Group__4__Impl"


    // $ANTLR start "rule__Select__Group__5"
    // InternalSeleniumDSL.g:1390:1: rule__Select__Group__5 : rule__Select__Group__5__Impl rule__Select__Group__6 ;
    public final void rule__Select__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1394:1: ( rule__Select__Group__5__Impl rule__Select__Group__6 )
            // InternalSeleniumDSL.g:1395:2: rule__Select__Group__5__Impl rule__Select__Group__6
            {
            pushFollow(FOLLOW_15);
            rule__Select__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Select__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Select__Group__5"


    // $ANTLR start "rule__Select__Group__5__Impl"
    // InternalSeleniumDSL.g:1402:1: rule__Select__Group__5__Impl : ( 'combo' ) ;
    public final void rule__Select__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1406:1: ( ( 'combo' ) )
            // InternalSeleniumDSL.g:1407:1: ( 'combo' )
            {
            // InternalSeleniumDSL.g:1407:1: ( 'combo' )
            // InternalSeleniumDSL.g:1408:2: 'combo'
            {
             before(grammarAccess.getSelectAccess().getComboKeyword_5()); 
            match(input,35,FOLLOW_2); 
             after(grammarAccess.getSelectAccess().getComboKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Select__Group__5__Impl"


    // $ANTLR start "rule__Select__Group__6"
    // InternalSeleniumDSL.g:1417:1: rule__Select__Group__6 : rule__Select__Group__6__Impl ;
    public final void rule__Select__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1421:1: ( rule__Select__Group__6__Impl )
            // InternalSeleniumDSL.g:1422:2: rule__Select__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Select__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Select__Group__6"


    // $ANTLR start "rule__Select__Group__6__Impl"
    // InternalSeleniumDSL.g:1428:1: rule__Select__Group__6__Impl : ( ( rule__Select__ConditionAssignment_6 ) ) ;
    public final void rule__Select__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1432:1: ( ( ( rule__Select__ConditionAssignment_6 ) ) )
            // InternalSeleniumDSL.g:1433:1: ( ( rule__Select__ConditionAssignment_6 ) )
            {
            // InternalSeleniumDSL.g:1433:1: ( ( rule__Select__ConditionAssignment_6 ) )
            // InternalSeleniumDSL.g:1434:2: ( rule__Select__ConditionAssignment_6 )
            {
             before(grammarAccess.getSelectAccess().getConditionAssignment_6()); 
            // InternalSeleniumDSL.g:1435:2: ( rule__Select__ConditionAssignment_6 )
            // InternalSeleniumDSL.g:1435:3: rule__Select__ConditionAssignment_6
            {
            pushFollow(FOLLOW_2);
            rule__Select__ConditionAssignment_6();

            state._fsp--;


            }

             after(grammarAccess.getSelectAccess().getConditionAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Select__Group__6__Impl"


    // $ANTLR start "rule__Check__Group__0"
    // InternalSeleniumDSL.g:1444:1: rule__Check__Group__0 : rule__Check__Group__0__Impl rule__Check__Group__1 ;
    public final void rule__Check__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1448:1: ( rule__Check__Group__0__Impl rule__Check__Group__1 )
            // InternalSeleniumDSL.g:1449:2: rule__Check__Group__0__Impl rule__Check__Group__1
            {
            pushFollow(FOLLOW_16);
            rule__Check__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Check__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Check__Group__0"


    // $ANTLR start "rule__Check__Group__0__Impl"
    // InternalSeleniumDSL.g:1456:1: rule__Check__Group__0__Impl : ( ( rule__Check__ToCheckAssignment_0 ) ) ;
    public final void rule__Check__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1460:1: ( ( ( rule__Check__ToCheckAssignment_0 ) ) )
            // InternalSeleniumDSL.g:1461:1: ( ( rule__Check__ToCheckAssignment_0 ) )
            {
            // InternalSeleniumDSL.g:1461:1: ( ( rule__Check__ToCheckAssignment_0 ) )
            // InternalSeleniumDSL.g:1462:2: ( rule__Check__ToCheckAssignment_0 )
            {
             before(grammarAccess.getCheckAccess().getToCheckAssignment_0()); 
            // InternalSeleniumDSL.g:1463:2: ( rule__Check__ToCheckAssignment_0 )
            // InternalSeleniumDSL.g:1463:3: rule__Check__ToCheckAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Check__ToCheckAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getCheckAccess().getToCheckAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Check__Group__0__Impl"


    // $ANTLR start "rule__Check__Group__1"
    // InternalSeleniumDSL.g:1471:1: rule__Check__Group__1 : rule__Check__Group__1__Impl rule__Check__Group__2 ;
    public final void rule__Check__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1475:1: ( rule__Check__Group__1__Impl rule__Check__Group__2 )
            // InternalSeleniumDSL.g:1476:2: rule__Check__Group__1__Impl rule__Check__Group__2
            {
            pushFollow(FOLLOW_16);
            rule__Check__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Check__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Check__Group__1"


    // $ANTLR start "rule__Check__Group__1__Impl"
    // InternalSeleniumDSL.g:1483:1: rule__Check__Group__1__Impl : ( ( rule__Check__AllAssignment_1 )? ) ;
    public final void rule__Check__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1487:1: ( ( ( rule__Check__AllAssignment_1 )? ) )
            // InternalSeleniumDSL.g:1488:1: ( ( rule__Check__AllAssignment_1 )? )
            {
            // InternalSeleniumDSL.g:1488:1: ( ( rule__Check__AllAssignment_1 )? )
            // InternalSeleniumDSL.g:1489:2: ( rule__Check__AllAssignment_1 )?
            {
             before(grammarAccess.getCheckAccess().getAllAssignment_1()); 
            // InternalSeleniumDSL.g:1490:2: ( rule__Check__AllAssignment_1 )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==48) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalSeleniumDSL.g:1490:3: rule__Check__AllAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Check__AllAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getCheckAccess().getAllAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Check__Group__1__Impl"


    // $ANTLR start "rule__Check__Group__2"
    // InternalSeleniumDSL.g:1498:1: rule__Check__Group__2 : rule__Check__Group__2__Impl rule__Check__Group__3 ;
    public final void rule__Check__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1502:1: ( rule__Check__Group__2__Impl rule__Check__Group__3 )
            // InternalSeleniumDSL.g:1503:2: rule__Check__Group__2__Impl rule__Check__Group__3
            {
            pushFollow(FOLLOW_15);
            rule__Check__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Check__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Check__Group__2"


    // $ANTLR start "rule__Check__Group__2__Impl"
    // InternalSeleniumDSL.g:1510:1: rule__Check__Group__2__Impl : ( ( rule__Check__TargetTypeAssignment_2 ) ) ;
    public final void rule__Check__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1514:1: ( ( ( rule__Check__TargetTypeAssignment_2 ) ) )
            // InternalSeleniumDSL.g:1515:1: ( ( rule__Check__TargetTypeAssignment_2 ) )
            {
            // InternalSeleniumDSL.g:1515:1: ( ( rule__Check__TargetTypeAssignment_2 ) )
            // InternalSeleniumDSL.g:1516:2: ( rule__Check__TargetTypeAssignment_2 )
            {
             before(grammarAccess.getCheckAccess().getTargetTypeAssignment_2()); 
            // InternalSeleniumDSL.g:1517:2: ( rule__Check__TargetTypeAssignment_2 )
            // InternalSeleniumDSL.g:1517:3: rule__Check__TargetTypeAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Check__TargetTypeAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getCheckAccess().getTargetTypeAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Check__Group__2__Impl"


    // $ANTLR start "rule__Check__Group__3"
    // InternalSeleniumDSL.g:1525:1: rule__Check__Group__3 : rule__Check__Group__3__Impl ;
    public final void rule__Check__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1529:1: ( rule__Check__Group__3__Impl )
            // InternalSeleniumDSL.g:1530:2: rule__Check__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Check__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Check__Group__3"


    // $ANTLR start "rule__Check__Group__3__Impl"
    // InternalSeleniumDSL.g:1536:1: rule__Check__Group__3__Impl : ( ( rule__Check__ConditionAssignment_3 )? ) ;
    public final void rule__Check__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1540:1: ( ( ( rule__Check__ConditionAssignment_3 )? ) )
            // InternalSeleniumDSL.g:1541:1: ( ( rule__Check__ConditionAssignment_3 )? )
            {
            // InternalSeleniumDSL.g:1541:1: ( ( rule__Check__ConditionAssignment_3 )? )
            // InternalSeleniumDSL.g:1542:2: ( rule__Check__ConditionAssignment_3 )?
            {
             before(grammarAccess.getCheckAccess().getConditionAssignment_3()); 
            // InternalSeleniumDSL.g:1543:2: ( rule__Check__ConditionAssignment_3 )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==31) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalSeleniumDSL.g:1543:3: rule__Check__ConditionAssignment_3
                    {
                    pushFollow(FOLLOW_2);
                    rule__Check__ConditionAssignment_3();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getCheckAccess().getConditionAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Check__Group__3__Impl"


    // $ANTLR start "rule__Click__Group__0"
    // InternalSeleniumDSL.g:1552:1: rule__Click__Group__0 : rule__Click__Group__0__Impl rule__Click__Group__1 ;
    public final void rule__Click__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1556:1: ( rule__Click__Group__0__Impl rule__Click__Group__1 )
            // InternalSeleniumDSL.g:1557:2: rule__Click__Group__0__Impl rule__Click__Group__1
            {
            pushFollow(FOLLOW_16);
            rule__Click__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Click__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Click__Group__0"


    // $ANTLR start "rule__Click__Group__0__Impl"
    // InternalSeleniumDSL.g:1564:1: rule__Click__Group__0__Impl : ( 'click' ) ;
    public final void rule__Click__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1568:1: ( ( 'click' ) )
            // InternalSeleniumDSL.g:1569:1: ( 'click' )
            {
            // InternalSeleniumDSL.g:1569:1: ( 'click' )
            // InternalSeleniumDSL.g:1570:2: 'click'
            {
             before(grammarAccess.getClickAccess().getClickKeyword_0()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getClickAccess().getClickKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Click__Group__0__Impl"


    // $ANTLR start "rule__Click__Group__1"
    // InternalSeleniumDSL.g:1579:1: rule__Click__Group__1 : rule__Click__Group__1__Impl rule__Click__Group__2 ;
    public final void rule__Click__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1583:1: ( rule__Click__Group__1__Impl rule__Click__Group__2 )
            // InternalSeleniumDSL.g:1584:2: rule__Click__Group__1__Impl rule__Click__Group__2
            {
            pushFollow(FOLLOW_16);
            rule__Click__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Click__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Click__Group__1"


    // $ANTLR start "rule__Click__Group__1__Impl"
    // InternalSeleniumDSL.g:1591:1: rule__Click__Group__1__Impl : ( ( rule__Click__AllAssignment_1 )? ) ;
    public final void rule__Click__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1595:1: ( ( ( rule__Click__AllAssignment_1 )? ) )
            // InternalSeleniumDSL.g:1596:1: ( ( rule__Click__AllAssignment_1 )? )
            {
            // InternalSeleniumDSL.g:1596:1: ( ( rule__Click__AllAssignment_1 )? )
            // InternalSeleniumDSL.g:1597:2: ( rule__Click__AllAssignment_1 )?
            {
             before(grammarAccess.getClickAccess().getAllAssignment_1()); 
            // InternalSeleniumDSL.g:1598:2: ( rule__Click__AllAssignment_1 )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==48) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalSeleniumDSL.g:1598:3: rule__Click__AllAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Click__AllAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getClickAccess().getAllAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Click__Group__1__Impl"


    // $ANTLR start "rule__Click__Group__2"
    // InternalSeleniumDSL.g:1606:1: rule__Click__Group__2 : rule__Click__Group__2__Impl rule__Click__Group__3 ;
    public final void rule__Click__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1610:1: ( rule__Click__Group__2__Impl rule__Click__Group__3 )
            // InternalSeleniumDSL.g:1611:2: rule__Click__Group__2__Impl rule__Click__Group__3
            {
            pushFollow(FOLLOW_15);
            rule__Click__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Click__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Click__Group__2"


    // $ANTLR start "rule__Click__Group__2__Impl"
    // InternalSeleniumDSL.g:1618:1: rule__Click__Group__2__Impl : ( ( rule__Click__TargetTypeAssignment_2 ) ) ;
    public final void rule__Click__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1622:1: ( ( ( rule__Click__TargetTypeAssignment_2 ) ) )
            // InternalSeleniumDSL.g:1623:1: ( ( rule__Click__TargetTypeAssignment_2 ) )
            {
            // InternalSeleniumDSL.g:1623:1: ( ( rule__Click__TargetTypeAssignment_2 ) )
            // InternalSeleniumDSL.g:1624:2: ( rule__Click__TargetTypeAssignment_2 )
            {
             before(grammarAccess.getClickAccess().getTargetTypeAssignment_2()); 
            // InternalSeleniumDSL.g:1625:2: ( rule__Click__TargetTypeAssignment_2 )
            // InternalSeleniumDSL.g:1625:3: rule__Click__TargetTypeAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Click__TargetTypeAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getClickAccess().getTargetTypeAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Click__Group__2__Impl"


    // $ANTLR start "rule__Click__Group__3"
    // InternalSeleniumDSL.g:1633:1: rule__Click__Group__3 : rule__Click__Group__3__Impl ;
    public final void rule__Click__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1637:1: ( rule__Click__Group__3__Impl )
            // InternalSeleniumDSL.g:1638:2: rule__Click__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Click__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Click__Group__3"


    // $ANTLR start "rule__Click__Group__3__Impl"
    // InternalSeleniumDSL.g:1644:1: rule__Click__Group__3__Impl : ( ( rule__Click__ConditionAssignment_3 ) ) ;
    public final void rule__Click__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1648:1: ( ( ( rule__Click__ConditionAssignment_3 ) ) )
            // InternalSeleniumDSL.g:1649:1: ( ( rule__Click__ConditionAssignment_3 ) )
            {
            // InternalSeleniumDSL.g:1649:1: ( ( rule__Click__ConditionAssignment_3 ) )
            // InternalSeleniumDSL.g:1650:2: ( rule__Click__ConditionAssignment_3 )
            {
             before(grammarAccess.getClickAccess().getConditionAssignment_3()); 
            // InternalSeleniumDSL.g:1651:2: ( rule__Click__ConditionAssignment_3 )
            // InternalSeleniumDSL.g:1651:3: rule__Click__ConditionAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Click__ConditionAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getClickAccess().getConditionAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Click__Group__3__Impl"


    // $ANTLR start "rule__Store__Group__0"
    // InternalSeleniumDSL.g:1660:1: rule__Store__Group__0 : rule__Store__Group__0__Impl rule__Store__Group__1 ;
    public final void rule__Store__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1664:1: ( rule__Store__Group__0__Impl rule__Store__Group__1 )
            // InternalSeleniumDSL.g:1665:2: rule__Store__Group__0__Impl rule__Store__Group__1
            {
            pushFollow(FOLLOW_17);
            rule__Store__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Store__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Store__Group__0"


    // $ANTLR start "rule__Store__Group__0__Impl"
    // InternalSeleniumDSL.g:1672:1: rule__Store__Group__0__Impl : ( 'store' ) ;
    public final void rule__Store__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1676:1: ( ( 'store' ) )
            // InternalSeleniumDSL.g:1677:1: ( 'store' )
            {
            // InternalSeleniumDSL.g:1677:1: ( 'store' )
            // InternalSeleniumDSL.g:1678:2: 'store'
            {
             before(grammarAccess.getStoreAccess().getStoreKeyword_0()); 
            match(input,37,FOLLOW_2); 
             after(grammarAccess.getStoreAccess().getStoreKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Store__Group__0__Impl"


    // $ANTLR start "rule__Store__Group__1"
    // InternalSeleniumDSL.g:1687:1: rule__Store__Group__1 : rule__Store__Group__1__Impl rule__Store__Group__2 ;
    public final void rule__Store__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1691:1: ( rule__Store__Group__1__Impl rule__Store__Group__2 )
            // InternalSeleniumDSL.g:1692:2: rule__Store__Group__1__Impl rule__Store__Group__2
            {
            pushFollow(FOLLOW_13);
            rule__Store__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Store__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Store__Group__1"


    // $ANTLR start "rule__Store__Group__1__Impl"
    // InternalSeleniumDSL.g:1699:1: rule__Store__Group__1__Impl : ( ( rule__Store__Alternatives_1 ) ) ;
    public final void rule__Store__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1703:1: ( ( ( rule__Store__Alternatives_1 ) ) )
            // InternalSeleniumDSL.g:1704:1: ( ( rule__Store__Alternatives_1 ) )
            {
            // InternalSeleniumDSL.g:1704:1: ( ( rule__Store__Alternatives_1 ) )
            // InternalSeleniumDSL.g:1705:2: ( rule__Store__Alternatives_1 )
            {
             before(grammarAccess.getStoreAccess().getAlternatives_1()); 
            // InternalSeleniumDSL.g:1706:2: ( rule__Store__Alternatives_1 )
            // InternalSeleniumDSL.g:1706:3: rule__Store__Alternatives_1
            {
            pushFollow(FOLLOW_2);
            rule__Store__Alternatives_1();

            state._fsp--;


            }

             after(grammarAccess.getStoreAccess().getAlternatives_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Store__Group__1__Impl"


    // $ANTLR start "rule__Store__Group__2"
    // InternalSeleniumDSL.g:1714:1: rule__Store__Group__2 : rule__Store__Group__2__Impl rule__Store__Group__3 ;
    public final void rule__Store__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1718:1: ( rule__Store__Group__2__Impl rule__Store__Group__3 )
            // InternalSeleniumDSL.g:1719:2: rule__Store__Group__2__Impl rule__Store__Group__3
            {
            pushFollow(FOLLOW_18);
            rule__Store__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Store__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Store__Group__2"


    // $ANTLR start "rule__Store__Group__2__Impl"
    // InternalSeleniumDSL.g:1726:1: rule__Store__Group__2__Impl : ( 'in' ) ;
    public final void rule__Store__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1730:1: ( ( 'in' ) )
            // InternalSeleniumDSL.g:1731:1: ( 'in' )
            {
            // InternalSeleniumDSL.g:1731:1: ( 'in' )
            // InternalSeleniumDSL.g:1732:2: 'in'
            {
             before(grammarAccess.getStoreAccess().getInKeyword_2()); 
            match(input,34,FOLLOW_2); 
             after(grammarAccess.getStoreAccess().getInKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Store__Group__2__Impl"


    // $ANTLR start "rule__Store__Group__3"
    // InternalSeleniumDSL.g:1741:1: rule__Store__Group__3 : rule__Store__Group__3__Impl ;
    public final void rule__Store__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1745:1: ( rule__Store__Group__3__Impl )
            // InternalSeleniumDSL.g:1746:2: rule__Store__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Store__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Store__Group__3"


    // $ANTLR start "rule__Store__Group__3__Impl"
    // InternalSeleniumDSL.g:1752:1: rule__Store__Group__3__Impl : ( ( rule__Store__VariableNameAssignment_3 ) ) ;
    public final void rule__Store__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1756:1: ( ( ( rule__Store__VariableNameAssignment_3 ) ) )
            // InternalSeleniumDSL.g:1757:1: ( ( rule__Store__VariableNameAssignment_3 ) )
            {
            // InternalSeleniumDSL.g:1757:1: ( ( rule__Store__VariableNameAssignment_3 ) )
            // InternalSeleniumDSL.g:1758:2: ( rule__Store__VariableNameAssignment_3 )
            {
             before(grammarAccess.getStoreAccess().getVariableNameAssignment_3()); 
            // InternalSeleniumDSL.g:1759:2: ( rule__Store__VariableNameAssignment_3 )
            // InternalSeleniumDSL.g:1759:3: rule__Store__VariableNameAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Store__VariableNameAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getStoreAccess().getVariableNameAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Store__Group__3__Impl"


    // $ANTLR start "rule__Store__Group_1_0__0"
    // InternalSeleniumDSL.g:1768:1: rule__Store__Group_1_0__0 : rule__Store__Group_1_0__0__Impl rule__Store__Group_1_0__1 ;
    public final void rule__Store__Group_1_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1772:1: ( rule__Store__Group_1_0__0__Impl rule__Store__Group_1_0__1 )
            // InternalSeleniumDSL.g:1773:2: rule__Store__Group_1_0__0__Impl rule__Store__Group_1_0__1
            {
            pushFollow(FOLLOW_19);
            rule__Store__Group_1_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Store__Group_1_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Store__Group_1_0__0"


    // $ANTLR start "rule__Store__Group_1_0__0__Impl"
    // InternalSeleniumDSL.g:1780:1: rule__Store__Group_1_0__0__Impl : ( ( rule__Store__AttributeCheckAssignment_1_0_0 ) ) ;
    public final void rule__Store__Group_1_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1784:1: ( ( ( rule__Store__AttributeCheckAssignment_1_0_0 ) ) )
            // InternalSeleniumDSL.g:1785:1: ( ( rule__Store__AttributeCheckAssignment_1_0_0 ) )
            {
            // InternalSeleniumDSL.g:1785:1: ( ( rule__Store__AttributeCheckAssignment_1_0_0 ) )
            // InternalSeleniumDSL.g:1786:2: ( rule__Store__AttributeCheckAssignment_1_0_0 )
            {
             before(grammarAccess.getStoreAccess().getAttributeCheckAssignment_1_0_0()); 
            // InternalSeleniumDSL.g:1787:2: ( rule__Store__AttributeCheckAssignment_1_0_0 )
            // InternalSeleniumDSL.g:1787:3: rule__Store__AttributeCheckAssignment_1_0_0
            {
            pushFollow(FOLLOW_2);
            rule__Store__AttributeCheckAssignment_1_0_0();

            state._fsp--;


            }

             after(grammarAccess.getStoreAccess().getAttributeCheckAssignment_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Store__Group_1_0__0__Impl"


    // $ANTLR start "rule__Store__Group_1_0__1"
    // InternalSeleniumDSL.g:1795:1: rule__Store__Group_1_0__1 : rule__Store__Group_1_0__1__Impl rule__Store__Group_1_0__2 ;
    public final void rule__Store__Group_1_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1799:1: ( rule__Store__Group_1_0__1__Impl rule__Store__Group_1_0__2 )
            // InternalSeleniumDSL.g:1800:2: rule__Store__Group_1_0__1__Impl rule__Store__Group_1_0__2
            {
            pushFollow(FOLLOW_16);
            rule__Store__Group_1_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Store__Group_1_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Store__Group_1_0__1"


    // $ANTLR start "rule__Store__Group_1_0__1__Impl"
    // InternalSeleniumDSL.g:1807:1: rule__Store__Group_1_0__1__Impl : ( 'of' ) ;
    public final void rule__Store__Group_1_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1811:1: ( ( 'of' ) )
            // InternalSeleniumDSL.g:1812:1: ( 'of' )
            {
            // InternalSeleniumDSL.g:1812:1: ( 'of' )
            // InternalSeleniumDSL.g:1813:2: 'of'
            {
             before(grammarAccess.getStoreAccess().getOfKeyword_1_0_1()); 
            match(input,38,FOLLOW_2); 
             after(grammarAccess.getStoreAccess().getOfKeyword_1_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Store__Group_1_0__1__Impl"


    // $ANTLR start "rule__Store__Group_1_0__2"
    // InternalSeleniumDSL.g:1822:1: rule__Store__Group_1_0__2 : rule__Store__Group_1_0__2__Impl rule__Store__Group_1_0__3 ;
    public final void rule__Store__Group_1_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1826:1: ( rule__Store__Group_1_0__2__Impl rule__Store__Group_1_0__3 )
            // InternalSeleniumDSL.g:1827:2: rule__Store__Group_1_0__2__Impl rule__Store__Group_1_0__3
            {
            pushFollow(FOLLOW_15);
            rule__Store__Group_1_0__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Store__Group_1_0__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Store__Group_1_0__2"


    // $ANTLR start "rule__Store__Group_1_0__2__Impl"
    // InternalSeleniumDSL.g:1834:1: rule__Store__Group_1_0__2__Impl : ( ( rule__Store__TargetTypeAssignment_1_0_2 ) ) ;
    public final void rule__Store__Group_1_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1838:1: ( ( ( rule__Store__TargetTypeAssignment_1_0_2 ) ) )
            // InternalSeleniumDSL.g:1839:1: ( ( rule__Store__TargetTypeAssignment_1_0_2 ) )
            {
            // InternalSeleniumDSL.g:1839:1: ( ( rule__Store__TargetTypeAssignment_1_0_2 ) )
            // InternalSeleniumDSL.g:1840:2: ( rule__Store__TargetTypeAssignment_1_0_2 )
            {
             before(grammarAccess.getStoreAccess().getTargetTypeAssignment_1_0_2()); 
            // InternalSeleniumDSL.g:1841:2: ( rule__Store__TargetTypeAssignment_1_0_2 )
            // InternalSeleniumDSL.g:1841:3: rule__Store__TargetTypeAssignment_1_0_2
            {
            pushFollow(FOLLOW_2);
            rule__Store__TargetTypeAssignment_1_0_2();

            state._fsp--;


            }

             after(grammarAccess.getStoreAccess().getTargetTypeAssignment_1_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Store__Group_1_0__2__Impl"


    // $ANTLR start "rule__Store__Group_1_0__3"
    // InternalSeleniumDSL.g:1849:1: rule__Store__Group_1_0__3 : rule__Store__Group_1_0__3__Impl ;
    public final void rule__Store__Group_1_0__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1853:1: ( rule__Store__Group_1_0__3__Impl )
            // InternalSeleniumDSL.g:1854:2: rule__Store__Group_1_0__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Store__Group_1_0__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Store__Group_1_0__3"


    // $ANTLR start "rule__Store__Group_1_0__3__Impl"
    // InternalSeleniumDSL.g:1860:1: rule__Store__Group_1_0__3__Impl : ( ( rule__Store__ConditionAssignment_1_0_3 ) ) ;
    public final void rule__Store__Group_1_0__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1864:1: ( ( ( rule__Store__ConditionAssignment_1_0_3 ) ) )
            // InternalSeleniumDSL.g:1865:1: ( ( rule__Store__ConditionAssignment_1_0_3 ) )
            {
            // InternalSeleniumDSL.g:1865:1: ( ( rule__Store__ConditionAssignment_1_0_3 ) )
            // InternalSeleniumDSL.g:1866:2: ( rule__Store__ConditionAssignment_1_0_3 )
            {
             before(grammarAccess.getStoreAccess().getConditionAssignment_1_0_3()); 
            // InternalSeleniumDSL.g:1867:2: ( rule__Store__ConditionAssignment_1_0_3 )
            // InternalSeleniumDSL.g:1867:3: rule__Store__ConditionAssignment_1_0_3
            {
            pushFollow(FOLLOW_2);
            rule__Store__ConditionAssignment_1_0_3();

            state._fsp--;


            }

             after(grammarAccess.getStoreAccess().getConditionAssignment_1_0_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Store__Group_1_0__3__Impl"


    // $ANTLR start "rule__Set__Group__0"
    // InternalSeleniumDSL.g:1876:1: rule__Set__Group__0 : rule__Set__Group__0__Impl rule__Set__Group__1 ;
    public final void rule__Set__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1880:1: ( rule__Set__Group__0__Impl rule__Set__Group__1 )
            // InternalSeleniumDSL.g:1881:2: rule__Set__Group__0__Impl rule__Set__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__Set__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Set__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Set__Group__0"


    // $ANTLR start "rule__Set__Group__0__Impl"
    // InternalSeleniumDSL.g:1888:1: rule__Set__Group__0__Impl : ( 'set' ) ;
    public final void rule__Set__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1892:1: ( ( 'set' ) )
            // InternalSeleniumDSL.g:1893:1: ( 'set' )
            {
            // InternalSeleniumDSL.g:1893:1: ( 'set' )
            // InternalSeleniumDSL.g:1894:2: 'set'
            {
             before(grammarAccess.getSetAccess().getSetKeyword_0()); 
            match(input,39,FOLLOW_2); 
             after(grammarAccess.getSetAccess().getSetKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Set__Group__0__Impl"


    // $ANTLR start "rule__Set__Group__1"
    // InternalSeleniumDSL.g:1903:1: rule__Set__Group__1 : rule__Set__Group__1__Impl rule__Set__Group__2 ;
    public final void rule__Set__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1907:1: ( rule__Set__Group__1__Impl rule__Set__Group__2 )
            // InternalSeleniumDSL.g:1908:2: rule__Set__Group__1__Impl rule__Set__Group__2
            {
            pushFollow(FOLLOW_20);
            rule__Set__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Set__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Set__Group__1"


    // $ANTLR start "rule__Set__Group__1__Impl"
    // InternalSeleniumDSL.g:1915:1: rule__Set__Group__1__Impl : ( ( rule__Set__ValueAssignment_1 ) ) ;
    public final void rule__Set__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1919:1: ( ( ( rule__Set__ValueAssignment_1 ) ) )
            // InternalSeleniumDSL.g:1920:1: ( ( rule__Set__ValueAssignment_1 ) )
            {
            // InternalSeleniumDSL.g:1920:1: ( ( rule__Set__ValueAssignment_1 ) )
            // InternalSeleniumDSL.g:1921:2: ( rule__Set__ValueAssignment_1 )
            {
             before(grammarAccess.getSetAccess().getValueAssignment_1()); 
            // InternalSeleniumDSL.g:1922:2: ( rule__Set__ValueAssignment_1 )
            // InternalSeleniumDSL.g:1922:3: rule__Set__ValueAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Set__ValueAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getSetAccess().getValueAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Set__Group__1__Impl"


    // $ANTLR start "rule__Set__Group__2"
    // InternalSeleniumDSL.g:1930:1: rule__Set__Group__2 : rule__Set__Group__2__Impl rule__Set__Group__3 ;
    public final void rule__Set__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1934:1: ( rule__Set__Group__2__Impl rule__Set__Group__3 )
            // InternalSeleniumDSL.g:1935:2: rule__Set__Group__2__Impl rule__Set__Group__3
            {
            pushFollow(FOLLOW_16);
            rule__Set__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Set__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Set__Group__2"


    // $ANTLR start "rule__Set__Group__2__Impl"
    // InternalSeleniumDSL.g:1942:1: rule__Set__Group__2__Impl : ( 'to' ) ;
    public final void rule__Set__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1946:1: ( ( 'to' ) )
            // InternalSeleniumDSL.g:1947:1: ( 'to' )
            {
            // InternalSeleniumDSL.g:1947:1: ( 'to' )
            // InternalSeleniumDSL.g:1948:2: 'to'
            {
             before(grammarAccess.getSetAccess().getToKeyword_2()); 
            match(input,40,FOLLOW_2); 
             after(grammarAccess.getSetAccess().getToKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Set__Group__2__Impl"


    // $ANTLR start "rule__Set__Group__3"
    // InternalSeleniumDSL.g:1957:1: rule__Set__Group__3 : rule__Set__Group__3__Impl rule__Set__Group__4 ;
    public final void rule__Set__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1961:1: ( rule__Set__Group__3__Impl rule__Set__Group__4 )
            // InternalSeleniumDSL.g:1962:2: rule__Set__Group__3__Impl rule__Set__Group__4
            {
            pushFollow(FOLLOW_15);
            rule__Set__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Set__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Set__Group__3"


    // $ANTLR start "rule__Set__Group__3__Impl"
    // InternalSeleniumDSL.g:1969:1: rule__Set__Group__3__Impl : ( ( rule__Set__TargetTypeAssignment_3 ) ) ;
    public final void rule__Set__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1973:1: ( ( ( rule__Set__TargetTypeAssignment_3 ) ) )
            // InternalSeleniumDSL.g:1974:1: ( ( rule__Set__TargetTypeAssignment_3 ) )
            {
            // InternalSeleniumDSL.g:1974:1: ( ( rule__Set__TargetTypeAssignment_3 ) )
            // InternalSeleniumDSL.g:1975:2: ( rule__Set__TargetTypeAssignment_3 )
            {
             before(grammarAccess.getSetAccess().getTargetTypeAssignment_3()); 
            // InternalSeleniumDSL.g:1976:2: ( rule__Set__TargetTypeAssignment_3 )
            // InternalSeleniumDSL.g:1976:3: rule__Set__TargetTypeAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Set__TargetTypeAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getSetAccess().getTargetTypeAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Set__Group__3__Impl"


    // $ANTLR start "rule__Set__Group__4"
    // InternalSeleniumDSL.g:1984:1: rule__Set__Group__4 : rule__Set__Group__4__Impl ;
    public final void rule__Set__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1988:1: ( rule__Set__Group__4__Impl )
            // InternalSeleniumDSL.g:1989:2: rule__Set__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Set__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Set__Group__4"


    // $ANTLR start "rule__Set__Group__4__Impl"
    // InternalSeleniumDSL.g:1995:1: rule__Set__Group__4__Impl : ( ( rule__Set__ConditionAssignment_4 ) ) ;
    public final void rule__Set__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:1999:1: ( ( ( rule__Set__ConditionAssignment_4 ) ) )
            // InternalSeleniumDSL.g:2000:1: ( ( rule__Set__ConditionAssignment_4 ) )
            {
            // InternalSeleniumDSL.g:2000:1: ( ( rule__Set__ConditionAssignment_4 ) )
            // InternalSeleniumDSL.g:2001:2: ( rule__Set__ConditionAssignment_4 )
            {
             before(grammarAccess.getSetAccess().getConditionAssignment_4()); 
            // InternalSeleniumDSL.g:2002:2: ( rule__Set__ConditionAssignment_4 )
            // InternalSeleniumDSL.g:2002:3: rule__Set__ConditionAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__Set__ConditionAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getSetAccess().getConditionAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Set__Group__4__Impl"


    // $ANTLR start "rule__IsElementPresent__Group__0"
    // InternalSeleniumDSL.g:2011:1: rule__IsElementPresent__Group__0 : rule__IsElementPresent__Group__0__Impl rule__IsElementPresent__Group__1 ;
    public final void rule__IsElementPresent__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2015:1: ( rule__IsElementPresent__Group__0__Impl rule__IsElementPresent__Group__1 )
            // InternalSeleniumDSL.g:2016:2: rule__IsElementPresent__Group__0__Impl rule__IsElementPresent__Group__1
            {
            pushFollow(FOLLOW_16);
            rule__IsElementPresent__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IsElementPresent__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IsElementPresent__Group__0"


    // $ANTLR start "rule__IsElementPresent__Group__0__Impl"
    // InternalSeleniumDSL.g:2023:1: rule__IsElementPresent__Group__0__Impl : ( 'isElementPresent' ) ;
    public final void rule__IsElementPresent__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2027:1: ( ( 'isElementPresent' ) )
            // InternalSeleniumDSL.g:2028:1: ( 'isElementPresent' )
            {
            // InternalSeleniumDSL.g:2028:1: ( 'isElementPresent' )
            // InternalSeleniumDSL.g:2029:2: 'isElementPresent'
            {
             before(grammarAccess.getIsElementPresentAccess().getIsElementPresentKeyword_0()); 
            match(input,41,FOLLOW_2); 
             after(grammarAccess.getIsElementPresentAccess().getIsElementPresentKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IsElementPresent__Group__0__Impl"


    // $ANTLR start "rule__IsElementPresent__Group__1"
    // InternalSeleniumDSL.g:2038:1: rule__IsElementPresent__Group__1 : rule__IsElementPresent__Group__1__Impl rule__IsElementPresent__Group__2 ;
    public final void rule__IsElementPresent__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2042:1: ( rule__IsElementPresent__Group__1__Impl rule__IsElementPresent__Group__2 )
            // InternalSeleniumDSL.g:2043:2: rule__IsElementPresent__Group__1__Impl rule__IsElementPresent__Group__2
            {
            pushFollow(FOLLOW_15);
            rule__IsElementPresent__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IsElementPresent__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IsElementPresent__Group__1"


    // $ANTLR start "rule__IsElementPresent__Group__1__Impl"
    // InternalSeleniumDSL.g:2050:1: rule__IsElementPresent__Group__1__Impl : ( ( rule__IsElementPresent__TargetAssignment_1 ) ) ;
    public final void rule__IsElementPresent__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2054:1: ( ( ( rule__IsElementPresent__TargetAssignment_1 ) ) )
            // InternalSeleniumDSL.g:2055:1: ( ( rule__IsElementPresent__TargetAssignment_1 ) )
            {
            // InternalSeleniumDSL.g:2055:1: ( ( rule__IsElementPresent__TargetAssignment_1 ) )
            // InternalSeleniumDSL.g:2056:2: ( rule__IsElementPresent__TargetAssignment_1 )
            {
             before(grammarAccess.getIsElementPresentAccess().getTargetAssignment_1()); 
            // InternalSeleniumDSL.g:2057:2: ( rule__IsElementPresent__TargetAssignment_1 )
            // InternalSeleniumDSL.g:2057:3: rule__IsElementPresent__TargetAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__IsElementPresent__TargetAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getIsElementPresentAccess().getTargetAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IsElementPresent__Group__1__Impl"


    // $ANTLR start "rule__IsElementPresent__Group__2"
    // InternalSeleniumDSL.g:2065:1: rule__IsElementPresent__Group__2 : rule__IsElementPresent__Group__2__Impl ;
    public final void rule__IsElementPresent__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2069:1: ( rule__IsElementPresent__Group__2__Impl )
            // InternalSeleniumDSL.g:2070:2: rule__IsElementPresent__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__IsElementPresent__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IsElementPresent__Group__2"


    // $ANTLR start "rule__IsElementPresent__Group__2__Impl"
    // InternalSeleniumDSL.g:2076:1: rule__IsElementPresent__Group__2__Impl : ( ( rule__IsElementPresent__ConditionAssignment_2 ) ) ;
    public final void rule__IsElementPresent__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2080:1: ( ( ( rule__IsElementPresent__ConditionAssignment_2 ) ) )
            // InternalSeleniumDSL.g:2081:1: ( ( rule__IsElementPresent__ConditionAssignment_2 ) )
            {
            // InternalSeleniumDSL.g:2081:1: ( ( rule__IsElementPresent__ConditionAssignment_2 ) )
            // InternalSeleniumDSL.g:2082:2: ( rule__IsElementPresent__ConditionAssignment_2 )
            {
             before(grammarAccess.getIsElementPresentAccess().getConditionAssignment_2()); 
            // InternalSeleniumDSL.g:2083:2: ( rule__IsElementPresent__ConditionAssignment_2 )
            // InternalSeleniumDSL.g:2083:3: rule__IsElementPresent__ConditionAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__IsElementPresent__ConditionAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getIsElementPresentAccess().getConditionAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IsElementPresent__Group__2__Impl"


    // $ANTLR start "rule__Count__Group__0"
    // InternalSeleniumDSL.g:2092:1: rule__Count__Group__0 : rule__Count__Group__0__Impl rule__Count__Group__1 ;
    public final void rule__Count__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2096:1: ( rule__Count__Group__0__Impl rule__Count__Group__1 )
            // InternalSeleniumDSL.g:2097:2: rule__Count__Group__0__Impl rule__Count__Group__1
            {
            pushFollow(FOLLOW_16);
            rule__Count__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Count__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Count__Group__0"


    // $ANTLR start "rule__Count__Group__0__Impl"
    // InternalSeleniumDSL.g:2104:1: rule__Count__Group__0__Impl : ( 'count' ) ;
    public final void rule__Count__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2108:1: ( ( 'count' ) )
            // InternalSeleniumDSL.g:2109:1: ( 'count' )
            {
            // InternalSeleniumDSL.g:2109:1: ( 'count' )
            // InternalSeleniumDSL.g:2110:2: 'count'
            {
             before(grammarAccess.getCountAccess().getCountKeyword_0()); 
            match(input,42,FOLLOW_2); 
             after(grammarAccess.getCountAccess().getCountKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Count__Group__0__Impl"


    // $ANTLR start "rule__Count__Group__1"
    // InternalSeleniumDSL.g:2119:1: rule__Count__Group__1 : rule__Count__Group__1__Impl rule__Count__Group__2 ;
    public final void rule__Count__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2123:1: ( rule__Count__Group__1__Impl rule__Count__Group__2 )
            // InternalSeleniumDSL.g:2124:2: rule__Count__Group__1__Impl rule__Count__Group__2
            {
            pushFollow(FOLLOW_15);
            rule__Count__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Count__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Count__Group__1"


    // $ANTLR start "rule__Count__Group__1__Impl"
    // InternalSeleniumDSL.g:2131:1: rule__Count__Group__1__Impl : ( ( rule__Count__TargetTypeAssignment_1 ) ) ;
    public final void rule__Count__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2135:1: ( ( ( rule__Count__TargetTypeAssignment_1 ) ) )
            // InternalSeleniumDSL.g:2136:1: ( ( rule__Count__TargetTypeAssignment_1 ) )
            {
            // InternalSeleniumDSL.g:2136:1: ( ( rule__Count__TargetTypeAssignment_1 ) )
            // InternalSeleniumDSL.g:2137:2: ( rule__Count__TargetTypeAssignment_1 )
            {
             before(grammarAccess.getCountAccess().getTargetTypeAssignment_1()); 
            // InternalSeleniumDSL.g:2138:2: ( rule__Count__TargetTypeAssignment_1 )
            // InternalSeleniumDSL.g:2138:3: rule__Count__TargetTypeAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Count__TargetTypeAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getCountAccess().getTargetTypeAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Count__Group__1__Impl"


    // $ANTLR start "rule__Count__Group__2"
    // InternalSeleniumDSL.g:2146:1: rule__Count__Group__2 : rule__Count__Group__2__Impl rule__Count__Group__3 ;
    public final void rule__Count__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2150:1: ( rule__Count__Group__2__Impl rule__Count__Group__3 )
            // InternalSeleniumDSL.g:2151:2: rule__Count__Group__2__Impl rule__Count__Group__3
            {
            pushFollow(FOLLOW_13);
            rule__Count__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Count__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Count__Group__2"


    // $ANTLR start "rule__Count__Group__2__Impl"
    // InternalSeleniumDSL.g:2158:1: rule__Count__Group__2__Impl : ( ( rule__Count__ConditionAssignment_2 ) ) ;
    public final void rule__Count__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2162:1: ( ( ( rule__Count__ConditionAssignment_2 ) ) )
            // InternalSeleniumDSL.g:2163:1: ( ( rule__Count__ConditionAssignment_2 ) )
            {
            // InternalSeleniumDSL.g:2163:1: ( ( rule__Count__ConditionAssignment_2 ) )
            // InternalSeleniumDSL.g:2164:2: ( rule__Count__ConditionAssignment_2 )
            {
             before(grammarAccess.getCountAccess().getConditionAssignment_2()); 
            // InternalSeleniumDSL.g:2165:2: ( rule__Count__ConditionAssignment_2 )
            // InternalSeleniumDSL.g:2165:3: rule__Count__ConditionAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Count__ConditionAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getCountAccess().getConditionAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Count__Group__2__Impl"


    // $ANTLR start "rule__Count__Group__3"
    // InternalSeleniumDSL.g:2173:1: rule__Count__Group__3 : rule__Count__Group__3__Impl rule__Count__Group__4 ;
    public final void rule__Count__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2177:1: ( rule__Count__Group__3__Impl rule__Count__Group__4 )
            // InternalSeleniumDSL.g:2178:2: rule__Count__Group__3__Impl rule__Count__Group__4
            {
            pushFollow(FOLLOW_18);
            rule__Count__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Count__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Count__Group__3"


    // $ANTLR start "rule__Count__Group__3__Impl"
    // InternalSeleniumDSL.g:2185:1: rule__Count__Group__3__Impl : ( 'in' ) ;
    public final void rule__Count__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2189:1: ( ( 'in' ) )
            // InternalSeleniumDSL.g:2190:1: ( 'in' )
            {
            // InternalSeleniumDSL.g:2190:1: ( 'in' )
            // InternalSeleniumDSL.g:2191:2: 'in'
            {
             before(grammarAccess.getCountAccess().getInKeyword_3()); 
            match(input,34,FOLLOW_2); 
             after(grammarAccess.getCountAccess().getInKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Count__Group__3__Impl"


    // $ANTLR start "rule__Count__Group__4"
    // InternalSeleniumDSL.g:2200:1: rule__Count__Group__4 : rule__Count__Group__4__Impl ;
    public final void rule__Count__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2204:1: ( rule__Count__Group__4__Impl )
            // InternalSeleniumDSL.g:2205:2: rule__Count__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Count__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Count__Group__4"


    // $ANTLR start "rule__Count__Group__4__Impl"
    // InternalSeleniumDSL.g:2211:1: rule__Count__Group__4__Impl : ( ( rule__Count__VariableNameAssignment_4 ) ) ;
    public final void rule__Count__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2215:1: ( ( ( rule__Count__VariableNameAssignment_4 ) ) )
            // InternalSeleniumDSL.g:2216:1: ( ( rule__Count__VariableNameAssignment_4 ) )
            {
            // InternalSeleniumDSL.g:2216:1: ( ( rule__Count__VariableNameAssignment_4 ) )
            // InternalSeleniumDSL.g:2217:2: ( rule__Count__VariableNameAssignment_4 )
            {
             before(grammarAccess.getCountAccess().getVariableNameAssignment_4()); 
            // InternalSeleniumDSL.g:2218:2: ( rule__Count__VariableNameAssignment_4 )
            // InternalSeleniumDSL.g:2218:3: rule__Count__VariableNameAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__Count__VariableNameAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getCountAccess().getVariableNameAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Count__Group__4__Impl"


    // $ANTLR start "rule__Function__Group__0"
    // InternalSeleniumDSL.g:2227:1: rule__Function__Group__0 : rule__Function__Group__0__Impl rule__Function__Group__1 ;
    public final void rule__Function__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2231:1: ( rule__Function__Group__0__Impl rule__Function__Group__1 )
            // InternalSeleniumDSL.g:2232:2: rule__Function__Group__0__Impl rule__Function__Group__1
            {
            pushFollow(FOLLOW_18);
            rule__Function__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Function__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__0"


    // $ANTLR start "rule__Function__Group__0__Impl"
    // InternalSeleniumDSL.g:2239:1: rule__Function__Group__0__Impl : ( 'define' ) ;
    public final void rule__Function__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2243:1: ( ( 'define' ) )
            // InternalSeleniumDSL.g:2244:1: ( 'define' )
            {
            // InternalSeleniumDSL.g:2244:1: ( 'define' )
            // InternalSeleniumDSL.g:2245:2: 'define'
            {
             before(grammarAccess.getFunctionAccess().getDefineKeyword_0()); 
            match(input,43,FOLLOW_2); 
             after(grammarAccess.getFunctionAccess().getDefineKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__0__Impl"


    // $ANTLR start "rule__Function__Group__1"
    // InternalSeleniumDSL.g:2254:1: rule__Function__Group__1 : rule__Function__Group__1__Impl rule__Function__Group__2 ;
    public final void rule__Function__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2258:1: ( rule__Function__Group__1__Impl rule__Function__Group__2 )
            // InternalSeleniumDSL.g:2259:2: rule__Function__Group__1__Impl rule__Function__Group__2
            {
            pushFollow(FOLLOW_21);
            rule__Function__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Function__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__1"


    // $ANTLR start "rule__Function__Group__1__Impl"
    // InternalSeleniumDSL.g:2266:1: rule__Function__Group__1__Impl : ( ( rule__Function__FunctionNameAssignment_1 ) ) ;
    public final void rule__Function__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2270:1: ( ( ( rule__Function__FunctionNameAssignment_1 ) ) )
            // InternalSeleniumDSL.g:2271:1: ( ( rule__Function__FunctionNameAssignment_1 ) )
            {
            // InternalSeleniumDSL.g:2271:1: ( ( rule__Function__FunctionNameAssignment_1 ) )
            // InternalSeleniumDSL.g:2272:2: ( rule__Function__FunctionNameAssignment_1 )
            {
             before(grammarAccess.getFunctionAccess().getFunctionNameAssignment_1()); 
            // InternalSeleniumDSL.g:2273:2: ( rule__Function__FunctionNameAssignment_1 )
            // InternalSeleniumDSL.g:2273:3: rule__Function__FunctionNameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Function__FunctionNameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getFunctionAccess().getFunctionNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__1__Impl"


    // $ANTLR start "rule__Function__Group__2"
    // InternalSeleniumDSL.g:2281:1: rule__Function__Group__2 : rule__Function__Group__2__Impl rule__Function__Group__3 ;
    public final void rule__Function__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2285:1: ( rule__Function__Group__2__Impl rule__Function__Group__3 )
            // InternalSeleniumDSL.g:2286:2: rule__Function__Group__2__Impl rule__Function__Group__3
            {
            pushFollow(FOLLOW_22);
            rule__Function__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Function__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__2"


    // $ANTLR start "rule__Function__Group__2__Impl"
    // InternalSeleniumDSL.g:2293:1: rule__Function__Group__2__Impl : ( '(' ) ;
    public final void rule__Function__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2297:1: ( ( '(' ) )
            // InternalSeleniumDSL.g:2298:1: ( '(' )
            {
            // InternalSeleniumDSL.g:2298:1: ( '(' )
            // InternalSeleniumDSL.g:2299:2: '('
            {
             before(grammarAccess.getFunctionAccess().getLeftParenthesisKeyword_2()); 
            match(input,44,FOLLOW_2); 
             after(grammarAccess.getFunctionAccess().getLeftParenthesisKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__2__Impl"


    // $ANTLR start "rule__Function__Group__3"
    // InternalSeleniumDSL.g:2308:1: rule__Function__Group__3 : rule__Function__Group__3__Impl rule__Function__Group__4 ;
    public final void rule__Function__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2312:1: ( rule__Function__Group__3__Impl rule__Function__Group__4 )
            // InternalSeleniumDSL.g:2313:2: rule__Function__Group__3__Impl rule__Function__Group__4
            {
            pushFollow(FOLLOW_22);
            rule__Function__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Function__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__3"


    // $ANTLR start "rule__Function__Group__3__Impl"
    // InternalSeleniumDSL.g:2320:1: rule__Function__Group__3__Impl : ( ( rule__Function__ArgumentsAssignment_3 )* ) ;
    public final void rule__Function__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2324:1: ( ( ( rule__Function__ArgumentsAssignment_3 )* ) )
            // InternalSeleniumDSL.g:2325:1: ( ( rule__Function__ArgumentsAssignment_3 )* )
            {
            // InternalSeleniumDSL.g:2325:1: ( ( rule__Function__ArgumentsAssignment_3 )* )
            // InternalSeleniumDSL.g:2326:2: ( rule__Function__ArgumentsAssignment_3 )*
            {
             before(grammarAccess.getFunctionAccess().getArgumentsAssignment_3()); 
            // InternalSeleniumDSL.g:2327:2: ( rule__Function__ArgumentsAssignment_3 )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==RULE_ID) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalSeleniumDSL.g:2327:3: rule__Function__ArgumentsAssignment_3
            	    {
            	    pushFollow(FOLLOW_23);
            	    rule__Function__ArgumentsAssignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

             after(grammarAccess.getFunctionAccess().getArgumentsAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__3__Impl"


    // $ANTLR start "rule__Function__Group__4"
    // InternalSeleniumDSL.g:2335:1: rule__Function__Group__4 : rule__Function__Group__4__Impl rule__Function__Group__5 ;
    public final void rule__Function__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2339:1: ( rule__Function__Group__4__Impl rule__Function__Group__5 )
            // InternalSeleniumDSL.g:2340:2: rule__Function__Group__4__Impl rule__Function__Group__5
            {
            pushFollow(FOLLOW_24);
            rule__Function__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Function__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__4"


    // $ANTLR start "rule__Function__Group__4__Impl"
    // InternalSeleniumDSL.g:2347:1: rule__Function__Group__4__Impl : ( ')' ) ;
    public final void rule__Function__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2351:1: ( ( ')' ) )
            // InternalSeleniumDSL.g:2352:1: ( ')' )
            {
            // InternalSeleniumDSL.g:2352:1: ( ')' )
            // InternalSeleniumDSL.g:2353:2: ')'
            {
             before(grammarAccess.getFunctionAccess().getRightParenthesisKeyword_4()); 
            match(input,45,FOLLOW_2); 
             after(grammarAccess.getFunctionAccess().getRightParenthesisKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__4__Impl"


    // $ANTLR start "rule__Function__Group__5"
    // InternalSeleniumDSL.g:2362:1: rule__Function__Group__5 : rule__Function__Group__5__Impl rule__Function__Group__6 ;
    public final void rule__Function__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2366:1: ( rule__Function__Group__5__Impl rule__Function__Group__6 )
            // InternalSeleniumDSL.g:2367:2: rule__Function__Group__5__Impl rule__Function__Group__6
            {
            pushFollow(FOLLOW_24);
            rule__Function__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Function__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__5"


    // $ANTLR start "rule__Function__Group__5__Impl"
    // InternalSeleniumDSL.g:2374:1: rule__Function__Group__5__Impl : ( ( rule__Function__InstructionsAssignment_5 )* ) ;
    public final void rule__Function__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2378:1: ( ( ( rule__Function__InstructionsAssignment_5 )* ) )
            // InternalSeleniumDSL.g:2379:1: ( ( rule__Function__InstructionsAssignment_5 )* )
            {
            // InternalSeleniumDSL.g:2379:1: ( ( rule__Function__InstructionsAssignment_5 )* )
            // InternalSeleniumDSL.g:2380:2: ( rule__Function__InstructionsAssignment_5 )*
            {
             before(grammarAccess.getFunctionAccess().getInstructionsAssignment_5()); 
            // InternalSeleniumDSL.g:2381:2: ( rule__Function__InstructionsAssignment_5 )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==RULE_ID||(LA16_0>=27 && LA16_0<=28)||LA16_0==30||LA16_0==32||(LA16_0>=36 && LA16_0<=37)||LA16_0==39||(LA16_0>=41 && LA16_0<=42)||LA16_0==47) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalSeleniumDSL.g:2381:3: rule__Function__InstructionsAssignment_5
            	    {
            	    pushFollow(FOLLOW_6);
            	    rule__Function__InstructionsAssignment_5();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);

             after(grammarAccess.getFunctionAccess().getInstructionsAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__5__Impl"


    // $ANTLR start "rule__Function__Group__6"
    // InternalSeleniumDSL.g:2389:1: rule__Function__Group__6 : rule__Function__Group__6__Impl ;
    public final void rule__Function__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2393:1: ( rule__Function__Group__6__Impl )
            // InternalSeleniumDSL.g:2394:2: rule__Function__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Function__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__6"


    // $ANTLR start "rule__Function__Group__6__Impl"
    // InternalSeleniumDSL.g:2400:1: rule__Function__Group__6__Impl : ( ';' ) ;
    public final void rule__Function__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2404:1: ( ( ';' ) )
            // InternalSeleniumDSL.g:2405:1: ( ';' )
            {
            // InternalSeleniumDSL.g:2405:1: ( ';' )
            // InternalSeleniumDSL.g:2406:2: ';'
            {
             before(grammarAccess.getFunctionAccess().getSemicolonKeyword_6()); 
            match(input,46,FOLLOW_2); 
             after(grammarAccess.getFunctionAccess().getSemicolonKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__6__Impl"


    // $ANTLR start "rule__FunctionCall__Group__0"
    // InternalSeleniumDSL.g:2416:1: rule__FunctionCall__Group__0 : rule__FunctionCall__Group__0__Impl rule__FunctionCall__Group__1 ;
    public final void rule__FunctionCall__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2420:1: ( rule__FunctionCall__Group__0__Impl rule__FunctionCall__Group__1 )
            // InternalSeleniumDSL.g:2421:2: rule__FunctionCall__Group__0__Impl rule__FunctionCall__Group__1
            {
            pushFollow(FOLLOW_21);
            rule__FunctionCall__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FunctionCall__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionCall__Group__0"


    // $ANTLR start "rule__FunctionCall__Group__0__Impl"
    // InternalSeleniumDSL.g:2428:1: rule__FunctionCall__Group__0__Impl : ( ( rule__FunctionCall__FunctionNameAssignment_0 ) ) ;
    public final void rule__FunctionCall__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2432:1: ( ( ( rule__FunctionCall__FunctionNameAssignment_0 ) ) )
            // InternalSeleniumDSL.g:2433:1: ( ( rule__FunctionCall__FunctionNameAssignment_0 ) )
            {
            // InternalSeleniumDSL.g:2433:1: ( ( rule__FunctionCall__FunctionNameAssignment_0 ) )
            // InternalSeleniumDSL.g:2434:2: ( rule__FunctionCall__FunctionNameAssignment_0 )
            {
             before(grammarAccess.getFunctionCallAccess().getFunctionNameAssignment_0()); 
            // InternalSeleniumDSL.g:2435:2: ( rule__FunctionCall__FunctionNameAssignment_0 )
            // InternalSeleniumDSL.g:2435:3: rule__FunctionCall__FunctionNameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__FunctionCall__FunctionNameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getFunctionCallAccess().getFunctionNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionCall__Group__0__Impl"


    // $ANTLR start "rule__FunctionCall__Group__1"
    // InternalSeleniumDSL.g:2443:1: rule__FunctionCall__Group__1 : rule__FunctionCall__Group__1__Impl rule__FunctionCall__Group__2 ;
    public final void rule__FunctionCall__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2447:1: ( rule__FunctionCall__Group__1__Impl rule__FunctionCall__Group__2 )
            // InternalSeleniumDSL.g:2448:2: rule__FunctionCall__Group__1__Impl rule__FunctionCall__Group__2
            {
            pushFollow(FOLLOW_25);
            rule__FunctionCall__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FunctionCall__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionCall__Group__1"


    // $ANTLR start "rule__FunctionCall__Group__1__Impl"
    // InternalSeleniumDSL.g:2455:1: rule__FunctionCall__Group__1__Impl : ( '(' ) ;
    public final void rule__FunctionCall__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2459:1: ( ( '(' ) )
            // InternalSeleniumDSL.g:2460:1: ( '(' )
            {
            // InternalSeleniumDSL.g:2460:1: ( '(' )
            // InternalSeleniumDSL.g:2461:2: '('
            {
             before(grammarAccess.getFunctionCallAccess().getLeftParenthesisKeyword_1()); 
            match(input,44,FOLLOW_2); 
             after(grammarAccess.getFunctionCallAccess().getLeftParenthesisKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionCall__Group__1__Impl"


    // $ANTLR start "rule__FunctionCall__Group__2"
    // InternalSeleniumDSL.g:2470:1: rule__FunctionCall__Group__2 : rule__FunctionCall__Group__2__Impl rule__FunctionCall__Group__3 ;
    public final void rule__FunctionCall__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2474:1: ( rule__FunctionCall__Group__2__Impl rule__FunctionCall__Group__3 )
            // InternalSeleniumDSL.g:2475:2: rule__FunctionCall__Group__2__Impl rule__FunctionCall__Group__3
            {
            pushFollow(FOLLOW_25);
            rule__FunctionCall__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FunctionCall__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionCall__Group__2"


    // $ANTLR start "rule__FunctionCall__Group__2__Impl"
    // InternalSeleniumDSL.g:2482:1: rule__FunctionCall__Group__2__Impl : ( ( rule__FunctionCall__ValuesAssignment_2 )* ) ;
    public final void rule__FunctionCall__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2486:1: ( ( ( rule__FunctionCall__ValuesAssignment_2 )* ) )
            // InternalSeleniumDSL.g:2487:1: ( ( rule__FunctionCall__ValuesAssignment_2 )* )
            {
            // InternalSeleniumDSL.g:2487:1: ( ( rule__FunctionCall__ValuesAssignment_2 )* )
            // InternalSeleniumDSL.g:2488:2: ( rule__FunctionCall__ValuesAssignment_2 )*
            {
             before(grammarAccess.getFunctionCallAccess().getValuesAssignment_2()); 
            // InternalSeleniumDSL.g:2489:2: ( rule__FunctionCall__ValuesAssignment_2 )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( ((LA17_0>=RULE_STRING && LA17_0<=RULE_ID)) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // InternalSeleniumDSL.g:2489:3: rule__FunctionCall__ValuesAssignment_2
            	    {
            	    pushFollow(FOLLOW_26);
            	    rule__FunctionCall__ValuesAssignment_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);

             after(grammarAccess.getFunctionCallAccess().getValuesAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionCall__Group__2__Impl"


    // $ANTLR start "rule__FunctionCall__Group__3"
    // InternalSeleniumDSL.g:2497:1: rule__FunctionCall__Group__3 : rule__FunctionCall__Group__3__Impl ;
    public final void rule__FunctionCall__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2501:1: ( rule__FunctionCall__Group__3__Impl )
            // InternalSeleniumDSL.g:2502:2: rule__FunctionCall__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__FunctionCall__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionCall__Group__3"


    // $ANTLR start "rule__FunctionCall__Group__3__Impl"
    // InternalSeleniumDSL.g:2508:1: rule__FunctionCall__Group__3__Impl : ( ')' ) ;
    public final void rule__FunctionCall__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2512:1: ( ( ')' ) )
            // InternalSeleniumDSL.g:2513:1: ( ')' )
            {
            // InternalSeleniumDSL.g:2513:1: ( ')' )
            // InternalSeleniumDSL.g:2514:2: ')'
            {
             before(grammarAccess.getFunctionCallAccess().getRightParenthesisKeyword_3()); 
            match(input,45,FOLLOW_2); 
             after(grammarAccess.getFunctionCallAccess().getRightParenthesisKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionCall__Group__3__Impl"


    // $ANTLR start "rule__Assert__Group__0"
    // InternalSeleniumDSL.g:2524:1: rule__Assert__Group__0 : rule__Assert__Group__0__Impl rule__Assert__Group__1 ;
    public final void rule__Assert__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2528:1: ( rule__Assert__Group__0__Impl rule__Assert__Group__1 )
            // InternalSeleniumDSL.g:2529:2: rule__Assert__Group__0__Impl rule__Assert__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__Assert__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Assert__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assert__Group__0"


    // $ANTLR start "rule__Assert__Group__0__Impl"
    // InternalSeleniumDSL.g:2536:1: rule__Assert__Group__0__Impl : ( 'assert' ) ;
    public final void rule__Assert__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2540:1: ( ( 'assert' ) )
            // InternalSeleniumDSL.g:2541:1: ( 'assert' )
            {
            // InternalSeleniumDSL.g:2541:1: ( 'assert' )
            // InternalSeleniumDSL.g:2542:2: 'assert'
            {
             before(grammarAccess.getAssertAccess().getAssertKeyword_0()); 
            match(input,47,FOLLOW_2); 
             after(grammarAccess.getAssertAccess().getAssertKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assert__Group__0__Impl"


    // $ANTLR start "rule__Assert__Group__1"
    // InternalSeleniumDSL.g:2551:1: rule__Assert__Group__1 : rule__Assert__Group__1__Impl rule__Assert__Group__2 ;
    public final void rule__Assert__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2555:1: ( rule__Assert__Group__1__Impl rule__Assert__Group__2 )
            // InternalSeleniumDSL.g:2556:2: rule__Assert__Group__1__Impl rule__Assert__Group__2
            {
            pushFollow(FOLLOW_27);
            rule__Assert__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Assert__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assert__Group__1"


    // $ANTLR start "rule__Assert__Group__1__Impl"
    // InternalSeleniumDSL.g:2563:1: rule__Assert__Group__1__Impl : ( ( rule__Assert__Value1Assignment_1 ) ) ;
    public final void rule__Assert__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2567:1: ( ( ( rule__Assert__Value1Assignment_1 ) ) )
            // InternalSeleniumDSL.g:2568:1: ( ( rule__Assert__Value1Assignment_1 ) )
            {
            // InternalSeleniumDSL.g:2568:1: ( ( rule__Assert__Value1Assignment_1 ) )
            // InternalSeleniumDSL.g:2569:2: ( rule__Assert__Value1Assignment_1 )
            {
             before(grammarAccess.getAssertAccess().getValue1Assignment_1()); 
            // InternalSeleniumDSL.g:2570:2: ( rule__Assert__Value1Assignment_1 )
            // InternalSeleniumDSL.g:2570:3: rule__Assert__Value1Assignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Assert__Value1Assignment_1();

            state._fsp--;


            }

             after(grammarAccess.getAssertAccess().getValue1Assignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assert__Group__1__Impl"


    // $ANTLR start "rule__Assert__Group__2"
    // InternalSeleniumDSL.g:2578:1: rule__Assert__Group__2 : rule__Assert__Group__2__Impl rule__Assert__Group__3 ;
    public final void rule__Assert__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2582:1: ( rule__Assert__Group__2__Impl rule__Assert__Group__3 )
            // InternalSeleniumDSL.g:2583:2: rule__Assert__Group__2__Impl rule__Assert__Group__3
            {
            pushFollow(FOLLOW_8);
            rule__Assert__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Assert__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assert__Group__2"


    // $ANTLR start "rule__Assert__Group__2__Impl"
    // InternalSeleniumDSL.g:2590:1: rule__Assert__Group__2__Impl : ( ( rule__Assert__Group_2__0 ) ) ;
    public final void rule__Assert__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2594:1: ( ( ( rule__Assert__Group_2__0 ) ) )
            // InternalSeleniumDSL.g:2595:1: ( ( rule__Assert__Group_2__0 ) )
            {
            // InternalSeleniumDSL.g:2595:1: ( ( rule__Assert__Group_2__0 ) )
            // InternalSeleniumDSL.g:2596:2: ( rule__Assert__Group_2__0 )
            {
             before(grammarAccess.getAssertAccess().getGroup_2()); 
            // InternalSeleniumDSL.g:2597:2: ( rule__Assert__Group_2__0 )
            // InternalSeleniumDSL.g:2597:3: rule__Assert__Group_2__0
            {
            pushFollow(FOLLOW_2);
            rule__Assert__Group_2__0();

            state._fsp--;


            }

             after(grammarAccess.getAssertAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assert__Group__2__Impl"


    // $ANTLR start "rule__Assert__Group__3"
    // InternalSeleniumDSL.g:2605:1: rule__Assert__Group__3 : rule__Assert__Group__3__Impl ;
    public final void rule__Assert__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2609:1: ( rule__Assert__Group__3__Impl )
            // InternalSeleniumDSL.g:2610:2: rule__Assert__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Assert__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assert__Group__3"


    // $ANTLR start "rule__Assert__Group__3__Impl"
    // InternalSeleniumDSL.g:2616:1: rule__Assert__Group__3__Impl : ( ( rule__Assert__Value2Assignment_3 ) ) ;
    public final void rule__Assert__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2620:1: ( ( ( rule__Assert__Value2Assignment_3 ) ) )
            // InternalSeleniumDSL.g:2621:1: ( ( rule__Assert__Value2Assignment_3 ) )
            {
            // InternalSeleniumDSL.g:2621:1: ( ( rule__Assert__Value2Assignment_3 ) )
            // InternalSeleniumDSL.g:2622:2: ( rule__Assert__Value2Assignment_3 )
            {
             before(grammarAccess.getAssertAccess().getValue2Assignment_3()); 
            // InternalSeleniumDSL.g:2623:2: ( rule__Assert__Value2Assignment_3 )
            // InternalSeleniumDSL.g:2623:3: rule__Assert__Value2Assignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Assert__Value2Assignment_3();

            state._fsp--;


            }

             after(grammarAccess.getAssertAccess().getValue2Assignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assert__Group__3__Impl"


    // $ANTLR start "rule__Assert__Group_2__0"
    // InternalSeleniumDSL.g:2632:1: rule__Assert__Group_2__0 : rule__Assert__Group_2__0__Impl rule__Assert__Group_2__1 ;
    public final void rule__Assert__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2636:1: ( rule__Assert__Group_2__0__Impl rule__Assert__Group_2__1 )
            // InternalSeleniumDSL.g:2637:2: rule__Assert__Group_2__0__Impl rule__Assert__Group_2__1
            {
            pushFollow(FOLLOW_27);
            rule__Assert__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Assert__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assert__Group_2__0"


    // $ANTLR start "rule__Assert__Group_2__0__Impl"
    // InternalSeleniumDSL.g:2644:1: rule__Assert__Group_2__0__Impl : ( ( rule__Assert__NegateAssignment_2_0 )? ) ;
    public final void rule__Assert__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2648:1: ( ( ( rule__Assert__NegateAssignment_2_0 )? ) )
            // InternalSeleniumDSL.g:2649:1: ( ( rule__Assert__NegateAssignment_2_0 )? )
            {
            // InternalSeleniumDSL.g:2649:1: ( ( rule__Assert__NegateAssignment_2_0 )? )
            // InternalSeleniumDSL.g:2650:2: ( rule__Assert__NegateAssignment_2_0 )?
            {
             before(grammarAccess.getAssertAccess().getNegateAssignment_2_0()); 
            // InternalSeleniumDSL.g:2651:2: ( rule__Assert__NegateAssignment_2_0 )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==49) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalSeleniumDSL.g:2651:3: rule__Assert__NegateAssignment_2_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Assert__NegateAssignment_2_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAssertAccess().getNegateAssignment_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assert__Group_2__0__Impl"


    // $ANTLR start "rule__Assert__Group_2__1"
    // InternalSeleniumDSL.g:2659:1: rule__Assert__Group_2__1 : rule__Assert__Group_2__1__Impl ;
    public final void rule__Assert__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2663:1: ( rule__Assert__Group_2__1__Impl )
            // InternalSeleniumDSL.g:2664:2: rule__Assert__Group_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Assert__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assert__Group_2__1"


    // $ANTLR start "rule__Assert__Group_2__1__Impl"
    // InternalSeleniumDSL.g:2670:1: rule__Assert__Group_2__1__Impl : ( 'equals' ) ;
    public final void rule__Assert__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2674:1: ( ( 'equals' ) )
            // InternalSeleniumDSL.g:2675:1: ( 'equals' )
            {
            // InternalSeleniumDSL.g:2675:1: ( 'equals' )
            // InternalSeleniumDSL.g:2676:2: 'equals'
            {
             before(grammarAccess.getAssertAccess().getEqualsKeyword_2_1()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getAssertAccess().getEqualsKeyword_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assert__Group_2__1__Impl"


    // $ANTLR start "rule__SeleniumProgram__FunctionsAssignment_0"
    // InternalSeleniumDSL.g:2686:1: rule__SeleniumProgram__FunctionsAssignment_0 : ( ruleFunction ) ;
    public final void rule__SeleniumProgram__FunctionsAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2690:1: ( ( ruleFunction ) )
            // InternalSeleniumDSL.g:2691:2: ( ruleFunction )
            {
            // InternalSeleniumDSL.g:2691:2: ( ruleFunction )
            // InternalSeleniumDSL.g:2692:3: ruleFunction
            {
             before(grammarAccess.getSeleniumProgramAccess().getFunctionsFunctionParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleFunction();

            state._fsp--;

             after(grammarAccess.getSeleniumProgramAccess().getFunctionsFunctionParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SeleniumProgram__FunctionsAssignment_0"


    // $ANTLR start "rule__SeleniumProgram__OpenAssignment_1"
    // InternalSeleniumDSL.g:2701:1: rule__SeleniumProgram__OpenAssignment_1 : ( ruleOpen ) ;
    public final void rule__SeleniumProgram__OpenAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2705:1: ( ( ruleOpen ) )
            // InternalSeleniumDSL.g:2706:2: ( ruleOpen )
            {
            // InternalSeleniumDSL.g:2706:2: ( ruleOpen )
            // InternalSeleniumDSL.g:2707:3: ruleOpen
            {
             before(grammarAccess.getSeleniumProgramAccess().getOpenOpenParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleOpen();

            state._fsp--;

             after(grammarAccess.getSeleniumProgramAccess().getOpenOpenParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SeleniumProgram__OpenAssignment_1"


    // $ANTLR start "rule__SeleniumProgram__SeleniumInstructionAssignment_2"
    // InternalSeleniumDSL.g:2716:1: rule__SeleniumProgram__SeleniumInstructionAssignment_2 : ( ruleSeleniumInstruction ) ;
    public final void rule__SeleniumProgram__SeleniumInstructionAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2720:1: ( ( ruleSeleniumInstruction ) )
            // InternalSeleniumDSL.g:2721:2: ( ruleSeleniumInstruction )
            {
            // InternalSeleniumDSL.g:2721:2: ( ruleSeleniumInstruction )
            // InternalSeleniumDSL.g:2722:3: ruleSeleniumInstruction
            {
             before(grammarAccess.getSeleniumProgramAccess().getSeleniumInstructionSeleniumInstructionParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleSeleniumInstruction();

            state._fsp--;

             after(grammarAccess.getSeleniumProgramAccess().getSeleniumInstructionSeleniumInstructionParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SeleniumProgram__SeleniumInstructionAssignment_2"


    // $ANTLR start "rule__SeleniumProgram__CloseAssignment_3"
    // InternalSeleniumDSL.g:2731:1: rule__SeleniumProgram__CloseAssignment_3 : ( ruleClose ) ;
    public final void rule__SeleniumProgram__CloseAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2735:1: ( ( ruleClose ) )
            // InternalSeleniumDSL.g:2736:2: ( ruleClose )
            {
            // InternalSeleniumDSL.g:2736:2: ( ruleClose )
            // InternalSeleniumDSL.g:2737:3: ruleClose
            {
             before(grammarAccess.getSeleniumProgramAccess().getCloseCloseParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleClose();

            state._fsp--;

             after(grammarAccess.getSeleniumProgramAccess().getCloseCloseParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SeleniumProgram__CloseAssignment_3"


    // $ANTLR start "rule__Open__BrowserAssignment_1"
    // InternalSeleniumDSL.g:2746:1: rule__Open__BrowserAssignment_1 : ( ruleBrowser ) ;
    public final void rule__Open__BrowserAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2750:1: ( ( ruleBrowser ) )
            // InternalSeleniumDSL.g:2751:2: ( ruleBrowser )
            {
            // InternalSeleniumDSL.g:2751:2: ( ruleBrowser )
            // InternalSeleniumDSL.g:2752:3: ruleBrowser
            {
             before(grammarAccess.getOpenAccess().getBrowserBrowserParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleBrowser();

            state._fsp--;

             after(grammarAccess.getOpenAccess().getBrowserBrowserParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Open__BrowserAssignment_1"


    // $ANTLR start "rule__Value__VariableNameAssignment_0"
    // InternalSeleniumDSL.g:2761:1: rule__Value__VariableNameAssignment_0 : ( ruleVariableName ) ;
    public final void rule__Value__VariableNameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2765:1: ( ( ruleVariableName ) )
            // InternalSeleniumDSL.g:2766:2: ( ruleVariableName )
            {
            // InternalSeleniumDSL.g:2766:2: ( ruleVariableName )
            // InternalSeleniumDSL.g:2767:3: ruleVariableName
            {
             before(grammarAccess.getValueAccess().getVariableNameVariableNameParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleVariableName();

            state._fsp--;

             after(grammarAccess.getValueAccess().getVariableNameVariableNameParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value__VariableNameAssignment_0"


    // $ANTLR start "rule__Value__ValueAssignment_1"
    // InternalSeleniumDSL.g:2776:1: rule__Value__ValueAssignment_1 : ( RULE_STRING ) ;
    public final void rule__Value__ValueAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2780:1: ( ( RULE_STRING ) )
            // InternalSeleniumDSL.g:2781:2: ( RULE_STRING )
            {
            // InternalSeleniumDSL.g:2781:2: ( RULE_STRING )
            // InternalSeleniumDSL.g:2782:3: RULE_STRING
            {
             before(grammarAccess.getValueAccess().getValueSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getValueAccess().getValueSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value__ValueAssignment_1"


    // $ANTLR start "rule__Get__UrlAssignment_1"
    // InternalSeleniumDSL.g:2791:1: rule__Get__UrlAssignment_1 : ( ruleValue ) ;
    public final void rule__Get__UrlAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2795:1: ( ( ruleValue ) )
            // InternalSeleniumDSL.g:2796:2: ( ruleValue )
            {
            // InternalSeleniumDSL.g:2796:2: ( ruleValue )
            // InternalSeleniumDSL.g:2797:3: ruleValue
            {
             before(grammarAccess.getGetAccess().getUrlValueParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleValue();

            state._fsp--;

             after(grammarAccess.getGetAccess().getUrlValueParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Get__UrlAssignment_1"


    // $ANTLR start "rule__TargetType__TargetTypeAssignment"
    // InternalSeleniumDSL.g:2806:1: rule__TargetType__TargetTypeAssignment : ( ( rule__TargetType__TargetTypeAlternatives_0 ) ) ;
    public final void rule__TargetType__TargetTypeAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2810:1: ( ( ( rule__TargetType__TargetTypeAlternatives_0 ) ) )
            // InternalSeleniumDSL.g:2811:2: ( ( rule__TargetType__TargetTypeAlternatives_0 ) )
            {
            // InternalSeleniumDSL.g:2811:2: ( ( rule__TargetType__TargetTypeAlternatives_0 ) )
            // InternalSeleniumDSL.g:2812:3: ( rule__TargetType__TargetTypeAlternatives_0 )
            {
             before(grammarAccess.getTargetTypeAccess().getTargetTypeAlternatives_0()); 
            // InternalSeleniumDSL.g:2813:3: ( rule__TargetType__TargetTypeAlternatives_0 )
            // InternalSeleniumDSL.g:2813:4: rule__TargetType__TargetTypeAlternatives_0
            {
            pushFollow(FOLLOW_2);
            rule__TargetType__TargetTypeAlternatives_0();

            state._fsp--;


            }

             after(grammarAccess.getTargetTypeAccess().getTargetTypeAlternatives_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TargetType__TargetTypeAssignment"


    // $ANTLR start "rule__AttributeCheck__AttributeNameAssignment"
    // InternalSeleniumDSL.g:2821:1: rule__AttributeCheck__AttributeNameAssignment : ( ( rule__AttributeCheck__AttributeNameAlternatives_0 ) ) ;
    public final void rule__AttributeCheck__AttributeNameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2825:1: ( ( ( rule__AttributeCheck__AttributeNameAlternatives_0 ) ) )
            // InternalSeleniumDSL.g:2826:2: ( ( rule__AttributeCheck__AttributeNameAlternatives_0 ) )
            {
            // InternalSeleniumDSL.g:2826:2: ( ( rule__AttributeCheck__AttributeNameAlternatives_0 ) )
            // InternalSeleniumDSL.g:2827:3: ( rule__AttributeCheck__AttributeNameAlternatives_0 )
            {
             before(grammarAccess.getAttributeCheckAccess().getAttributeNameAlternatives_0()); 
            // InternalSeleniumDSL.g:2828:3: ( rule__AttributeCheck__AttributeNameAlternatives_0 )
            // InternalSeleniumDSL.g:2828:4: rule__AttributeCheck__AttributeNameAlternatives_0
            {
            pushFollow(FOLLOW_2);
            rule__AttributeCheck__AttributeNameAlternatives_0();

            state._fsp--;


            }

             after(grammarAccess.getAttributeCheckAccess().getAttributeNameAlternatives_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeCheck__AttributeNameAssignment"


    // $ANTLR start "rule__VariableName__NameAssignment"
    // InternalSeleniumDSL.g:2836:1: rule__VariableName__NameAssignment : ( RULE_ID ) ;
    public final void rule__VariableName__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2840:1: ( ( RULE_ID ) )
            // InternalSeleniumDSL.g:2841:2: ( RULE_ID )
            {
            // InternalSeleniumDSL.g:2841:2: ( RULE_ID )
            // InternalSeleniumDSL.g:2842:3: RULE_ID
            {
             before(grammarAccess.getVariableNameAccess().getNameIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getVariableNameAccess().getNameIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableName__NameAssignment"


    // $ANTLR start "rule__Condition__AttributeCheckAssignment_1"
    // InternalSeleniumDSL.g:2851:1: rule__Condition__AttributeCheckAssignment_1 : ( ruleAttributeCheck ) ;
    public final void rule__Condition__AttributeCheckAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2855:1: ( ( ruleAttributeCheck ) )
            // InternalSeleniumDSL.g:2856:2: ( ruleAttributeCheck )
            {
            // InternalSeleniumDSL.g:2856:2: ( ruleAttributeCheck )
            // InternalSeleniumDSL.g:2857:3: ruleAttributeCheck
            {
             before(grammarAccess.getConditionAccess().getAttributeCheckAttributeCheckParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleAttributeCheck();

            state._fsp--;

             after(grammarAccess.getConditionAccess().getAttributeCheckAttributeCheckParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__AttributeCheckAssignment_1"


    // $ANTLR start "rule__Condition__OperatorAssignment_2"
    // InternalSeleniumDSL.g:2866:1: rule__Condition__OperatorAssignment_2 : ( ( rule__Condition__OperatorAlternatives_2_0 ) ) ;
    public final void rule__Condition__OperatorAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2870:1: ( ( ( rule__Condition__OperatorAlternatives_2_0 ) ) )
            // InternalSeleniumDSL.g:2871:2: ( ( rule__Condition__OperatorAlternatives_2_0 ) )
            {
            // InternalSeleniumDSL.g:2871:2: ( ( rule__Condition__OperatorAlternatives_2_0 ) )
            // InternalSeleniumDSL.g:2872:3: ( rule__Condition__OperatorAlternatives_2_0 )
            {
             before(grammarAccess.getConditionAccess().getOperatorAlternatives_2_0()); 
            // InternalSeleniumDSL.g:2873:3: ( rule__Condition__OperatorAlternatives_2_0 )
            // InternalSeleniumDSL.g:2873:4: rule__Condition__OperatorAlternatives_2_0
            {
            pushFollow(FOLLOW_2);
            rule__Condition__OperatorAlternatives_2_0();

            state._fsp--;


            }

             after(grammarAccess.getConditionAccess().getOperatorAlternatives_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__OperatorAssignment_2"


    // $ANTLR start "rule__Condition__ValueAssignment_3"
    // InternalSeleniumDSL.g:2881:1: rule__Condition__ValueAssignment_3 : ( ruleValue ) ;
    public final void rule__Condition__ValueAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2885:1: ( ( ruleValue ) )
            // InternalSeleniumDSL.g:2886:2: ( ruleValue )
            {
            // InternalSeleniumDSL.g:2886:2: ( ruleValue )
            // InternalSeleniumDSL.g:2887:3: ruleValue
            {
             before(grammarAccess.getConditionAccess().getValueValueParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleValue();

            state._fsp--;

             after(grammarAccess.getConditionAccess().getValueValueParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__ValueAssignment_3"


    // $ANTLR start "rule__Select__TypeComboSelectionAssignment_2"
    // InternalSeleniumDSL.g:2896:1: rule__Select__TypeComboSelectionAssignment_2 : ( ruleTypeComboSelection ) ;
    public final void rule__Select__TypeComboSelectionAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2900:1: ( ( ruleTypeComboSelection ) )
            // InternalSeleniumDSL.g:2901:2: ( ruleTypeComboSelection )
            {
            // InternalSeleniumDSL.g:2901:2: ( ruleTypeComboSelection )
            // InternalSeleniumDSL.g:2902:3: ruleTypeComboSelection
            {
             before(grammarAccess.getSelectAccess().getTypeComboSelectionTypeComboSelectionParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleTypeComboSelection();

            state._fsp--;

             after(grammarAccess.getSelectAccess().getTypeComboSelectionTypeComboSelectionParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Select__TypeComboSelectionAssignment_2"


    // $ANTLR start "rule__Select__ValueAssignment_3"
    // InternalSeleniumDSL.g:2911:1: rule__Select__ValueAssignment_3 : ( ruleValue ) ;
    public final void rule__Select__ValueAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2915:1: ( ( ruleValue ) )
            // InternalSeleniumDSL.g:2916:2: ( ruleValue )
            {
            // InternalSeleniumDSL.g:2916:2: ( ruleValue )
            // InternalSeleniumDSL.g:2917:3: ruleValue
            {
             before(grammarAccess.getSelectAccess().getValueValueParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleValue();

            state._fsp--;

             after(grammarAccess.getSelectAccess().getValueValueParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Select__ValueAssignment_3"


    // $ANTLR start "rule__Select__ConditionAssignment_6"
    // InternalSeleniumDSL.g:2926:1: rule__Select__ConditionAssignment_6 : ( ruleCondition ) ;
    public final void rule__Select__ConditionAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2930:1: ( ( ruleCondition ) )
            // InternalSeleniumDSL.g:2931:2: ( ruleCondition )
            {
            // InternalSeleniumDSL.g:2931:2: ( ruleCondition )
            // InternalSeleniumDSL.g:2932:3: ruleCondition
            {
             before(grammarAccess.getSelectAccess().getConditionConditionParserRuleCall_6_0()); 
            pushFollow(FOLLOW_2);
            ruleCondition();

            state._fsp--;

             after(grammarAccess.getSelectAccess().getConditionConditionParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Select__ConditionAssignment_6"


    // $ANTLR start "rule__Check__ToCheckAssignment_0"
    // InternalSeleniumDSL.g:2941:1: rule__Check__ToCheckAssignment_0 : ( ( rule__Check__ToCheckAlternatives_0_0 ) ) ;
    public final void rule__Check__ToCheckAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2945:1: ( ( ( rule__Check__ToCheckAlternatives_0_0 ) ) )
            // InternalSeleniumDSL.g:2946:2: ( ( rule__Check__ToCheckAlternatives_0_0 ) )
            {
            // InternalSeleniumDSL.g:2946:2: ( ( rule__Check__ToCheckAlternatives_0_0 ) )
            // InternalSeleniumDSL.g:2947:3: ( rule__Check__ToCheckAlternatives_0_0 )
            {
             before(grammarAccess.getCheckAccess().getToCheckAlternatives_0_0()); 
            // InternalSeleniumDSL.g:2948:3: ( rule__Check__ToCheckAlternatives_0_0 )
            // InternalSeleniumDSL.g:2948:4: rule__Check__ToCheckAlternatives_0_0
            {
            pushFollow(FOLLOW_2);
            rule__Check__ToCheckAlternatives_0_0();

            state._fsp--;


            }

             after(grammarAccess.getCheckAccess().getToCheckAlternatives_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Check__ToCheckAssignment_0"


    // $ANTLR start "rule__Check__AllAssignment_1"
    // InternalSeleniumDSL.g:2956:1: rule__Check__AllAssignment_1 : ( ( 'all' ) ) ;
    public final void rule__Check__AllAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2960:1: ( ( ( 'all' ) ) )
            // InternalSeleniumDSL.g:2961:2: ( ( 'all' ) )
            {
            // InternalSeleniumDSL.g:2961:2: ( ( 'all' ) )
            // InternalSeleniumDSL.g:2962:3: ( 'all' )
            {
             before(grammarAccess.getCheckAccess().getAllAllKeyword_1_0()); 
            // InternalSeleniumDSL.g:2963:3: ( 'all' )
            // InternalSeleniumDSL.g:2964:4: 'all'
            {
             before(grammarAccess.getCheckAccess().getAllAllKeyword_1_0()); 
            match(input,48,FOLLOW_2); 
             after(grammarAccess.getCheckAccess().getAllAllKeyword_1_0()); 

            }

             after(grammarAccess.getCheckAccess().getAllAllKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Check__AllAssignment_1"


    // $ANTLR start "rule__Check__TargetTypeAssignment_2"
    // InternalSeleniumDSL.g:2975:1: rule__Check__TargetTypeAssignment_2 : ( ruleTargetType ) ;
    public final void rule__Check__TargetTypeAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2979:1: ( ( ruleTargetType ) )
            // InternalSeleniumDSL.g:2980:2: ( ruleTargetType )
            {
            // InternalSeleniumDSL.g:2980:2: ( ruleTargetType )
            // InternalSeleniumDSL.g:2981:3: ruleTargetType
            {
             before(grammarAccess.getCheckAccess().getTargetTypeTargetTypeParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleTargetType();

            state._fsp--;

             after(grammarAccess.getCheckAccess().getTargetTypeTargetTypeParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Check__TargetTypeAssignment_2"


    // $ANTLR start "rule__Check__ConditionAssignment_3"
    // InternalSeleniumDSL.g:2990:1: rule__Check__ConditionAssignment_3 : ( ruleCondition ) ;
    public final void rule__Check__ConditionAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:2994:1: ( ( ruleCondition ) )
            // InternalSeleniumDSL.g:2995:2: ( ruleCondition )
            {
            // InternalSeleniumDSL.g:2995:2: ( ruleCondition )
            // InternalSeleniumDSL.g:2996:3: ruleCondition
            {
             before(grammarAccess.getCheckAccess().getConditionConditionParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleCondition();

            state._fsp--;

             after(grammarAccess.getCheckAccess().getConditionConditionParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Check__ConditionAssignment_3"


    // $ANTLR start "rule__Click__AllAssignment_1"
    // InternalSeleniumDSL.g:3005:1: rule__Click__AllAssignment_1 : ( ( 'all' ) ) ;
    public final void rule__Click__AllAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:3009:1: ( ( ( 'all' ) ) )
            // InternalSeleniumDSL.g:3010:2: ( ( 'all' ) )
            {
            // InternalSeleniumDSL.g:3010:2: ( ( 'all' ) )
            // InternalSeleniumDSL.g:3011:3: ( 'all' )
            {
             before(grammarAccess.getClickAccess().getAllAllKeyword_1_0()); 
            // InternalSeleniumDSL.g:3012:3: ( 'all' )
            // InternalSeleniumDSL.g:3013:4: 'all'
            {
             before(grammarAccess.getClickAccess().getAllAllKeyword_1_0()); 
            match(input,48,FOLLOW_2); 
             after(grammarAccess.getClickAccess().getAllAllKeyword_1_0()); 

            }

             after(grammarAccess.getClickAccess().getAllAllKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Click__AllAssignment_1"


    // $ANTLR start "rule__Click__TargetTypeAssignment_2"
    // InternalSeleniumDSL.g:3024:1: rule__Click__TargetTypeAssignment_2 : ( ruleTargetType ) ;
    public final void rule__Click__TargetTypeAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:3028:1: ( ( ruleTargetType ) )
            // InternalSeleniumDSL.g:3029:2: ( ruleTargetType )
            {
            // InternalSeleniumDSL.g:3029:2: ( ruleTargetType )
            // InternalSeleniumDSL.g:3030:3: ruleTargetType
            {
             before(grammarAccess.getClickAccess().getTargetTypeTargetTypeParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleTargetType();

            state._fsp--;

             after(grammarAccess.getClickAccess().getTargetTypeTargetTypeParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Click__TargetTypeAssignment_2"


    // $ANTLR start "rule__Click__ConditionAssignment_3"
    // InternalSeleniumDSL.g:3039:1: rule__Click__ConditionAssignment_3 : ( ruleCondition ) ;
    public final void rule__Click__ConditionAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:3043:1: ( ( ruleCondition ) )
            // InternalSeleniumDSL.g:3044:2: ( ruleCondition )
            {
            // InternalSeleniumDSL.g:3044:2: ( ruleCondition )
            // InternalSeleniumDSL.g:3045:3: ruleCondition
            {
             before(grammarAccess.getClickAccess().getConditionConditionParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleCondition();

            state._fsp--;

             after(grammarAccess.getClickAccess().getConditionConditionParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Click__ConditionAssignment_3"


    // $ANTLR start "rule__Store__AttributeCheckAssignment_1_0_0"
    // InternalSeleniumDSL.g:3054:1: rule__Store__AttributeCheckAssignment_1_0_0 : ( ruleAttributeCheck ) ;
    public final void rule__Store__AttributeCheckAssignment_1_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:3058:1: ( ( ruleAttributeCheck ) )
            // InternalSeleniumDSL.g:3059:2: ( ruleAttributeCheck )
            {
            // InternalSeleniumDSL.g:3059:2: ( ruleAttributeCheck )
            // InternalSeleniumDSL.g:3060:3: ruleAttributeCheck
            {
             before(grammarAccess.getStoreAccess().getAttributeCheckAttributeCheckParserRuleCall_1_0_0_0()); 
            pushFollow(FOLLOW_2);
            ruleAttributeCheck();

            state._fsp--;

             after(grammarAccess.getStoreAccess().getAttributeCheckAttributeCheckParserRuleCall_1_0_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Store__AttributeCheckAssignment_1_0_0"


    // $ANTLR start "rule__Store__TargetTypeAssignment_1_0_2"
    // InternalSeleniumDSL.g:3069:1: rule__Store__TargetTypeAssignment_1_0_2 : ( ruleTargetType ) ;
    public final void rule__Store__TargetTypeAssignment_1_0_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:3073:1: ( ( ruleTargetType ) )
            // InternalSeleniumDSL.g:3074:2: ( ruleTargetType )
            {
            // InternalSeleniumDSL.g:3074:2: ( ruleTargetType )
            // InternalSeleniumDSL.g:3075:3: ruleTargetType
            {
             before(grammarAccess.getStoreAccess().getTargetTypeTargetTypeParserRuleCall_1_0_2_0()); 
            pushFollow(FOLLOW_2);
            ruleTargetType();

            state._fsp--;

             after(grammarAccess.getStoreAccess().getTargetTypeTargetTypeParserRuleCall_1_0_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Store__TargetTypeAssignment_1_0_2"


    // $ANTLR start "rule__Store__ConditionAssignment_1_0_3"
    // InternalSeleniumDSL.g:3084:1: rule__Store__ConditionAssignment_1_0_3 : ( ruleCondition ) ;
    public final void rule__Store__ConditionAssignment_1_0_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:3088:1: ( ( ruleCondition ) )
            // InternalSeleniumDSL.g:3089:2: ( ruleCondition )
            {
            // InternalSeleniumDSL.g:3089:2: ( ruleCondition )
            // InternalSeleniumDSL.g:3090:3: ruleCondition
            {
             before(grammarAccess.getStoreAccess().getConditionConditionParserRuleCall_1_0_3_0()); 
            pushFollow(FOLLOW_2);
            ruleCondition();

            state._fsp--;

             after(grammarAccess.getStoreAccess().getConditionConditionParserRuleCall_1_0_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Store__ConditionAssignment_1_0_3"


    // $ANTLR start "rule__Store__ValueAssignment_1_1"
    // InternalSeleniumDSL.g:3099:1: rule__Store__ValueAssignment_1_1 : ( ruleValue ) ;
    public final void rule__Store__ValueAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:3103:1: ( ( ruleValue ) )
            // InternalSeleniumDSL.g:3104:2: ( ruleValue )
            {
            // InternalSeleniumDSL.g:3104:2: ( ruleValue )
            // InternalSeleniumDSL.g:3105:3: ruleValue
            {
             before(grammarAccess.getStoreAccess().getValueValueParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleValue();

            state._fsp--;

             after(grammarAccess.getStoreAccess().getValueValueParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Store__ValueAssignment_1_1"


    // $ANTLR start "rule__Store__VariableNameAssignment_3"
    // InternalSeleniumDSL.g:3114:1: rule__Store__VariableNameAssignment_3 : ( ruleVariableName ) ;
    public final void rule__Store__VariableNameAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:3118:1: ( ( ruleVariableName ) )
            // InternalSeleniumDSL.g:3119:2: ( ruleVariableName )
            {
            // InternalSeleniumDSL.g:3119:2: ( ruleVariableName )
            // InternalSeleniumDSL.g:3120:3: ruleVariableName
            {
             before(grammarAccess.getStoreAccess().getVariableNameVariableNameParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleVariableName();

            state._fsp--;

             after(grammarAccess.getStoreAccess().getVariableNameVariableNameParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Store__VariableNameAssignment_3"


    // $ANTLR start "rule__Set__ValueAssignment_1"
    // InternalSeleniumDSL.g:3129:1: rule__Set__ValueAssignment_1 : ( ruleValue ) ;
    public final void rule__Set__ValueAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:3133:1: ( ( ruleValue ) )
            // InternalSeleniumDSL.g:3134:2: ( ruleValue )
            {
            // InternalSeleniumDSL.g:3134:2: ( ruleValue )
            // InternalSeleniumDSL.g:3135:3: ruleValue
            {
             before(grammarAccess.getSetAccess().getValueValueParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleValue();

            state._fsp--;

             after(grammarAccess.getSetAccess().getValueValueParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Set__ValueAssignment_1"


    // $ANTLR start "rule__Set__TargetTypeAssignment_3"
    // InternalSeleniumDSL.g:3144:1: rule__Set__TargetTypeAssignment_3 : ( ruleTargetType ) ;
    public final void rule__Set__TargetTypeAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:3148:1: ( ( ruleTargetType ) )
            // InternalSeleniumDSL.g:3149:2: ( ruleTargetType )
            {
            // InternalSeleniumDSL.g:3149:2: ( ruleTargetType )
            // InternalSeleniumDSL.g:3150:3: ruleTargetType
            {
             before(grammarAccess.getSetAccess().getTargetTypeTargetTypeParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleTargetType();

            state._fsp--;

             after(grammarAccess.getSetAccess().getTargetTypeTargetTypeParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Set__TargetTypeAssignment_3"


    // $ANTLR start "rule__Set__ConditionAssignment_4"
    // InternalSeleniumDSL.g:3159:1: rule__Set__ConditionAssignment_4 : ( ruleCondition ) ;
    public final void rule__Set__ConditionAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:3163:1: ( ( ruleCondition ) )
            // InternalSeleniumDSL.g:3164:2: ( ruleCondition )
            {
            // InternalSeleniumDSL.g:3164:2: ( ruleCondition )
            // InternalSeleniumDSL.g:3165:3: ruleCondition
            {
             before(grammarAccess.getSetAccess().getConditionConditionParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleCondition();

            state._fsp--;

             after(grammarAccess.getSetAccess().getConditionConditionParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Set__ConditionAssignment_4"


    // $ANTLR start "rule__IsElementPresent__TargetAssignment_1"
    // InternalSeleniumDSL.g:3174:1: rule__IsElementPresent__TargetAssignment_1 : ( ruleTargetType ) ;
    public final void rule__IsElementPresent__TargetAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:3178:1: ( ( ruleTargetType ) )
            // InternalSeleniumDSL.g:3179:2: ( ruleTargetType )
            {
            // InternalSeleniumDSL.g:3179:2: ( ruleTargetType )
            // InternalSeleniumDSL.g:3180:3: ruleTargetType
            {
             before(grammarAccess.getIsElementPresentAccess().getTargetTargetTypeParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleTargetType();

            state._fsp--;

             after(grammarAccess.getIsElementPresentAccess().getTargetTargetTypeParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IsElementPresent__TargetAssignment_1"


    // $ANTLR start "rule__IsElementPresent__ConditionAssignment_2"
    // InternalSeleniumDSL.g:3189:1: rule__IsElementPresent__ConditionAssignment_2 : ( ruleCondition ) ;
    public final void rule__IsElementPresent__ConditionAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:3193:1: ( ( ruleCondition ) )
            // InternalSeleniumDSL.g:3194:2: ( ruleCondition )
            {
            // InternalSeleniumDSL.g:3194:2: ( ruleCondition )
            // InternalSeleniumDSL.g:3195:3: ruleCondition
            {
             before(grammarAccess.getIsElementPresentAccess().getConditionConditionParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleCondition();

            state._fsp--;

             after(grammarAccess.getIsElementPresentAccess().getConditionConditionParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IsElementPresent__ConditionAssignment_2"


    // $ANTLR start "rule__Count__TargetTypeAssignment_1"
    // InternalSeleniumDSL.g:3204:1: rule__Count__TargetTypeAssignment_1 : ( ruleTargetType ) ;
    public final void rule__Count__TargetTypeAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:3208:1: ( ( ruleTargetType ) )
            // InternalSeleniumDSL.g:3209:2: ( ruleTargetType )
            {
            // InternalSeleniumDSL.g:3209:2: ( ruleTargetType )
            // InternalSeleniumDSL.g:3210:3: ruleTargetType
            {
             before(grammarAccess.getCountAccess().getTargetTypeTargetTypeParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleTargetType();

            state._fsp--;

             after(grammarAccess.getCountAccess().getTargetTypeTargetTypeParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Count__TargetTypeAssignment_1"


    // $ANTLR start "rule__Count__ConditionAssignment_2"
    // InternalSeleniumDSL.g:3219:1: rule__Count__ConditionAssignment_2 : ( ruleCondition ) ;
    public final void rule__Count__ConditionAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:3223:1: ( ( ruleCondition ) )
            // InternalSeleniumDSL.g:3224:2: ( ruleCondition )
            {
            // InternalSeleniumDSL.g:3224:2: ( ruleCondition )
            // InternalSeleniumDSL.g:3225:3: ruleCondition
            {
             before(grammarAccess.getCountAccess().getConditionConditionParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleCondition();

            state._fsp--;

             after(grammarAccess.getCountAccess().getConditionConditionParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Count__ConditionAssignment_2"


    // $ANTLR start "rule__Count__VariableNameAssignment_4"
    // InternalSeleniumDSL.g:3234:1: rule__Count__VariableNameAssignment_4 : ( ruleVariableName ) ;
    public final void rule__Count__VariableNameAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:3238:1: ( ( ruleVariableName ) )
            // InternalSeleniumDSL.g:3239:2: ( ruleVariableName )
            {
            // InternalSeleniumDSL.g:3239:2: ( ruleVariableName )
            // InternalSeleniumDSL.g:3240:3: ruleVariableName
            {
             before(grammarAccess.getCountAccess().getVariableNameVariableNameParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleVariableName();

            state._fsp--;

             after(grammarAccess.getCountAccess().getVariableNameVariableNameParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Count__VariableNameAssignment_4"


    // $ANTLR start "rule__Argument__NameAssignment"
    // InternalSeleniumDSL.g:3249:1: rule__Argument__NameAssignment : ( RULE_ID ) ;
    public final void rule__Argument__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:3253:1: ( ( RULE_ID ) )
            // InternalSeleniumDSL.g:3254:2: ( RULE_ID )
            {
            // InternalSeleniumDSL.g:3254:2: ( RULE_ID )
            // InternalSeleniumDSL.g:3255:3: RULE_ID
            {
             before(grammarAccess.getArgumentAccess().getNameIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getArgumentAccess().getNameIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Argument__NameAssignment"


    // $ANTLR start "rule__FunctionName__NameAssignment"
    // InternalSeleniumDSL.g:3264:1: rule__FunctionName__NameAssignment : ( RULE_ID ) ;
    public final void rule__FunctionName__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:3268:1: ( ( RULE_ID ) )
            // InternalSeleniumDSL.g:3269:2: ( RULE_ID )
            {
            // InternalSeleniumDSL.g:3269:2: ( RULE_ID )
            // InternalSeleniumDSL.g:3270:3: RULE_ID
            {
             before(grammarAccess.getFunctionNameAccess().getNameIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getFunctionNameAccess().getNameIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionName__NameAssignment"


    // $ANTLR start "rule__Function__FunctionNameAssignment_1"
    // InternalSeleniumDSL.g:3279:1: rule__Function__FunctionNameAssignment_1 : ( ruleFunctionName ) ;
    public final void rule__Function__FunctionNameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:3283:1: ( ( ruleFunctionName ) )
            // InternalSeleniumDSL.g:3284:2: ( ruleFunctionName )
            {
            // InternalSeleniumDSL.g:3284:2: ( ruleFunctionName )
            // InternalSeleniumDSL.g:3285:3: ruleFunctionName
            {
             before(grammarAccess.getFunctionAccess().getFunctionNameFunctionNameParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleFunctionName();

            state._fsp--;

             after(grammarAccess.getFunctionAccess().getFunctionNameFunctionNameParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__FunctionNameAssignment_1"


    // $ANTLR start "rule__Function__ArgumentsAssignment_3"
    // InternalSeleniumDSL.g:3294:1: rule__Function__ArgumentsAssignment_3 : ( ruleArgument ) ;
    public final void rule__Function__ArgumentsAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:3298:1: ( ( ruleArgument ) )
            // InternalSeleniumDSL.g:3299:2: ( ruleArgument )
            {
            // InternalSeleniumDSL.g:3299:2: ( ruleArgument )
            // InternalSeleniumDSL.g:3300:3: ruleArgument
            {
             before(grammarAccess.getFunctionAccess().getArgumentsArgumentParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleArgument();

            state._fsp--;

             after(grammarAccess.getFunctionAccess().getArgumentsArgumentParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__ArgumentsAssignment_3"


    // $ANTLR start "rule__Function__InstructionsAssignment_5"
    // InternalSeleniumDSL.g:3309:1: rule__Function__InstructionsAssignment_5 : ( ruleSeleniumInstruction ) ;
    public final void rule__Function__InstructionsAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:3313:1: ( ( ruleSeleniumInstruction ) )
            // InternalSeleniumDSL.g:3314:2: ( ruleSeleniumInstruction )
            {
            // InternalSeleniumDSL.g:3314:2: ( ruleSeleniumInstruction )
            // InternalSeleniumDSL.g:3315:3: ruleSeleniumInstruction
            {
             before(grammarAccess.getFunctionAccess().getInstructionsSeleniumInstructionParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleSeleniumInstruction();

            state._fsp--;

             after(grammarAccess.getFunctionAccess().getInstructionsSeleniumInstructionParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__InstructionsAssignment_5"


    // $ANTLR start "rule__FunctionCall__FunctionNameAssignment_0"
    // InternalSeleniumDSL.g:3324:1: rule__FunctionCall__FunctionNameAssignment_0 : ( ruleFunctionName ) ;
    public final void rule__FunctionCall__FunctionNameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:3328:1: ( ( ruleFunctionName ) )
            // InternalSeleniumDSL.g:3329:2: ( ruleFunctionName )
            {
            // InternalSeleniumDSL.g:3329:2: ( ruleFunctionName )
            // InternalSeleniumDSL.g:3330:3: ruleFunctionName
            {
             before(grammarAccess.getFunctionCallAccess().getFunctionNameFunctionNameParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleFunctionName();

            state._fsp--;

             after(grammarAccess.getFunctionCallAccess().getFunctionNameFunctionNameParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionCall__FunctionNameAssignment_0"


    // $ANTLR start "rule__FunctionCall__ValuesAssignment_2"
    // InternalSeleniumDSL.g:3339:1: rule__FunctionCall__ValuesAssignment_2 : ( ruleValue ) ;
    public final void rule__FunctionCall__ValuesAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:3343:1: ( ( ruleValue ) )
            // InternalSeleniumDSL.g:3344:2: ( ruleValue )
            {
            // InternalSeleniumDSL.g:3344:2: ( ruleValue )
            // InternalSeleniumDSL.g:3345:3: ruleValue
            {
             before(grammarAccess.getFunctionCallAccess().getValuesValueParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleValue();

            state._fsp--;

             after(grammarAccess.getFunctionCallAccess().getValuesValueParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionCall__ValuesAssignment_2"


    // $ANTLR start "rule__Assert__Value1Assignment_1"
    // InternalSeleniumDSL.g:3354:1: rule__Assert__Value1Assignment_1 : ( ruleValue ) ;
    public final void rule__Assert__Value1Assignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:3358:1: ( ( ruleValue ) )
            // InternalSeleniumDSL.g:3359:2: ( ruleValue )
            {
            // InternalSeleniumDSL.g:3359:2: ( ruleValue )
            // InternalSeleniumDSL.g:3360:3: ruleValue
            {
             before(grammarAccess.getAssertAccess().getValue1ValueParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleValue();

            state._fsp--;

             after(grammarAccess.getAssertAccess().getValue1ValueParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assert__Value1Assignment_1"


    // $ANTLR start "rule__Assert__NegateAssignment_2_0"
    // InternalSeleniumDSL.g:3369:1: rule__Assert__NegateAssignment_2_0 : ( ( 'not' ) ) ;
    public final void rule__Assert__NegateAssignment_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:3373:1: ( ( ( 'not' ) ) )
            // InternalSeleniumDSL.g:3374:2: ( ( 'not' ) )
            {
            // InternalSeleniumDSL.g:3374:2: ( ( 'not' ) )
            // InternalSeleniumDSL.g:3375:3: ( 'not' )
            {
             before(grammarAccess.getAssertAccess().getNegateNotKeyword_2_0_0()); 
            // InternalSeleniumDSL.g:3376:3: ( 'not' )
            // InternalSeleniumDSL.g:3377:4: 'not'
            {
             before(grammarAccess.getAssertAccess().getNegateNotKeyword_2_0_0()); 
            match(input,49,FOLLOW_2); 
             after(grammarAccess.getAssertAccess().getNegateNotKeyword_2_0_0()); 

            }

             after(grammarAccess.getAssertAccess().getNegateNotKeyword_2_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assert__NegateAssignment_2_0"


    // $ANTLR start "rule__Assert__Value2Assignment_3"
    // InternalSeleniumDSL.g:3388:1: rule__Assert__Value2Assignment_3 : ( ruleValue ) ;
    public final void rule__Assert__Value2Assignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSeleniumDSL.g:3392:1: ( ( ruleValue ) )
            // InternalSeleniumDSL.g:3393:2: ( ruleValue )
            {
            // InternalSeleniumDSL.g:3393:2: ( ruleValue )
            // InternalSeleniumDSL.g:3394:3: ruleValue
            {
             before(grammarAccess.getAssertAccess().getValue2ValueParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleValue();

            state._fsp--;

             after(grammarAccess.getAssertAccess().getValue2ValueParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assert__Value2Assignment_3"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000080000000002L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x000086B158000820L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x000086B158000022L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000007000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000800010L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000003000000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000004800000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x00010000007F8000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000800030L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000100000000000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000200000000020L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000000000022L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000C6B158000020L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000200000000030L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000000000032L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0002000002000000L});

}