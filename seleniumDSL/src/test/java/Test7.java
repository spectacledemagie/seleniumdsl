import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import static org.junit.Assert.*;

public class Test7 extends AbstractSeleniumTest {

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        init();
    }

    @Test
    public void test7() throws Exception {
        try {
            driver.get("http://www.imt-atlantique.fr/fr");

            String titleLink = getElement("//div[@class='actu_home_ctner_inner_cell1_titre']").getText();
            String urlLink = getElement("//a[@title='"+titleLink+"']").getAttribute("href");

            driver.get("http://www.imt-atlantique.fr/fr/rechercher");

            setValue("//input[contains(@id, 'edit-search-api-fulltext')]", titleLink);
            click("//input[contains(@value, 'Appliquer les filtres')]");

            assertFalse(isElementPresent("//a[contains(@href, '"+urlLink+"')]"));
        } catch(Exception e) {
            File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(scrFile, new File("src/main/resources/screenshots/test7.png"));
            throw new RuntimeException(e);
        }
    }

    @AfterClass
    public static void setUpAfterClass() {
        close();
    }
}
