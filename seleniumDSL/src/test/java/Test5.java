import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;
import static org.junit.Assert.*;

public class Test5 extends AbstractSeleniumTest {

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        init();
    }

    @Test
    public void test5() throws Exception {
        try {
            driver.get("http://www.imt-atlantique.fr/fr/formation/trouver-ma-formation");

            clickAllCheckboxes("//input[@type='checkbox']", false);

            clickCheckbox("//label[contains(text(), 'Anglais')]", true);
            clickCheckbox("//label[contains(text(), 'A domicile')]", true);

            click("//input[contains(@value, 'Appliquer les critères')]");

            assertTrue(isElementPresent("//div[contains(text(), 'Aucune formation trouvée répondant à vos critères')]"));
        } catch(Exception e) {
            File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(scrFile, new File("src/main/resources/screenshots/test5.png"));
            throw new RuntimeException(e);
        }
    }

    @AfterClass
    public static void setUpAfterClass() {
        close();
    }
}
