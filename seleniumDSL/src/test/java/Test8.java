import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class Test8 extends AbstractSeleniumTest {

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        init();
    }

    @Test
    public void test8() throws Exception {
        try {
            driver.get("http://www.imt-atlantique.fr/fr/rechercher");

            setValue("//input[contains(@id, 'edit-search-api-fulltext')]", "2007");
            click("//input[contains(@value, 'Appliquer les filtres')]");

            int nbResult1 = count("//div[@class='views-row']");
            selectComboOptionByText("//select[@id='edit-selecteur']", "Le mois dernier");
            click("//input[contains(@value, 'Appliquer les filtres')]");
            int nbResult2 = count("//div[@class='views-row']");

            assertEquals(nbResult1, nbResult2);

        } catch(Exception e) {
            File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(scrFile, new File("src/main/resources/screenshots/test8.png"));
            throw new RuntimeException(e);
        }
    }

    @AfterClass
    public static void setUpAfterClass() {
        close();
    }
}
