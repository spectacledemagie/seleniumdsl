import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;
import static org.junit.Assert.*;

public class Test4 extends AbstractSeleniumTest {

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
       init();
    }

    @Test
    public void test4() throws InterruptedException {
        driver.get("http://www.imt-atlantique.fr/fr/rechercher");

        setValue("//input[contains(@id, 'edit-search-api-fulltext')]", "Donald Trump");
        click("//input[contains(@value, 'Appliquer les filtres')]");
        assertTrue(isElementPresent("//div[contains(text(), 'Aucun résultat ne correspond à votre recherche')]"));
    }

    @AfterClass
    public static void setUpAfterClass() {
        close();
    }
}
