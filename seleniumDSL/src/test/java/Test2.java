import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;
import static org.junit.Assert.*;

public class Test2 extends AbstractSeleniumTest {
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        init();
    }

    @Test
    public void test2() throws InterruptedException {
        driver.get("http://www.imt-atlantique.fr/fr");

        click("//a[text()='Toutes les actualités']");

        assertTrue(isElementPresent("//a[contains(text(), 'Accueil')]"));
    }

    @AfterClass
    public static void setUpAfterClass() {
        close();
    }
}
