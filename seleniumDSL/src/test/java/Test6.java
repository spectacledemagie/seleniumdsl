import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.*;

import java.io.File;

import static org.junit.Assert.*;

public class Test6 extends AbstractSeleniumTest {

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        init();
    }

    @Test
    public void test6() throws Exception {
        try {
            driver.get("http://www.imt-atlantique.fr/fr/rechercher");

            setValue("//input[contains(@id, 'edit-search-api-fulltext')]", "2017");

            selectComboOptionByText("//select[@id='edit-selecteur']", "Le mois dernier");

            click("//input[contains(@value, 'Appliquer les filtres')]");

            assertTrue(isElementPresent("//div[contains(text(), 'Aucun résultat ne correspond à votre recherche')]"));
        } catch(Exception e) {
            File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(scrFile, new File("src/main/resources/screenshots/test6.png"));
            throw new RuntimeException(e);
        }
    }

    @AfterClass
    public static void setUpAfterClass() {
        close();
    }
}
