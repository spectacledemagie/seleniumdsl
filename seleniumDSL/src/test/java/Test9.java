import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class Test9 extends AbstractSeleniumTest {

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        init();
    }

    private static void testImpression(String titrePage) {
        click("//a[text()='"+titrePage+"']");
        assertTrue(isElementPresent("//a[@title='Imprimer']"));
        click("//a[@title='Imprimer']");
    }

    @Test
    public void test9() throws Exception {
        try {
            driver.get("http://www.imt-atlantique.fr/fr");

            testImpression("Communiqués de Presse");
            testImpression("Dossier de presse");
            testImpression("Visuels pour la presse");
            testImpression("La presse en parle");
            testImpression("Tribunes de presse");
            testImpression("Les palmarès");
        } catch(Exception e) {
            File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(scrFile, new File("src/main/resources/screenshots/test9.png"));
            throw new RuntimeException(e);
        }
    }

    @AfterClass
    public static void setUpAfterClass() {
        close();
    }
}
