import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import static org.junit.Assert.*;

public class Test1 extends AbstractSeleniumTest {

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        init();
    }

    @Test
    public void test1() {
        driver.get("http://www.imt-atlantique.fr/fr");

        assertTrue(isElementPresent("//a[text()='Toutes les actualités']"));
    }

    @AfterClass
    public static void setUpAfterClass() {
        close();
    }
}
