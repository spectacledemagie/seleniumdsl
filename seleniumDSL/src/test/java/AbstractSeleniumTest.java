import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class AbstractSeleniumTest {
    protected static WebDriver driver;
    protected static JavascriptExecutor je;
    protected static WebDriverWait wait;
    protected static final String SCROLL_TO = "window.scrollTo(0, %d)";

    protected static void init() throws MalformedURLException {
        ChromeOptions options_chrome = new ChromeOptions();
        FirefoxOptions options_firefox = new FirefoxOptions();

        DesiredCapabilities capabilities_chrome = DesiredCapabilities.chrome();
        DesiredCapabilities capabilities_firefox = DesiredCapabilities.firefox();

        capabilities_chrome.setCapability(ChromeOptions.CAPABILITY, options_chrome);
        capabilities_firefox.setCapability(FirefoxOptions.FIREFOX_OPTIONS, options_firefox);

        driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capabilities_chrome);
//        driver = new FirefoxDriver(new URL("http://localhost:4444/wd/hub"), capabilities_firefox);

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        je = (JavascriptExecutor) driver;
        wait = new WebDriverWait(driver, 5);
    }

    protected static void close() {
        driver.quit();
    }

    protected static boolean isElementPresent(String xpath) {
        try {
            driver.findElement(By.xpath(xpath));

            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    protected static void click(String xpath) {
        List<WebElement> elements = driver.findElements(By.xpath(xpath));

        for (WebElement element : elements) {
            if (element.isDisplayed()) {
                je.executeScript(String.format(SCROLL_TO, element.getLocation().getY()-200));
                element.click();
                break;
            }
        }
    }

    protected static void clickAll(String xpath) {
        List<WebElement> elements = driver.findElements(By.xpath(xpath));

        for (WebElement element : elements) {
            if (element.isDisplayed()) {
                je.executeScript(String.format(SCROLL_TO, element.getLocation().getY()-200));
                element.click();
            }
        }
    }

    protected static int count(String xpath) {
        List<WebElement> elements = driver.findElements(By.xpath(xpath));

        int compteur = 0;

        for (WebElement element : elements) {
            if (element.isDisplayed()) {
                compteur++;
            }
        }

        return compteur;
    }

    protected static void clickAllCheckboxes(String xpath, boolean check) {
        List<WebElement> elements = driver.findElements(By.xpath(xpath));

        for (WebElement element : elements) {
            if (element.isDisplayed()) {
                je.executeScript(String.format(SCROLL_TO, element.getLocation().getY()-200));

                if ((!element.isSelected() && check) || (element.isSelected() && !check)) {
                    element.click();
                }
            }
        }
    }

    protected static void clickCheckbox(String xpath, boolean check) {
        List<WebElement> elements = driver.findElements(By.xpath(xpath));

        for (WebElement element : elements) {
            if (element.isDisplayed()) {
                je.executeScript(String.format(SCROLL_TO, element.getLocation().getY()-200));

                if ((!element.isSelected() && check) || (element.isSelected() && !check)) {
                    element.click();
                }
                break;
            }
        }
    }

    protected static void setValue(String xpath, String value) {
        List<WebElement> elements = driver.findElements(By.xpath(xpath));

        for (WebElement element : elements) {
            if (element.isDisplayed()) {
                je.executeScript(String.format(SCROLL_TO, element.getLocation().getY()-200));
                element.sendKeys(value);
                break;
            }
        }
    }

    protected static void selectComboOptionByText(String xpath, String optionText) {
        List<WebElement> elements = driver.findElements(By.xpath(xpath));

        for (WebElement element : elements) {
            if (element.isDisplayed()) {
                je.executeScript(String.format(SCROLL_TO, element.getLocation().getY()-200));
                Select select = new Select(element);
                select.selectByVisibleText(optionText);
            }
        }
    }

    protected static void selectComboOptionByValue(String xpath, String optionValue) {
        List<WebElement> elements = driver.findElements(By.xpath(xpath));

        for (WebElement element : elements) {
            if (element.isDisplayed()) {
                je.executeScript(String.format(SCROLL_TO, element.getLocation().getY()-200));
                Select select = new Select(element);
                select.selectByValue(optionValue);
            }
        }
    }

    protected static WebElement getElement(String xpath) {
        List<WebElement> elements = driver.findElements(By.xpath(xpath));

        for (WebElement element : elements) {
            if (element.isDisplayed()) {
                return element;
            }
        }

        throw new RuntimeException("Pas d'élément trouvé");
    }
}
