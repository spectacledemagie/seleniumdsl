package org.xtext.selenium.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.selenium.services.SeleniumDSLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalSeleniumDSLParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'chrome'", "'firefox'", "'opera'", "'open'", "'close'", "'get'", "'button'", "'link'", "'image'", "'checkbox'", "'input'", "'div'", "'span'", "'label'", "'text'", "'where'", "'contains'", "'equals'", "'value'", "'select'", "'option'", "'in'", "'combo'", "'check'", "'uncheck'", "'all'", "'click'", "'store'", "'of'", "'set'", "'to'", "'isElementPresent'", "'count'", "'define'", "'('", "')'", "';'", "'assert'", "'not'"
    };
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int RULE_ID=5;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalSeleniumDSLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalSeleniumDSLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalSeleniumDSLParser.tokenNames; }
    public String getGrammarFileName() { return "InternalSeleniumDSL.g"; }



     	private SeleniumDSLGrammarAccess grammarAccess;

        public InternalSeleniumDSLParser(TokenStream input, SeleniumDSLGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "SeleniumProgram";
       	}

       	@Override
       	protected SeleniumDSLGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleSeleniumProgram"
    // InternalSeleniumDSL.g:64:1: entryRuleSeleniumProgram returns [EObject current=null] : iv_ruleSeleniumProgram= ruleSeleniumProgram EOF ;
    public final EObject entryRuleSeleniumProgram() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSeleniumProgram = null;


        try {
            // InternalSeleniumDSL.g:64:56: (iv_ruleSeleniumProgram= ruleSeleniumProgram EOF )
            // InternalSeleniumDSL.g:65:2: iv_ruleSeleniumProgram= ruleSeleniumProgram EOF
            {
             newCompositeNode(grammarAccess.getSeleniumProgramRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSeleniumProgram=ruleSeleniumProgram();

            state._fsp--;

             current =iv_ruleSeleniumProgram; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSeleniumProgram"


    // $ANTLR start "ruleSeleniumProgram"
    // InternalSeleniumDSL.g:71:1: ruleSeleniumProgram returns [EObject current=null] : ( ( (lv_functions_0_0= ruleFunction ) )* ( (lv_open_1_0= ruleOpen ) ) ( (lv_seleniumInstruction_2_0= ruleSeleniumInstruction ) )* ( (lv_close_3_0= ruleClose ) ) ) ;
    public final EObject ruleSeleniumProgram() throws RecognitionException {
        EObject current = null;

        EObject lv_functions_0_0 = null;

        EObject lv_open_1_0 = null;

        EObject lv_seleniumInstruction_2_0 = null;

        AntlrDatatypeRuleToken lv_close_3_0 = null;



        	enterRule();

        try {
            // InternalSeleniumDSL.g:77:2: ( ( ( (lv_functions_0_0= ruleFunction ) )* ( (lv_open_1_0= ruleOpen ) ) ( (lv_seleniumInstruction_2_0= ruleSeleniumInstruction ) )* ( (lv_close_3_0= ruleClose ) ) ) )
            // InternalSeleniumDSL.g:78:2: ( ( (lv_functions_0_0= ruleFunction ) )* ( (lv_open_1_0= ruleOpen ) ) ( (lv_seleniumInstruction_2_0= ruleSeleniumInstruction ) )* ( (lv_close_3_0= ruleClose ) ) )
            {
            // InternalSeleniumDSL.g:78:2: ( ( (lv_functions_0_0= ruleFunction ) )* ( (lv_open_1_0= ruleOpen ) ) ( (lv_seleniumInstruction_2_0= ruleSeleniumInstruction ) )* ( (lv_close_3_0= ruleClose ) ) )
            // InternalSeleniumDSL.g:79:3: ( (lv_functions_0_0= ruleFunction ) )* ( (lv_open_1_0= ruleOpen ) ) ( (lv_seleniumInstruction_2_0= ruleSeleniumInstruction ) )* ( (lv_close_3_0= ruleClose ) )
            {
            // InternalSeleniumDSL.g:79:3: ( (lv_functions_0_0= ruleFunction ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==44) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalSeleniumDSL.g:80:4: (lv_functions_0_0= ruleFunction )
            	    {
            	    // InternalSeleniumDSL.g:80:4: (lv_functions_0_0= ruleFunction )
            	    // InternalSeleniumDSL.g:81:5: lv_functions_0_0= ruleFunction
            	    {

            	    					newCompositeNode(grammarAccess.getSeleniumProgramAccess().getFunctionsFunctionParserRuleCall_0_0());
            	    				
            	    pushFollow(FOLLOW_3);
            	    lv_functions_0_0=ruleFunction();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getSeleniumProgramRule());
            	    					}
            	    					add(
            	    						current,
            	    						"functions",
            	    						lv_functions_0_0,
            	    						"org.xtext.selenium.SeleniumDSL.Function");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            // InternalSeleniumDSL.g:98:3: ( (lv_open_1_0= ruleOpen ) )
            // InternalSeleniumDSL.g:99:4: (lv_open_1_0= ruleOpen )
            {
            // InternalSeleniumDSL.g:99:4: (lv_open_1_0= ruleOpen )
            // InternalSeleniumDSL.g:100:5: lv_open_1_0= ruleOpen
            {

            					newCompositeNode(grammarAccess.getSeleniumProgramAccess().getOpenOpenParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_4);
            lv_open_1_0=ruleOpen();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSeleniumProgramRule());
            					}
            					set(
            						current,
            						"open",
            						lv_open_1_0,
            						"org.xtext.selenium.SeleniumDSL.Open");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalSeleniumDSL.g:117:3: ( (lv_seleniumInstruction_2_0= ruleSeleniumInstruction ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==RULE_ID||LA2_0==16||LA2_0==30||(LA2_0>=34 && LA2_0<=35)||(LA2_0>=37 && LA2_0<=38)||LA2_0==40||(LA2_0>=42 && LA2_0<=43)||LA2_0==48) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalSeleniumDSL.g:118:4: (lv_seleniumInstruction_2_0= ruleSeleniumInstruction )
            	    {
            	    // InternalSeleniumDSL.g:118:4: (lv_seleniumInstruction_2_0= ruleSeleniumInstruction )
            	    // InternalSeleniumDSL.g:119:5: lv_seleniumInstruction_2_0= ruleSeleniumInstruction
            	    {

            	    					newCompositeNode(grammarAccess.getSeleniumProgramAccess().getSeleniumInstructionSeleniumInstructionParserRuleCall_2_0());
            	    				
            	    pushFollow(FOLLOW_4);
            	    lv_seleniumInstruction_2_0=ruleSeleniumInstruction();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getSeleniumProgramRule());
            	    					}
            	    					add(
            	    						current,
            	    						"seleniumInstruction",
            	    						lv_seleniumInstruction_2_0,
            	    						"org.xtext.selenium.SeleniumDSL.SeleniumInstruction");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            // InternalSeleniumDSL.g:136:3: ( (lv_close_3_0= ruleClose ) )
            // InternalSeleniumDSL.g:137:4: (lv_close_3_0= ruleClose )
            {
            // InternalSeleniumDSL.g:137:4: (lv_close_3_0= ruleClose )
            // InternalSeleniumDSL.g:138:5: lv_close_3_0= ruleClose
            {

            					newCompositeNode(grammarAccess.getSeleniumProgramAccess().getCloseCloseParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_2);
            lv_close_3_0=ruleClose();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSeleniumProgramRule());
            					}
            					set(
            						current,
            						"close",
            						lv_close_3_0,
            						"org.xtext.selenium.SeleniumDSL.Close");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSeleniumProgram"


    // $ANTLR start "entryRuleBrowser"
    // InternalSeleniumDSL.g:159:1: entryRuleBrowser returns [String current=null] : iv_ruleBrowser= ruleBrowser EOF ;
    public final String entryRuleBrowser() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleBrowser = null;


        try {
            // InternalSeleniumDSL.g:159:47: (iv_ruleBrowser= ruleBrowser EOF )
            // InternalSeleniumDSL.g:160:2: iv_ruleBrowser= ruleBrowser EOF
            {
             newCompositeNode(grammarAccess.getBrowserRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBrowser=ruleBrowser();

            state._fsp--;

             current =iv_ruleBrowser.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBrowser"


    // $ANTLR start "ruleBrowser"
    // InternalSeleniumDSL.g:166:1: ruleBrowser returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'chrome' | kw= 'firefox' | kw= 'opera' ) ;
    public final AntlrDatatypeRuleToken ruleBrowser() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalSeleniumDSL.g:172:2: ( (kw= 'chrome' | kw= 'firefox' | kw= 'opera' ) )
            // InternalSeleniumDSL.g:173:2: (kw= 'chrome' | kw= 'firefox' | kw= 'opera' )
            {
            // InternalSeleniumDSL.g:173:2: (kw= 'chrome' | kw= 'firefox' | kw= 'opera' )
            int alt3=3;
            switch ( input.LA(1) ) {
            case 11:
                {
                alt3=1;
                }
                break;
            case 12:
                {
                alt3=2;
                }
                break;
            case 13:
                {
                alt3=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // InternalSeleniumDSL.g:174:3: kw= 'chrome'
                    {
                    kw=(Token)match(input,11,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getBrowserAccess().getChromeKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalSeleniumDSL.g:180:3: kw= 'firefox'
                    {
                    kw=(Token)match(input,12,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getBrowserAccess().getFirefoxKeyword_1());
                    		

                    }
                    break;
                case 3 :
                    // InternalSeleniumDSL.g:186:3: kw= 'opera'
                    {
                    kw=(Token)match(input,13,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getBrowserAccess().getOperaKeyword_2());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBrowser"


    // $ANTLR start "entryRuleOpen"
    // InternalSeleniumDSL.g:195:1: entryRuleOpen returns [EObject current=null] : iv_ruleOpen= ruleOpen EOF ;
    public final EObject entryRuleOpen() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOpen = null;


        try {
            // InternalSeleniumDSL.g:195:45: (iv_ruleOpen= ruleOpen EOF )
            // InternalSeleniumDSL.g:196:2: iv_ruleOpen= ruleOpen EOF
            {
             newCompositeNode(grammarAccess.getOpenRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOpen=ruleOpen();

            state._fsp--;

             current =iv_ruleOpen; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOpen"


    // $ANTLR start "ruleOpen"
    // InternalSeleniumDSL.g:202:1: ruleOpen returns [EObject current=null] : (otherlv_0= 'open' ( (lv_browser_1_0= ruleBrowser ) ) ) ;
    public final EObject ruleOpen() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        AntlrDatatypeRuleToken lv_browser_1_0 = null;



        	enterRule();

        try {
            // InternalSeleniumDSL.g:208:2: ( (otherlv_0= 'open' ( (lv_browser_1_0= ruleBrowser ) ) ) )
            // InternalSeleniumDSL.g:209:2: (otherlv_0= 'open' ( (lv_browser_1_0= ruleBrowser ) ) )
            {
            // InternalSeleniumDSL.g:209:2: (otherlv_0= 'open' ( (lv_browser_1_0= ruleBrowser ) ) )
            // InternalSeleniumDSL.g:210:3: otherlv_0= 'open' ( (lv_browser_1_0= ruleBrowser ) )
            {
            otherlv_0=(Token)match(input,14,FOLLOW_5); 

            			newLeafNode(otherlv_0, grammarAccess.getOpenAccess().getOpenKeyword_0());
            		
            // InternalSeleniumDSL.g:214:3: ( (lv_browser_1_0= ruleBrowser ) )
            // InternalSeleniumDSL.g:215:4: (lv_browser_1_0= ruleBrowser )
            {
            // InternalSeleniumDSL.g:215:4: (lv_browser_1_0= ruleBrowser )
            // InternalSeleniumDSL.g:216:5: lv_browser_1_0= ruleBrowser
            {

            					newCompositeNode(grammarAccess.getOpenAccess().getBrowserBrowserParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_browser_1_0=ruleBrowser();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getOpenRule());
            					}
            					set(
            						current,
            						"browser",
            						lv_browser_1_0,
            						"org.xtext.selenium.SeleniumDSL.Browser");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOpen"


    // $ANTLR start "entryRuleClose"
    // InternalSeleniumDSL.g:237:1: entryRuleClose returns [String current=null] : iv_ruleClose= ruleClose EOF ;
    public final String entryRuleClose() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleClose = null;


        try {
            // InternalSeleniumDSL.g:237:45: (iv_ruleClose= ruleClose EOF )
            // InternalSeleniumDSL.g:238:2: iv_ruleClose= ruleClose EOF
            {
             newCompositeNode(grammarAccess.getCloseRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleClose=ruleClose();

            state._fsp--;

             current =iv_ruleClose.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleClose"


    // $ANTLR start "ruleClose"
    // InternalSeleniumDSL.g:244:1: ruleClose returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : kw= 'close' ;
    public final AntlrDatatypeRuleToken ruleClose() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalSeleniumDSL.g:250:2: (kw= 'close' )
            // InternalSeleniumDSL.g:251:2: kw= 'close'
            {
            kw=(Token)match(input,15,FOLLOW_2); 

            		current.merge(kw);
            		newLeafNode(kw, grammarAccess.getCloseAccess().getCloseKeyword());
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleClose"


    // $ANTLR start "entryRuleSeleniumInstruction"
    // InternalSeleniumDSL.g:259:1: entryRuleSeleniumInstruction returns [EObject current=null] : iv_ruleSeleniumInstruction= ruleSeleniumInstruction EOF ;
    public final EObject entryRuleSeleniumInstruction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSeleniumInstruction = null;


        try {
            // InternalSeleniumDSL.g:259:60: (iv_ruleSeleniumInstruction= ruleSeleniumInstruction EOF )
            // InternalSeleniumDSL.g:260:2: iv_ruleSeleniumInstruction= ruleSeleniumInstruction EOF
            {
             newCompositeNode(grammarAccess.getSeleniumInstructionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSeleniumInstruction=ruleSeleniumInstruction();

            state._fsp--;

             current =iv_ruleSeleniumInstruction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSeleniumInstruction"


    // $ANTLR start "ruleSeleniumInstruction"
    // InternalSeleniumDSL.g:266:1: ruleSeleniumInstruction returns [EObject current=null] : (this_Get_0= ruleGet | this_Click_1= ruleClick | this_Store_2= ruleStore | this_IsElementPresent_3= ruleIsElementPresent | this_Set_4= ruleSet | this_FunctionCall_5= ruleFunctionCall | this_Count_6= ruleCount | this_Assert_7= ruleAssert | this_Select_8= ruleSelect | this_Check_9= ruleCheck ) ;
    public final EObject ruleSeleniumInstruction() throws RecognitionException {
        EObject current = null;

        EObject this_Get_0 = null;

        EObject this_Click_1 = null;

        EObject this_Store_2 = null;

        EObject this_IsElementPresent_3 = null;

        EObject this_Set_4 = null;

        EObject this_FunctionCall_5 = null;

        EObject this_Count_6 = null;

        EObject this_Assert_7 = null;

        EObject this_Select_8 = null;

        EObject this_Check_9 = null;



        	enterRule();

        try {
            // InternalSeleniumDSL.g:272:2: ( (this_Get_0= ruleGet | this_Click_1= ruleClick | this_Store_2= ruleStore | this_IsElementPresent_3= ruleIsElementPresent | this_Set_4= ruleSet | this_FunctionCall_5= ruleFunctionCall | this_Count_6= ruleCount | this_Assert_7= ruleAssert | this_Select_8= ruleSelect | this_Check_9= ruleCheck ) )
            // InternalSeleniumDSL.g:273:2: (this_Get_0= ruleGet | this_Click_1= ruleClick | this_Store_2= ruleStore | this_IsElementPresent_3= ruleIsElementPresent | this_Set_4= ruleSet | this_FunctionCall_5= ruleFunctionCall | this_Count_6= ruleCount | this_Assert_7= ruleAssert | this_Select_8= ruleSelect | this_Check_9= ruleCheck )
            {
            // InternalSeleniumDSL.g:273:2: (this_Get_0= ruleGet | this_Click_1= ruleClick | this_Store_2= ruleStore | this_IsElementPresent_3= ruleIsElementPresent | this_Set_4= ruleSet | this_FunctionCall_5= ruleFunctionCall | this_Count_6= ruleCount | this_Assert_7= ruleAssert | this_Select_8= ruleSelect | this_Check_9= ruleCheck )
            int alt4=10;
            switch ( input.LA(1) ) {
            case 16:
                {
                alt4=1;
                }
                break;
            case 37:
                {
                alt4=2;
                }
                break;
            case 38:
                {
                alt4=3;
                }
                break;
            case 42:
                {
                alt4=4;
                }
                break;
            case 40:
                {
                alt4=5;
                }
                break;
            case RULE_ID:
                {
                alt4=6;
                }
                break;
            case 43:
                {
                alt4=7;
                }
                break;
            case 48:
                {
                alt4=8;
                }
                break;
            case 30:
                {
                alt4=9;
                }
                break;
            case 34:
            case 35:
                {
                alt4=10;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // InternalSeleniumDSL.g:274:3: this_Get_0= ruleGet
                    {

                    			newCompositeNode(grammarAccess.getSeleniumInstructionAccess().getGetParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_Get_0=ruleGet();

                    state._fsp--;


                    			current = this_Get_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalSeleniumDSL.g:283:3: this_Click_1= ruleClick
                    {

                    			newCompositeNode(grammarAccess.getSeleniumInstructionAccess().getClickParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_Click_1=ruleClick();

                    state._fsp--;


                    			current = this_Click_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalSeleniumDSL.g:292:3: this_Store_2= ruleStore
                    {

                    			newCompositeNode(grammarAccess.getSeleniumInstructionAccess().getStoreParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_Store_2=ruleStore();

                    state._fsp--;


                    			current = this_Store_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalSeleniumDSL.g:301:3: this_IsElementPresent_3= ruleIsElementPresent
                    {

                    			newCompositeNode(grammarAccess.getSeleniumInstructionAccess().getIsElementPresentParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_IsElementPresent_3=ruleIsElementPresent();

                    state._fsp--;


                    			current = this_IsElementPresent_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 5 :
                    // InternalSeleniumDSL.g:310:3: this_Set_4= ruleSet
                    {

                    			newCompositeNode(grammarAccess.getSeleniumInstructionAccess().getSetParserRuleCall_4());
                    		
                    pushFollow(FOLLOW_2);
                    this_Set_4=ruleSet();

                    state._fsp--;


                    			current = this_Set_4;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 6 :
                    // InternalSeleniumDSL.g:319:3: this_FunctionCall_5= ruleFunctionCall
                    {

                    			newCompositeNode(grammarAccess.getSeleniumInstructionAccess().getFunctionCallParserRuleCall_5());
                    		
                    pushFollow(FOLLOW_2);
                    this_FunctionCall_5=ruleFunctionCall();

                    state._fsp--;


                    			current = this_FunctionCall_5;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 7 :
                    // InternalSeleniumDSL.g:328:3: this_Count_6= ruleCount
                    {

                    			newCompositeNode(grammarAccess.getSeleniumInstructionAccess().getCountParserRuleCall_6());
                    		
                    pushFollow(FOLLOW_2);
                    this_Count_6=ruleCount();

                    state._fsp--;


                    			current = this_Count_6;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 8 :
                    // InternalSeleniumDSL.g:337:3: this_Assert_7= ruleAssert
                    {

                    			newCompositeNode(grammarAccess.getSeleniumInstructionAccess().getAssertParserRuleCall_7());
                    		
                    pushFollow(FOLLOW_2);
                    this_Assert_7=ruleAssert();

                    state._fsp--;


                    			current = this_Assert_7;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 9 :
                    // InternalSeleniumDSL.g:346:3: this_Select_8= ruleSelect
                    {

                    			newCompositeNode(grammarAccess.getSeleniumInstructionAccess().getSelectParserRuleCall_8());
                    		
                    pushFollow(FOLLOW_2);
                    this_Select_8=ruleSelect();

                    state._fsp--;


                    			current = this_Select_8;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 10 :
                    // InternalSeleniumDSL.g:355:3: this_Check_9= ruleCheck
                    {

                    			newCompositeNode(grammarAccess.getSeleniumInstructionAccess().getCheckParserRuleCall_9());
                    		
                    pushFollow(FOLLOW_2);
                    this_Check_9=ruleCheck();

                    state._fsp--;


                    			current = this_Check_9;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSeleniumInstruction"


    // $ANTLR start "entryRuleValue"
    // InternalSeleniumDSL.g:367:1: entryRuleValue returns [EObject current=null] : iv_ruleValue= ruleValue EOF ;
    public final EObject entryRuleValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleValue = null;


        try {
            // InternalSeleniumDSL.g:367:46: (iv_ruleValue= ruleValue EOF )
            // InternalSeleniumDSL.g:368:2: iv_ruleValue= ruleValue EOF
            {
             newCompositeNode(grammarAccess.getValueRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleValue=ruleValue();

            state._fsp--;

             current =iv_ruleValue; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleValue"


    // $ANTLR start "ruleValue"
    // InternalSeleniumDSL.g:374:1: ruleValue returns [EObject current=null] : ( ( (lv_variableName_0_0= ruleVariableName ) ) | ( (lv_value_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleValue() throws RecognitionException {
        EObject current = null;

        Token lv_value_1_0=null;
        EObject lv_variableName_0_0 = null;



        	enterRule();

        try {
            // InternalSeleniumDSL.g:380:2: ( ( ( (lv_variableName_0_0= ruleVariableName ) ) | ( (lv_value_1_0= RULE_STRING ) ) ) )
            // InternalSeleniumDSL.g:381:2: ( ( (lv_variableName_0_0= ruleVariableName ) ) | ( (lv_value_1_0= RULE_STRING ) ) )
            {
            // InternalSeleniumDSL.g:381:2: ( ( (lv_variableName_0_0= ruleVariableName ) ) | ( (lv_value_1_0= RULE_STRING ) ) )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==RULE_ID) ) {
                alt5=1;
            }
            else if ( (LA5_0==RULE_STRING) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // InternalSeleniumDSL.g:382:3: ( (lv_variableName_0_0= ruleVariableName ) )
                    {
                    // InternalSeleniumDSL.g:382:3: ( (lv_variableName_0_0= ruleVariableName ) )
                    // InternalSeleniumDSL.g:383:4: (lv_variableName_0_0= ruleVariableName )
                    {
                    // InternalSeleniumDSL.g:383:4: (lv_variableName_0_0= ruleVariableName )
                    // InternalSeleniumDSL.g:384:5: lv_variableName_0_0= ruleVariableName
                    {

                    					newCompositeNode(grammarAccess.getValueAccess().getVariableNameVariableNameParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_variableName_0_0=ruleVariableName();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getValueRule());
                    					}
                    					set(
                    						current,
                    						"variableName",
                    						lv_variableName_0_0,
                    						"org.xtext.selenium.SeleniumDSL.VariableName");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalSeleniumDSL.g:402:3: ( (lv_value_1_0= RULE_STRING ) )
                    {
                    // InternalSeleniumDSL.g:402:3: ( (lv_value_1_0= RULE_STRING ) )
                    // InternalSeleniumDSL.g:403:4: (lv_value_1_0= RULE_STRING )
                    {
                    // InternalSeleniumDSL.g:403:4: (lv_value_1_0= RULE_STRING )
                    // InternalSeleniumDSL.g:404:5: lv_value_1_0= RULE_STRING
                    {
                    lv_value_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    					newLeafNode(lv_value_1_0, grammarAccess.getValueAccess().getValueSTRINGTerminalRuleCall_1_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getValueRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"value",
                    						lv_value_1_0,
                    						"org.eclipse.xtext.common.Terminals.STRING");
                    				

                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleValue"


    // $ANTLR start "entryRuleGet"
    // InternalSeleniumDSL.g:424:1: entryRuleGet returns [EObject current=null] : iv_ruleGet= ruleGet EOF ;
    public final EObject entryRuleGet() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGet = null;


        try {
            // InternalSeleniumDSL.g:424:44: (iv_ruleGet= ruleGet EOF )
            // InternalSeleniumDSL.g:425:2: iv_ruleGet= ruleGet EOF
            {
             newCompositeNode(grammarAccess.getGetRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleGet=ruleGet();

            state._fsp--;

             current =iv_ruleGet; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGet"


    // $ANTLR start "ruleGet"
    // InternalSeleniumDSL.g:431:1: ruleGet returns [EObject current=null] : (otherlv_0= 'get' ( (lv_url_1_0= ruleValue ) ) ) ;
    public final EObject ruleGet() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_url_1_0 = null;



        	enterRule();

        try {
            // InternalSeleniumDSL.g:437:2: ( (otherlv_0= 'get' ( (lv_url_1_0= ruleValue ) ) ) )
            // InternalSeleniumDSL.g:438:2: (otherlv_0= 'get' ( (lv_url_1_0= ruleValue ) ) )
            {
            // InternalSeleniumDSL.g:438:2: (otherlv_0= 'get' ( (lv_url_1_0= ruleValue ) ) )
            // InternalSeleniumDSL.g:439:3: otherlv_0= 'get' ( (lv_url_1_0= ruleValue ) )
            {
            otherlv_0=(Token)match(input,16,FOLLOW_6); 

            			newLeafNode(otherlv_0, grammarAccess.getGetAccess().getGetKeyword_0());
            		
            // InternalSeleniumDSL.g:443:3: ( (lv_url_1_0= ruleValue ) )
            // InternalSeleniumDSL.g:444:4: (lv_url_1_0= ruleValue )
            {
            // InternalSeleniumDSL.g:444:4: (lv_url_1_0= ruleValue )
            // InternalSeleniumDSL.g:445:5: lv_url_1_0= ruleValue
            {

            					newCompositeNode(grammarAccess.getGetAccess().getUrlValueParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_url_1_0=ruleValue();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getGetRule());
            					}
            					set(
            						current,
            						"url",
            						lv_url_1_0,
            						"org.xtext.selenium.SeleniumDSL.Value");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGet"


    // $ANTLR start "entryRuleTargetType"
    // InternalSeleniumDSL.g:466:1: entryRuleTargetType returns [EObject current=null] : iv_ruleTargetType= ruleTargetType EOF ;
    public final EObject entryRuleTargetType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTargetType = null;


        try {
            // InternalSeleniumDSL.g:466:51: (iv_ruleTargetType= ruleTargetType EOF )
            // InternalSeleniumDSL.g:467:2: iv_ruleTargetType= ruleTargetType EOF
            {
             newCompositeNode(grammarAccess.getTargetTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTargetType=ruleTargetType();

            state._fsp--;

             current =iv_ruleTargetType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTargetType"


    // $ANTLR start "ruleTargetType"
    // InternalSeleniumDSL.g:473:1: ruleTargetType returns [EObject current=null] : ( ( (lv_targetType_0_1= 'button' | lv_targetType_0_2= 'link' | lv_targetType_0_3= 'image' | lv_targetType_0_4= 'checkbox' | lv_targetType_0_5= 'input' | lv_targetType_0_6= 'div' | lv_targetType_0_7= 'span' | lv_targetType_0_8= 'label' ) ) ) ;
    public final EObject ruleTargetType() throws RecognitionException {
        EObject current = null;

        Token lv_targetType_0_1=null;
        Token lv_targetType_0_2=null;
        Token lv_targetType_0_3=null;
        Token lv_targetType_0_4=null;
        Token lv_targetType_0_5=null;
        Token lv_targetType_0_6=null;
        Token lv_targetType_0_7=null;
        Token lv_targetType_0_8=null;


        	enterRule();

        try {
            // InternalSeleniumDSL.g:479:2: ( ( ( (lv_targetType_0_1= 'button' | lv_targetType_0_2= 'link' | lv_targetType_0_3= 'image' | lv_targetType_0_4= 'checkbox' | lv_targetType_0_5= 'input' | lv_targetType_0_6= 'div' | lv_targetType_0_7= 'span' | lv_targetType_0_8= 'label' ) ) ) )
            // InternalSeleniumDSL.g:480:2: ( ( (lv_targetType_0_1= 'button' | lv_targetType_0_2= 'link' | lv_targetType_0_3= 'image' | lv_targetType_0_4= 'checkbox' | lv_targetType_0_5= 'input' | lv_targetType_0_6= 'div' | lv_targetType_0_7= 'span' | lv_targetType_0_8= 'label' ) ) )
            {
            // InternalSeleniumDSL.g:480:2: ( ( (lv_targetType_0_1= 'button' | lv_targetType_0_2= 'link' | lv_targetType_0_3= 'image' | lv_targetType_0_4= 'checkbox' | lv_targetType_0_5= 'input' | lv_targetType_0_6= 'div' | lv_targetType_0_7= 'span' | lv_targetType_0_8= 'label' ) ) )
            // InternalSeleniumDSL.g:481:3: ( (lv_targetType_0_1= 'button' | lv_targetType_0_2= 'link' | lv_targetType_0_3= 'image' | lv_targetType_0_4= 'checkbox' | lv_targetType_0_5= 'input' | lv_targetType_0_6= 'div' | lv_targetType_0_7= 'span' | lv_targetType_0_8= 'label' ) )
            {
            // InternalSeleniumDSL.g:481:3: ( (lv_targetType_0_1= 'button' | lv_targetType_0_2= 'link' | lv_targetType_0_3= 'image' | lv_targetType_0_4= 'checkbox' | lv_targetType_0_5= 'input' | lv_targetType_0_6= 'div' | lv_targetType_0_7= 'span' | lv_targetType_0_8= 'label' ) )
            // InternalSeleniumDSL.g:482:4: (lv_targetType_0_1= 'button' | lv_targetType_0_2= 'link' | lv_targetType_0_3= 'image' | lv_targetType_0_4= 'checkbox' | lv_targetType_0_5= 'input' | lv_targetType_0_6= 'div' | lv_targetType_0_7= 'span' | lv_targetType_0_8= 'label' )
            {
            // InternalSeleniumDSL.g:482:4: (lv_targetType_0_1= 'button' | lv_targetType_0_2= 'link' | lv_targetType_0_3= 'image' | lv_targetType_0_4= 'checkbox' | lv_targetType_0_5= 'input' | lv_targetType_0_6= 'div' | lv_targetType_0_7= 'span' | lv_targetType_0_8= 'label' )
            int alt6=8;
            switch ( input.LA(1) ) {
            case 17:
                {
                alt6=1;
                }
                break;
            case 18:
                {
                alt6=2;
                }
                break;
            case 19:
                {
                alt6=3;
                }
                break;
            case 20:
                {
                alt6=4;
                }
                break;
            case 21:
                {
                alt6=5;
                }
                break;
            case 22:
                {
                alt6=6;
                }
                break;
            case 23:
                {
                alt6=7;
                }
                break;
            case 24:
                {
                alt6=8;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // InternalSeleniumDSL.g:483:5: lv_targetType_0_1= 'button'
                    {
                    lv_targetType_0_1=(Token)match(input,17,FOLLOW_2); 

                    					newLeafNode(lv_targetType_0_1, grammarAccess.getTargetTypeAccess().getTargetTypeButtonKeyword_0_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getTargetTypeRule());
                    					}
                    					setWithLastConsumed(current, "targetType", lv_targetType_0_1, null);
                    				

                    }
                    break;
                case 2 :
                    // InternalSeleniumDSL.g:494:5: lv_targetType_0_2= 'link'
                    {
                    lv_targetType_0_2=(Token)match(input,18,FOLLOW_2); 

                    					newLeafNode(lv_targetType_0_2, grammarAccess.getTargetTypeAccess().getTargetTypeLinkKeyword_0_1());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getTargetTypeRule());
                    					}
                    					setWithLastConsumed(current, "targetType", lv_targetType_0_2, null);
                    				

                    }
                    break;
                case 3 :
                    // InternalSeleniumDSL.g:505:5: lv_targetType_0_3= 'image'
                    {
                    lv_targetType_0_3=(Token)match(input,19,FOLLOW_2); 

                    					newLeafNode(lv_targetType_0_3, grammarAccess.getTargetTypeAccess().getTargetTypeImageKeyword_0_2());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getTargetTypeRule());
                    					}
                    					setWithLastConsumed(current, "targetType", lv_targetType_0_3, null);
                    				

                    }
                    break;
                case 4 :
                    // InternalSeleniumDSL.g:516:5: lv_targetType_0_4= 'checkbox'
                    {
                    lv_targetType_0_4=(Token)match(input,20,FOLLOW_2); 

                    					newLeafNode(lv_targetType_0_4, grammarAccess.getTargetTypeAccess().getTargetTypeCheckboxKeyword_0_3());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getTargetTypeRule());
                    					}
                    					setWithLastConsumed(current, "targetType", lv_targetType_0_4, null);
                    				

                    }
                    break;
                case 5 :
                    // InternalSeleniumDSL.g:527:5: lv_targetType_0_5= 'input'
                    {
                    lv_targetType_0_5=(Token)match(input,21,FOLLOW_2); 

                    					newLeafNode(lv_targetType_0_5, grammarAccess.getTargetTypeAccess().getTargetTypeInputKeyword_0_4());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getTargetTypeRule());
                    					}
                    					setWithLastConsumed(current, "targetType", lv_targetType_0_5, null);
                    				

                    }
                    break;
                case 6 :
                    // InternalSeleniumDSL.g:538:5: lv_targetType_0_6= 'div'
                    {
                    lv_targetType_0_6=(Token)match(input,22,FOLLOW_2); 

                    					newLeafNode(lv_targetType_0_6, grammarAccess.getTargetTypeAccess().getTargetTypeDivKeyword_0_5());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getTargetTypeRule());
                    					}
                    					setWithLastConsumed(current, "targetType", lv_targetType_0_6, null);
                    				

                    }
                    break;
                case 7 :
                    // InternalSeleniumDSL.g:549:5: lv_targetType_0_7= 'span'
                    {
                    lv_targetType_0_7=(Token)match(input,23,FOLLOW_2); 

                    					newLeafNode(lv_targetType_0_7, grammarAccess.getTargetTypeAccess().getTargetTypeSpanKeyword_0_6());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getTargetTypeRule());
                    					}
                    					setWithLastConsumed(current, "targetType", lv_targetType_0_7, null);
                    				

                    }
                    break;
                case 8 :
                    // InternalSeleniumDSL.g:560:5: lv_targetType_0_8= 'label'
                    {
                    lv_targetType_0_8=(Token)match(input,24,FOLLOW_2); 

                    					newLeafNode(lv_targetType_0_8, grammarAccess.getTargetTypeAccess().getTargetTypeLabelKeyword_0_7());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getTargetTypeRule());
                    					}
                    					setWithLastConsumed(current, "targetType", lv_targetType_0_8, null);
                    				

                    }
                    break;

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTargetType"


    // $ANTLR start "entryRuleAttributeCheck"
    // InternalSeleniumDSL.g:576:1: entryRuleAttributeCheck returns [EObject current=null] : iv_ruleAttributeCheck= ruleAttributeCheck EOF ;
    public final EObject entryRuleAttributeCheck() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAttributeCheck = null;


        try {
            // InternalSeleniumDSL.g:576:55: (iv_ruleAttributeCheck= ruleAttributeCheck EOF )
            // InternalSeleniumDSL.g:577:2: iv_ruleAttributeCheck= ruleAttributeCheck EOF
            {
             newCompositeNode(grammarAccess.getAttributeCheckRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAttributeCheck=ruleAttributeCheck();

            state._fsp--;

             current =iv_ruleAttributeCheck; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAttributeCheck"


    // $ANTLR start "ruleAttributeCheck"
    // InternalSeleniumDSL.g:583:1: ruleAttributeCheck returns [EObject current=null] : ( ( (lv_attributeName_0_1= RULE_STRING | lv_attributeName_0_2= 'text' ) ) ) ;
    public final EObject ruleAttributeCheck() throws RecognitionException {
        EObject current = null;

        Token lv_attributeName_0_1=null;
        Token lv_attributeName_0_2=null;


        	enterRule();

        try {
            // InternalSeleniumDSL.g:589:2: ( ( ( (lv_attributeName_0_1= RULE_STRING | lv_attributeName_0_2= 'text' ) ) ) )
            // InternalSeleniumDSL.g:590:2: ( ( (lv_attributeName_0_1= RULE_STRING | lv_attributeName_0_2= 'text' ) ) )
            {
            // InternalSeleniumDSL.g:590:2: ( ( (lv_attributeName_0_1= RULE_STRING | lv_attributeName_0_2= 'text' ) ) )
            // InternalSeleniumDSL.g:591:3: ( (lv_attributeName_0_1= RULE_STRING | lv_attributeName_0_2= 'text' ) )
            {
            // InternalSeleniumDSL.g:591:3: ( (lv_attributeName_0_1= RULE_STRING | lv_attributeName_0_2= 'text' ) )
            // InternalSeleniumDSL.g:592:4: (lv_attributeName_0_1= RULE_STRING | lv_attributeName_0_2= 'text' )
            {
            // InternalSeleniumDSL.g:592:4: (lv_attributeName_0_1= RULE_STRING | lv_attributeName_0_2= 'text' )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==RULE_STRING) ) {
                alt7=1;
            }
            else if ( (LA7_0==25) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalSeleniumDSL.g:593:5: lv_attributeName_0_1= RULE_STRING
                    {
                    lv_attributeName_0_1=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    					newLeafNode(lv_attributeName_0_1, grammarAccess.getAttributeCheckAccess().getAttributeNameSTRINGTerminalRuleCall_0_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getAttributeCheckRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"attributeName",
                    						lv_attributeName_0_1,
                    						"org.eclipse.xtext.common.Terminals.STRING");
                    				

                    }
                    break;
                case 2 :
                    // InternalSeleniumDSL.g:608:5: lv_attributeName_0_2= 'text'
                    {
                    lv_attributeName_0_2=(Token)match(input,25,FOLLOW_2); 

                    					newLeafNode(lv_attributeName_0_2, grammarAccess.getAttributeCheckAccess().getAttributeNameTextKeyword_0_1());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getAttributeCheckRule());
                    					}
                    					setWithLastConsumed(current, "attributeName", lv_attributeName_0_2, null);
                    				

                    }
                    break;

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAttributeCheck"


    // $ANTLR start "entryRuleVariableName"
    // InternalSeleniumDSL.g:624:1: entryRuleVariableName returns [EObject current=null] : iv_ruleVariableName= ruleVariableName EOF ;
    public final EObject entryRuleVariableName() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariableName = null;


        try {
            // InternalSeleniumDSL.g:624:53: (iv_ruleVariableName= ruleVariableName EOF )
            // InternalSeleniumDSL.g:625:2: iv_ruleVariableName= ruleVariableName EOF
            {
             newCompositeNode(grammarAccess.getVariableNameRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleVariableName=ruleVariableName();

            state._fsp--;

             current =iv_ruleVariableName; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableName"


    // $ANTLR start "ruleVariableName"
    // InternalSeleniumDSL.g:631:1: ruleVariableName returns [EObject current=null] : ( (lv_name_0_0= RULE_ID ) ) ;
    public final EObject ruleVariableName() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;


        	enterRule();

        try {
            // InternalSeleniumDSL.g:637:2: ( ( (lv_name_0_0= RULE_ID ) ) )
            // InternalSeleniumDSL.g:638:2: ( (lv_name_0_0= RULE_ID ) )
            {
            // InternalSeleniumDSL.g:638:2: ( (lv_name_0_0= RULE_ID ) )
            // InternalSeleniumDSL.g:639:3: (lv_name_0_0= RULE_ID )
            {
            // InternalSeleniumDSL.g:639:3: (lv_name_0_0= RULE_ID )
            // InternalSeleniumDSL.g:640:4: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            				newLeafNode(lv_name_0_0, grammarAccess.getVariableNameAccess().getNameIDTerminalRuleCall_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getVariableNameRule());
            				}
            				setWithLastConsumed(
            					current,
            					"name",
            					lv_name_0_0,
            					"org.eclipse.xtext.common.Terminals.ID");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableName"


    // $ANTLR start "entryRuleCondition"
    // InternalSeleniumDSL.g:659:1: entryRuleCondition returns [EObject current=null] : iv_ruleCondition= ruleCondition EOF ;
    public final EObject entryRuleCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCondition = null;


        try {
            // InternalSeleniumDSL.g:659:50: (iv_ruleCondition= ruleCondition EOF )
            // InternalSeleniumDSL.g:660:2: iv_ruleCondition= ruleCondition EOF
            {
             newCompositeNode(grammarAccess.getConditionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCondition=ruleCondition();

            state._fsp--;

             current =iv_ruleCondition; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCondition"


    // $ANTLR start "ruleCondition"
    // InternalSeleniumDSL.g:666:1: ruleCondition returns [EObject current=null] : (otherlv_0= 'where' ( (lv_attributeCheck_1_0= ruleAttributeCheck ) ) ( ( (lv_operator_2_1= 'contains' | lv_operator_2_2= 'equals' ) ) ) ( (lv_value_3_0= ruleValue ) ) ) ;
    public final EObject ruleCondition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_operator_2_1=null;
        Token lv_operator_2_2=null;
        EObject lv_attributeCheck_1_0 = null;

        EObject lv_value_3_0 = null;



        	enterRule();

        try {
            // InternalSeleniumDSL.g:672:2: ( (otherlv_0= 'where' ( (lv_attributeCheck_1_0= ruleAttributeCheck ) ) ( ( (lv_operator_2_1= 'contains' | lv_operator_2_2= 'equals' ) ) ) ( (lv_value_3_0= ruleValue ) ) ) )
            // InternalSeleniumDSL.g:673:2: (otherlv_0= 'where' ( (lv_attributeCheck_1_0= ruleAttributeCheck ) ) ( ( (lv_operator_2_1= 'contains' | lv_operator_2_2= 'equals' ) ) ) ( (lv_value_3_0= ruleValue ) ) )
            {
            // InternalSeleniumDSL.g:673:2: (otherlv_0= 'where' ( (lv_attributeCheck_1_0= ruleAttributeCheck ) ) ( ( (lv_operator_2_1= 'contains' | lv_operator_2_2= 'equals' ) ) ) ( (lv_value_3_0= ruleValue ) ) )
            // InternalSeleniumDSL.g:674:3: otherlv_0= 'where' ( (lv_attributeCheck_1_0= ruleAttributeCheck ) ) ( ( (lv_operator_2_1= 'contains' | lv_operator_2_2= 'equals' ) ) ) ( (lv_value_3_0= ruleValue ) )
            {
            otherlv_0=(Token)match(input,26,FOLLOW_7); 

            			newLeafNode(otherlv_0, grammarAccess.getConditionAccess().getWhereKeyword_0());
            		
            // InternalSeleniumDSL.g:678:3: ( (lv_attributeCheck_1_0= ruleAttributeCheck ) )
            // InternalSeleniumDSL.g:679:4: (lv_attributeCheck_1_0= ruleAttributeCheck )
            {
            // InternalSeleniumDSL.g:679:4: (lv_attributeCheck_1_0= ruleAttributeCheck )
            // InternalSeleniumDSL.g:680:5: lv_attributeCheck_1_0= ruleAttributeCheck
            {

            					newCompositeNode(grammarAccess.getConditionAccess().getAttributeCheckAttributeCheckParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_8);
            lv_attributeCheck_1_0=ruleAttributeCheck();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConditionRule());
            					}
            					set(
            						current,
            						"attributeCheck",
            						lv_attributeCheck_1_0,
            						"org.xtext.selenium.SeleniumDSL.AttributeCheck");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalSeleniumDSL.g:697:3: ( ( (lv_operator_2_1= 'contains' | lv_operator_2_2= 'equals' ) ) )
            // InternalSeleniumDSL.g:698:4: ( (lv_operator_2_1= 'contains' | lv_operator_2_2= 'equals' ) )
            {
            // InternalSeleniumDSL.g:698:4: ( (lv_operator_2_1= 'contains' | lv_operator_2_2= 'equals' ) )
            // InternalSeleniumDSL.g:699:5: (lv_operator_2_1= 'contains' | lv_operator_2_2= 'equals' )
            {
            // InternalSeleniumDSL.g:699:5: (lv_operator_2_1= 'contains' | lv_operator_2_2= 'equals' )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==27) ) {
                alt8=1;
            }
            else if ( (LA8_0==28) ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // InternalSeleniumDSL.g:700:6: lv_operator_2_1= 'contains'
                    {
                    lv_operator_2_1=(Token)match(input,27,FOLLOW_6); 

                    						newLeafNode(lv_operator_2_1, grammarAccess.getConditionAccess().getOperatorContainsKeyword_2_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getConditionRule());
                    						}
                    						setWithLastConsumed(current, "operator", lv_operator_2_1, null);
                    					

                    }
                    break;
                case 2 :
                    // InternalSeleniumDSL.g:711:6: lv_operator_2_2= 'equals'
                    {
                    lv_operator_2_2=(Token)match(input,28,FOLLOW_6); 

                    						newLeafNode(lv_operator_2_2, grammarAccess.getConditionAccess().getOperatorEqualsKeyword_2_0_1());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getConditionRule());
                    						}
                    						setWithLastConsumed(current, "operator", lv_operator_2_2, null);
                    					

                    }
                    break;

            }


            }


            }

            // InternalSeleniumDSL.g:724:3: ( (lv_value_3_0= ruleValue ) )
            // InternalSeleniumDSL.g:725:4: (lv_value_3_0= ruleValue )
            {
            // InternalSeleniumDSL.g:725:4: (lv_value_3_0= ruleValue )
            // InternalSeleniumDSL.g:726:5: lv_value_3_0= ruleValue
            {

            					newCompositeNode(grammarAccess.getConditionAccess().getValueValueParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_2);
            lv_value_3_0=ruleValue();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConditionRule());
            					}
            					set(
            						current,
            						"value",
            						lv_value_3_0,
            						"org.xtext.selenium.SeleniumDSL.Value");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCondition"


    // $ANTLR start "entryRuleTypeComboSelection"
    // InternalSeleniumDSL.g:747:1: entryRuleTypeComboSelection returns [String current=null] : iv_ruleTypeComboSelection= ruleTypeComboSelection EOF ;
    public final String entryRuleTypeComboSelection() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleTypeComboSelection = null;


        try {
            // InternalSeleniumDSL.g:747:58: (iv_ruleTypeComboSelection= ruleTypeComboSelection EOF )
            // InternalSeleniumDSL.g:748:2: iv_ruleTypeComboSelection= ruleTypeComboSelection EOF
            {
             newCompositeNode(grammarAccess.getTypeComboSelectionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTypeComboSelection=ruleTypeComboSelection();

            state._fsp--;

             current =iv_ruleTypeComboSelection.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypeComboSelection"


    // $ANTLR start "ruleTypeComboSelection"
    // InternalSeleniumDSL.g:754:1: ruleTypeComboSelection returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'text' | kw= 'value' ) ;
    public final AntlrDatatypeRuleToken ruleTypeComboSelection() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalSeleniumDSL.g:760:2: ( (kw= 'text' | kw= 'value' ) )
            // InternalSeleniumDSL.g:761:2: (kw= 'text' | kw= 'value' )
            {
            // InternalSeleniumDSL.g:761:2: (kw= 'text' | kw= 'value' )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==25) ) {
                alt9=1;
            }
            else if ( (LA9_0==29) ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // InternalSeleniumDSL.g:762:3: kw= 'text'
                    {
                    kw=(Token)match(input,25,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getTypeComboSelectionAccess().getTextKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalSeleniumDSL.g:768:3: kw= 'value'
                    {
                    kw=(Token)match(input,29,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getTypeComboSelectionAccess().getValueKeyword_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypeComboSelection"


    // $ANTLR start "entryRuleSelect"
    // InternalSeleniumDSL.g:777:1: entryRuleSelect returns [EObject current=null] : iv_ruleSelect= ruleSelect EOF ;
    public final EObject entryRuleSelect() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSelect = null;


        try {
            // InternalSeleniumDSL.g:777:47: (iv_ruleSelect= ruleSelect EOF )
            // InternalSeleniumDSL.g:778:2: iv_ruleSelect= ruleSelect EOF
            {
             newCompositeNode(grammarAccess.getSelectRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSelect=ruleSelect();

            state._fsp--;

             current =iv_ruleSelect; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSelect"


    // $ANTLR start "ruleSelect"
    // InternalSeleniumDSL.g:784:1: ruleSelect returns [EObject current=null] : (otherlv_0= 'select' otherlv_1= 'option' ( (lv_typeComboSelection_2_0= ruleTypeComboSelection ) ) ( (lv_value_3_0= ruleValue ) ) otherlv_4= 'in' otherlv_5= 'combo' ( (lv_condition_6_0= ruleCondition ) ) ) ;
    public final EObject ruleSelect() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        AntlrDatatypeRuleToken lv_typeComboSelection_2_0 = null;

        EObject lv_value_3_0 = null;

        EObject lv_condition_6_0 = null;



        	enterRule();

        try {
            // InternalSeleniumDSL.g:790:2: ( (otherlv_0= 'select' otherlv_1= 'option' ( (lv_typeComboSelection_2_0= ruleTypeComboSelection ) ) ( (lv_value_3_0= ruleValue ) ) otherlv_4= 'in' otherlv_5= 'combo' ( (lv_condition_6_0= ruleCondition ) ) ) )
            // InternalSeleniumDSL.g:791:2: (otherlv_0= 'select' otherlv_1= 'option' ( (lv_typeComboSelection_2_0= ruleTypeComboSelection ) ) ( (lv_value_3_0= ruleValue ) ) otherlv_4= 'in' otherlv_5= 'combo' ( (lv_condition_6_0= ruleCondition ) ) )
            {
            // InternalSeleniumDSL.g:791:2: (otherlv_0= 'select' otherlv_1= 'option' ( (lv_typeComboSelection_2_0= ruleTypeComboSelection ) ) ( (lv_value_3_0= ruleValue ) ) otherlv_4= 'in' otherlv_5= 'combo' ( (lv_condition_6_0= ruleCondition ) ) )
            // InternalSeleniumDSL.g:792:3: otherlv_0= 'select' otherlv_1= 'option' ( (lv_typeComboSelection_2_0= ruleTypeComboSelection ) ) ( (lv_value_3_0= ruleValue ) ) otherlv_4= 'in' otherlv_5= 'combo' ( (lv_condition_6_0= ruleCondition ) )
            {
            otherlv_0=(Token)match(input,30,FOLLOW_9); 

            			newLeafNode(otherlv_0, grammarAccess.getSelectAccess().getSelectKeyword_0());
            		
            otherlv_1=(Token)match(input,31,FOLLOW_10); 

            			newLeafNode(otherlv_1, grammarAccess.getSelectAccess().getOptionKeyword_1());
            		
            // InternalSeleniumDSL.g:800:3: ( (lv_typeComboSelection_2_0= ruleTypeComboSelection ) )
            // InternalSeleniumDSL.g:801:4: (lv_typeComboSelection_2_0= ruleTypeComboSelection )
            {
            // InternalSeleniumDSL.g:801:4: (lv_typeComboSelection_2_0= ruleTypeComboSelection )
            // InternalSeleniumDSL.g:802:5: lv_typeComboSelection_2_0= ruleTypeComboSelection
            {

            					newCompositeNode(grammarAccess.getSelectAccess().getTypeComboSelectionTypeComboSelectionParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_6);
            lv_typeComboSelection_2_0=ruleTypeComboSelection();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSelectRule());
            					}
            					set(
            						current,
            						"typeComboSelection",
            						lv_typeComboSelection_2_0,
            						"org.xtext.selenium.SeleniumDSL.TypeComboSelection");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalSeleniumDSL.g:819:3: ( (lv_value_3_0= ruleValue ) )
            // InternalSeleniumDSL.g:820:4: (lv_value_3_0= ruleValue )
            {
            // InternalSeleniumDSL.g:820:4: (lv_value_3_0= ruleValue )
            // InternalSeleniumDSL.g:821:5: lv_value_3_0= ruleValue
            {

            					newCompositeNode(grammarAccess.getSelectAccess().getValueValueParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_11);
            lv_value_3_0=ruleValue();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSelectRule());
            					}
            					set(
            						current,
            						"value",
            						lv_value_3_0,
            						"org.xtext.selenium.SeleniumDSL.Value");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_4=(Token)match(input,32,FOLLOW_12); 

            			newLeafNode(otherlv_4, grammarAccess.getSelectAccess().getInKeyword_4());
            		
            otherlv_5=(Token)match(input,33,FOLLOW_13); 

            			newLeafNode(otherlv_5, grammarAccess.getSelectAccess().getComboKeyword_5());
            		
            // InternalSeleniumDSL.g:846:3: ( (lv_condition_6_0= ruleCondition ) )
            // InternalSeleniumDSL.g:847:4: (lv_condition_6_0= ruleCondition )
            {
            // InternalSeleniumDSL.g:847:4: (lv_condition_6_0= ruleCondition )
            // InternalSeleniumDSL.g:848:5: lv_condition_6_0= ruleCondition
            {

            					newCompositeNode(grammarAccess.getSelectAccess().getConditionConditionParserRuleCall_6_0());
            				
            pushFollow(FOLLOW_2);
            lv_condition_6_0=ruleCondition();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSelectRule());
            					}
            					set(
            						current,
            						"condition",
            						lv_condition_6_0,
            						"org.xtext.selenium.SeleniumDSL.Condition");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSelect"


    // $ANTLR start "entryRuleCheck"
    // InternalSeleniumDSL.g:869:1: entryRuleCheck returns [EObject current=null] : iv_ruleCheck= ruleCheck EOF ;
    public final EObject entryRuleCheck() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCheck = null;


        try {
            // InternalSeleniumDSL.g:869:46: (iv_ruleCheck= ruleCheck EOF )
            // InternalSeleniumDSL.g:870:2: iv_ruleCheck= ruleCheck EOF
            {
             newCompositeNode(grammarAccess.getCheckRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCheck=ruleCheck();

            state._fsp--;

             current =iv_ruleCheck; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCheck"


    // $ANTLR start "ruleCheck"
    // InternalSeleniumDSL.g:876:1: ruleCheck returns [EObject current=null] : ( ( ( (lv_toCheck_0_1= 'check' | lv_toCheck_0_2= 'uncheck' ) ) ) ( (lv_all_1_0= 'all' ) )? ( (lv_targetType_2_0= ruleTargetType ) ) ( (lv_condition_3_0= ruleCondition ) )? ) ;
    public final EObject ruleCheck() throws RecognitionException {
        EObject current = null;

        Token lv_toCheck_0_1=null;
        Token lv_toCheck_0_2=null;
        Token lv_all_1_0=null;
        EObject lv_targetType_2_0 = null;

        EObject lv_condition_3_0 = null;



        	enterRule();

        try {
            // InternalSeleniumDSL.g:882:2: ( ( ( ( (lv_toCheck_0_1= 'check' | lv_toCheck_0_2= 'uncheck' ) ) ) ( (lv_all_1_0= 'all' ) )? ( (lv_targetType_2_0= ruleTargetType ) ) ( (lv_condition_3_0= ruleCondition ) )? ) )
            // InternalSeleniumDSL.g:883:2: ( ( ( (lv_toCheck_0_1= 'check' | lv_toCheck_0_2= 'uncheck' ) ) ) ( (lv_all_1_0= 'all' ) )? ( (lv_targetType_2_0= ruleTargetType ) ) ( (lv_condition_3_0= ruleCondition ) )? )
            {
            // InternalSeleniumDSL.g:883:2: ( ( ( (lv_toCheck_0_1= 'check' | lv_toCheck_0_2= 'uncheck' ) ) ) ( (lv_all_1_0= 'all' ) )? ( (lv_targetType_2_0= ruleTargetType ) ) ( (lv_condition_3_0= ruleCondition ) )? )
            // InternalSeleniumDSL.g:884:3: ( ( (lv_toCheck_0_1= 'check' | lv_toCheck_0_2= 'uncheck' ) ) ) ( (lv_all_1_0= 'all' ) )? ( (lv_targetType_2_0= ruleTargetType ) ) ( (lv_condition_3_0= ruleCondition ) )?
            {
            // InternalSeleniumDSL.g:884:3: ( ( (lv_toCheck_0_1= 'check' | lv_toCheck_0_2= 'uncheck' ) ) )
            // InternalSeleniumDSL.g:885:4: ( (lv_toCheck_0_1= 'check' | lv_toCheck_0_2= 'uncheck' ) )
            {
            // InternalSeleniumDSL.g:885:4: ( (lv_toCheck_0_1= 'check' | lv_toCheck_0_2= 'uncheck' ) )
            // InternalSeleniumDSL.g:886:5: (lv_toCheck_0_1= 'check' | lv_toCheck_0_2= 'uncheck' )
            {
            // InternalSeleniumDSL.g:886:5: (lv_toCheck_0_1= 'check' | lv_toCheck_0_2= 'uncheck' )
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==34) ) {
                alt10=1;
            }
            else if ( (LA10_0==35) ) {
                alt10=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }
            switch (alt10) {
                case 1 :
                    // InternalSeleniumDSL.g:887:6: lv_toCheck_0_1= 'check'
                    {
                    lv_toCheck_0_1=(Token)match(input,34,FOLLOW_14); 

                    						newLeafNode(lv_toCheck_0_1, grammarAccess.getCheckAccess().getToCheckCheckKeyword_0_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getCheckRule());
                    						}
                    						setWithLastConsumed(current, "toCheck", lv_toCheck_0_1, null);
                    					

                    }
                    break;
                case 2 :
                    // InternalSeleniumDSL.g:898:6: lv_toCheck_0_2= 'uncheck'
                    {
                    lv_toCheck_0_2=(Token)match(input,35,FOLLOW_14); 

                    						newLeafNode(lv_toCheck_0_2, grammarAccess.getCheckAccess().getToCheckUncheckKeyword_0_0_1());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getCheckRule());
                    						}
                    						setWithLastConsumed(current, "toCheck", lv_toCheck_0_2, null);
                    					

                    }
                    break;

            }


            }


            }

            // InternalSeleniumDSL.g:911:3: ( (lv_all_1_0= 'all' ) )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==36) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalSeleniumDSL.g:912:4: (lv_all_1_0= 'all' )
                    {
                    // InternalSeleniumDSL.g:912:4: (lv_all_1_0= 'all' )
                    // InternalSeleniumDSL.g:913:5: lv_all_1_0= 'all'
                    {
                    lv_all_1_0=(Token)match(input,36,FOLLOW_14); 

                    					newLeafNode(lv_all_1_0, grammarAccess.getCheckAccess().getAllAllKeyword_1_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getCheckRule());
                    					}
                    					setWithLastConsumed(current, "all", lv_all_1_0, "all");
                    				

                    }


                    }
                    break;

            }

            // InternalSeleniumDSL.g:925:3: ( (lv_targetType_2_0= ruleTargetType ) )
            // InternalSeleniumDSL.g:926:4: (lv_targetType_2_0= ruleTargetType )
            {
            // InternalSeleniumDSL.g:926:4: (lv_targetType_2_0= ruleTargetType )
            // InternalSeleniumDSL.g:927:5: lv_targetType_2_0= ruleTargetType
            {

            					newCompositeNode(grammarAccess.getCheckAccess().getTargetTypeTargetTypeParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_15);
            lv_targetType_2_0=ruleTargetType();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCheckRule());
            					}
            					set(
            						current,
            						"targetType",
            						lv_targetType_2_0,
            						"org.xtext.selenium.SeleniumDSL.TargetType");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalSeleniumDSL.g:944:3: ( (lv_condition_3_0= ruleCondition ) )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==26) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalSeleniumDSL.g:945:4: (lv_condition_3_0= ruleCondition )
                    {
                    // InternalSeleniumDSL.g:945:4: (lv_condition_3_0= ruleCondition )
                    // InternalSeleniumDSL.g:946:5: lv_condition_3_0= ruleCondition
                    {

                    					newCompositeNode(grammarAccess.getCheckAccess().getConditionConditionParserRuleCall_3_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_condition_3_0=ruleCondition();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getCheckRule());
                    					}
                    					set(
                    						current,
                    						"condition",
                    						lv_condition_3_0,
                    						"org.xtext.selenium.SeleniumDSL.Condition");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCheck"


    // $ANTLR start "entryRuleClick"
    // InternalSeleniumDSL.g:967:1: entryRuleClick returns [EObject current=null] : iv_ruleClick= ruleClick EOF ;
    public final EObject entryRuleClick() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleClick = null;


        try {
            // InternalSeleniumDSL.g:967:46: (iv_ruleClick= ruleClick EOF )
            // InternalSeleniumDSL.g:968:2: iv_ruleClick= ruleClick EOF
            {
             newCompositeNode(grammarAccess.getClickRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleClick=ruleClick();

            state._fsp--;

             current =iv_ruleClick; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleClick"


    // $ANTLR start "ruleClick"
    // InternalSeleniumDSL.g:974:1: ruleClick returns [EObject current=null] : (otherlv_0= 'click' ( (lv_all_1_0= 'all' ) )? ( (lv_targetType_2_0= ruleTargetType ) ) ( (lv_condition_3_0= ruleCondition ) ) ) ;
    public final EObject ruleClick() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_all_1_0=null;
        EObject lv_targetType_2_0 = null;

        EObject lv_condition_3_0 = null;



        	enterRule();

        try {
            // InternalSeleniumDSL.g:980:2: ( (otherlv_0= 'click' ( (lv_all_1_0= 'all' ) )? ( (lv_targetType_2_0= ruleTargetType ) ) ( (lv_condition_3_0= ruleCondition ) ) ) )
            // InternalSeleniumDSL.g:981:2: (otherlv_0= 'click' ( (lv_all_1_0= 'all' ) )? ( (lv_targetType_2_0= ruleTargetType ) ) ( (lv_condition_3_0= ruleCondition ) ) )
            {
            // InternalSeleniumDSL.g:981:2: (otherlv_0= 'click' ( (lv_all_1_0= 'all' ) )? ( (lv_targetType_2_0= ruleTargetType ) ) ( (lv_condition_3_0= ruleCondition ) ) )
            // InternalSeleniumDSL.g:982:3: otherlv_0= 'click' ( (lv_all_1_0= 'all' ) )? ( (lv_targetType_2_0= ruleTargetType ) ) ( (lv_condition_3_0= ruleCondition ) )
            {
            otherlv_0=(Token)match(input,37,FOLLOW_14); 

            			newLeafNode(otherlv_0, grammarAccess.getClickAccess().getClickKeyword_0());
            		
            // InternalSeleniumDSL.g:986:3: ( (lv_all_1_0= 'all' ) )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==36) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalSeleniumDSL.g:987:4: (lv_all_1_0= 'all' )
                    {
                    // InternalSeleniumDSL.g:987:4: (lv_all_1_0= 'all' )
                    // InternalSeleniumDSL.g:988:5: lv_all_1_0= 'all'
                    {
                    lv_all_1_0=(Token)match(input,36,FOLLOW_14); 

                    					newLeafNode(lv_all_1_0, grammarAccess.getClickAccess().getAllAllKeyword_1_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getClickRule());
                    					}
                    					setWithLastConsumed(current, "all", lv_all_1_0, "all");
                    				

                    }


                    }
                    break;

            }

            // InternalSeleniumDSL.g:1000:3: ( (lv_targetType_2_0= ruleTargetType ) )
            // InternalSeleniumDSL.g:1001:4: (lv_targetType_2_0= ruleTargetType )
            {
            // InternalSeleniumDSL.g:1001:4: (lv_targetType_2_0= ruleTargetType )
            // InternalSeleniumDSL.g:1002:5: lv_targetType_2_0= ruleTargetType
            {

            					newCompositeNode(grammarAccess.getClickAccess().getTargetTypeTargetTypeParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_13);
            lv_targetType_2_0=ruleTargetType();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getClickRule());
            					}
            					set(
            						current,
            						"targetType",
            						lv_targetType_2_0,
            						"org.xtext.selenium.SeleniumDSL.TargetType");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalSeleniumDSL.g:1019:3: ( (lv_condition_3_0= ruleCondition ) )
            // InternalSeleniumDSL.g:1020:4: (lv_condition_3_0= ruleCondition )
            {
            // InternalSeleniumDSL.g:1020:4: (lv_condition_3_0= ruleCondition )
            // InternalSeleniumDSL.g:1021:5: lv_condition_3_0= ruleCondition
            {

            					newCompositeNode(grammarAccess.getClickAccess().getConditionConditionParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_2);
            lv_condition_3_0=ruleCondition();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getClickRule());
            					}
            					set(
            						current,
            						"condition",
            						lv_condition_3_0,
            						"org.xtext.selenium.SeleniumDSL.Condition");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleClick"


    // $ANTLR start "entryRuleStore"
    // InternalSeleniumDSL.g:1042:1: entryRuleStore returns [EObject current=null] : iv_ruleStore= ruleStore EOF ;
    public final EObject entryRuleStore() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStore = null;


        try {
            // InternalSeleniumDSL.g:1042:46: (iv_ruleStore= ruleStore EOF )
            // InternalSeleniumDSL.g:1043:2: iv_ruleStore= ruleStore EOF
            {
             newCompositeNode(grammarAccess.getStoreRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStore=ruleStore();

            state._fsp--;

             current =iv_ruleStore; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStore"


    // $ANTLR start "ruleStore"
    // InternalSeleniumDSL.g:1049:1: ruleStore returns [EObject current=null] : (otherlv_0= 'store' ( ( ( (lv_attributeCheck_1_0= ruleAttributeCheck ) ) otherlv_2= 'of' ( (lv_targetType_3_0= ruleTargetType ) ) ( (lv_condition_4_0= ruleCondition ) ) ) | ( (lv_value_5_0= ruleValue ) ) ) otherlv_6= 'in' ( (lv_variableName_7_0= ruleVariableName ) ) ) ;
    public final EObject ruleStore() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_6=null;
        EObject lv_attributeCheck_1_0 = null;

        EObject lv_targetType_3_0 = null;

        EObject lv_condition_4_0 = null;

        EObject lv_value_5_0 = null;

        EObject lv_variableName_7_0 = null;



        	enterRule();

        try {
            // InternalSeleniumDSL.g:1055:2: ( (otherlv_0= 'store' ( ( ( (lv_attributeCheck_1_0= ruleAttributeCheck ) ) otherlv_2= 'of' ( (lv_targetType_3_0= ruleTargetType ) ) ( (lv_condition_4_0= ruleCondition ) ) ) | ( (lv_value_5_0= ruleValue ) ) ) otherlv_6= 'in' ( (lv_variableName_7_0= ruleVariableName ) ) ) )
            // InternalSeleniumDSL.g:1056:2: (otherlv_0= 'store' ( ( ( (lv_attributeCheck_1_0= ruleAttributeCheck ) ) otherlv_2= 'of' ( (lv_targetType_3_0= ruleTargetType ) ) ( (lv_condition_4_0= ruleCondition ) ) ) | ( (lv_value_5_0= ruleValue ) ) ) otherlv_6= 'in' ( (lv_variableName_7_0= ruleVariableName ) ) )
            {
            // InternalSeleniumDSL.g:1056:2: (otherlv_0= 'store' ( ( ( (lv_attributeCheck_1_0= ruleAttributeCheck ) ) otherlv_2= 'of' ( (lv_targetType_3_0= ruleTargetType ) ) ( (lv_condition_4_0= ruleCondition ) ) ) | ( (lv_value_5_0= ruleValue ) ) ) otherlv_6= 'in' ( (lv_variableName_7_0= ruleVariableName ) ) )
            // InternalSeleniumDSL.g:1057:3: otherlv_0= 'store' ( ( ( (lv_attributeCheck_1_0= ruleAttributeCheck ) ) otherlv_2= 'of' ( (lv_targetType_3_0= ruleTargetType ) ) ( (lv_condition_4_0= ruleCondition ) ) ) | ( (lv_value_5_0= ruleValue ) ) ) otherlv_6= 'in' ( (lv_variableName_7_0= ruleVariableName ) )
            {
            otherlv_0=(Token)match(input,38,FOLLOW_16); 

            			newLeafNode(otherlv_0, grammarAccess.getStoreAccess().getStoreKeyword_0());
            		
            // InternalSeleniumDSL.g:1061:3: ( ( ( (lv_attributeCheck_1_0= ruleAttributeCheck ) ) otherlv_2= 'of' ( (lv_targetType_3_0= ruleTargetType ) ) ( (lv_condition_4_0= ruleCondition ) ) ) | ( (lv_value_5_0= ruleValue ) ) )
            int alt14=2;
            switch ( input.LA(1) ) {
            case RULE_STRING:
                {
                int LA14_1 = input.LA(2);

                if ( (LA14_1==32) ) {
                    alt14=2;
                }
                else if ( (LA14_1==39) ) {
                    alt14=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 14, 1, input);

                    throw nvae;
                }
                }
                break;
            case 25:
                {
                alt14=1;
                }
                break;
            case RULE_ID:
                {
                alt14=2;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;
            }

            switch (alt14) {
                case 1 :
                    // InternalSeleniumDSL.g:1062:4: ( ( (lv_attributeCheck_1_0= ruleAttributeCheck ) ) otherlv_2= 'of' ( (lv_targetType_3_0= ruleTargetType ) ) ( (lv_condition_4_0= ruleCondition ) ) )
                    {
                    // InternalSeleniumDSL.g:1062:4: ( ( (lv_attributeCheck_1_0= ruleAttributeCheck ) ) otherlv_2= 'of' ( (lv_targetType_3_0= ruleTargetType ) ) ( (lv_condition_4_0= ruleCondition ) ) )
                    // InternalSeleniumDSL.g:1063:5: ( (lv_attributeCheck_1_0= ruleAttributeCheck ) ) otherlv_2= 'of' ( (lv_targetType_3_0= ruleTargetType ) ) ( (lv_condition_4_0= ruleCondition ) )
                    {
                    // InternalSeleniumDSL.g:1063:5: ( (lv_attributeCheck_1_0= ruleAttributeCheck ) )
                    // InternalSeleniumDSL.g:1064:6: (lv_attributeCheck_1_0= ruleAttributeCheck )
                    {
                    // InternalSeleniumDSL.g:1064:6: (lv_attributeCheck_1_0= ruleAttributeCheck )
                    // InternalSeleniumDSL.g:1065:7: lv_attributeCheck_1_0= ruleAttributeCheck
                    {

                    							newCompositeNode(grammarAccess.getStoreAccess().getAttributeCheckAttributeCheckParserRuleCall_1_0_0_0());
                    						
                    pushFollow(FOLLOW_17);
                    lv_attributeCheck_1_0=ruleAttributeCheck();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getStoreRule());
                    							}
                    							set(
                    								current,
                    								"attributeCheck",
                    								lv_attributeCheck_1_0,
                    								"org.xtext.selenium.SeleniumDSL.AttributeCheck");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }

                    otherlv_2=(Token)match(input,39,FOLLOW_14); 

                    					newLeafNode(otherlv_2, grammarAccess.getStoreAccess().getOfKeyword_1_0_1());
                    				
                    // InternalSeleniumDSL.g:1086:5: ( (lv_targetType_3_0= ruleTargetType ) )
                    // InternalSeleniumDSL.g:1087:6: (lv_targetType_3_0= ruleTargetType )
                    {
                    // InternalSeleniumDSL.g:1087:6: (lv_targetType_3_0= ruleTargetType )
                    // InternalSeleniumDSL.g:1088:7: lv_targetType_3_0= ruleTargetType
                    {

                    							newCompositeNode(grammarAccess.getStoreAccess().getTargetTypeTargetTypeParserRuleCall_1_0_2_0());
                    						
                    pushFollow(FOLLOW_13);
                    lv_targetType_3_0=ruleTargetType();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getStoreRule());
                    							}
                    							set(
                    								current,
                    								"targetType",
                    								lv_targetType_3_0,
                    								"org.xtext.selenium.SeleniumDSL.TargetType");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }

                    // InternalSeleniumDSL.g:1105:5: ( (lv_condition_4_0= ruleCondition ) )
                    // InternalSeleniumDSL.g:1106:6: (lv_condition_4_0= ruleCondition )
                    {
                    // InternalSeleniumDSL.g:1106:6: (lv_condition_4_0= ruleCondition )
                    // InternalSeleniumDSL.g:1107:7: lv_condition_4_0= ruleCondition
                    {

                    							newCompositeNode(grammarAccess.getStoreAccess().getConditionConditionParserRuleCall_1_0_3_0());
                    						
                    pushFollow(FOLLOW_11);
                    lv_condition_4_0=ruleCondition();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getStoreRule());
                    							}
                    							set(
                    								current,
                    								"condition",
                    								lv_condition_4_0,
                    								"org.xtext.selenium.SeleniumDSL.Condition");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalSeleniumDSL.g:1126:4: ( (lv_value_5_0= ruleValue ) )
                    {
                    // InternalSeleniumDSL.g:1126:4: ( (lv_value_5_0= ruleValue ) )
                    // InternalSeleniumDSL.g:1127:5: (lv_value_5_0= ruleValue )
                    {
                    // InternalSeleniumDSL.g:1127:5: (lv_value_5_0= ruleValue )
                    // InternalSeleniumDSL.g:1128:6: lv_value_5_0= ruleValue
                    {

                    						newCompositeNode(grammarAccess.getStoreAccess().getValueValueParserRuleCall_1_1_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_value_5_0=ruleValue();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getStoreRule());
                    						}
                    						set(
                    							current,
                    							"value",
                    							lv_value_5_0,
                    							"org.xtext.selenium.SeleniumDSL.Value");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_6=(Token)match(input,32,FOLLOW_18); 

            			newLeafNode(otherlv_6, grammarAccess.getStoreAccess().getInKeyword_2());
            		
            // InternalSeleniumDSL.g:1150:3: ( (lv_variableName_7_0= ruleVariableName ) )
            // InternalSeleniumDSL.g:1151:4: (lv_variableName_7_0= ruleVariableName )
            {
            // InternalSeleniumDSL.g:1151:4: (lv_variableName_7_0= ruleVariableName )
            // InternalSeleniumDSL.g:1152:5: lv_variableName_7_0= ruleVariableName
            {

            					newCompositeNode(grammarAccess.getStoreAccess().getVariableNameVariableNameParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_2);
            lv_variableName_7_0=ruleVariableName();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getStoreRule());
            					}
            					set(
            						current,
            						"variableName",
            						lv_variableName_7_0,
            						"org.xtext.selenium.SeleniumDSL.VariableName");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStore"


    // $ANTLR start "entryRuleSet"
    // InternalSeleniumDSL.g:1173:1: entryRuleSet returns [EObject current=null] : iv_ruleSet= ruleSet EOF ;
    public final EObject entryRuleSet() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSet = null;


        try {
            // InternalSeleniumDSL.g:1173:44: (iv_ruleSet= ruleSet EOF )
            // InternalSeleniumDSL.g:1174:2: iv_ruleSet= ruleSet EOF
            {
             newCompositeNode(grammarAccess.getSetRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSet=ruleSet();

            state._fsp--;

             current =iv_ruleSet; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSet"


    // $ANTLR start "ruleSet"
    // InternalSeleniumDSL.g:1180:1: ruleSet returns [EObject current=null] : (otherlv_0= 'set' ( (lv_value_1_0= ruleValue ) ) otherlv_2= 'to' ( (lv_targetType_3_0= ruleTargetType ) ) ( (lv_condition_4_0= ruleCondition ) ) ) ;
    public final EObject ruleSet() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_value_1_0 = null;

        EObject lv_targetType_3_0 = null;

        EObject lv_condition_4_0 = null;



        	enterRule();

        try {
            // InternalSeleniumDSL.g:1186:2: ( (otherlv_0= 'set' ( (lv_value_1_0= ruleValue ) ) otherlv_2= 'to' ( (lv_targetType_3_0= ruleTargetType ) ) ( (lv_condition_4_0= ruleCondition ) ) ) )
            // InternalSeleniumDSL.g:1187:2: (otherlv_0= 'set' ( (lv_value_1_0= ruleValue ) ) otherlv_2= 'to' ( (lv_targetType_3_0= ruleTargetType ) ) ( (lv_condition_4_0= ruleCondition ) ) )
            {
            // InternalSeleniumDSL.g:1187:2: (otherlv_0= 'set' ( (lv_value_1_0= ruleValue ) ) otherlv_2= 'to' ( (lv_targetType_3_0= ruleTargetType ) ) ( (lv_condition_4_0= ruleCondition ) ) )
            // InternalSeleniumDSL.g:1188:3: otherlv_0= 'set' ( (lv_value_1_0= ruleValue ) ) otherlv_2= 'to' ( (lv_targetType_3_0= ruleTargetType ) ) ( (lv_condition_4_0= ruleCondition ) )
            {
            otherlv_0=(Token)match(input,40,FOLLOW_6); 

            			newLeafNode(otherlv_0, grammarAccess.getSetAccess().getSetKeyword_0());
            		
            // InternalSeleniumDSL.g:1192:3: ( (lv_value_1_0= ruleValue ) )
            // InternalSeleniumDSL.g:1193:4: (lv_value_1_0= ruleValue )
            {
            // InternalSeleniumDSL.g:1193:4: (lv_value_1_0= ruleValue )
            // InternalSeleniumDSL.g:1194:5: lv_value_1_0= ruleValue
            {

            					newCompositeNode(grammarAccess.getSetAccess().getValueValueParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_19);
            lv_value_1_0=ruleValue();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSetRule());
            					}
            					set(
            						current,
            						"value",
            						lv_value_1_0,
            						"org.xtext.selenium.SeleniumDSL.Value");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,41,FOLLOW_14); 

            			newLeafNode(otherlv_2, grammarAccess.getSetAccess().getToKeyword_2());
            		
            // InternalSeleniumDSL.g:1215:3: ( (lv_targetType_3_0= ruleTargetType ) )
            // InternalSeleniumDSL.g:1216:4: (lv_targetType_3_0= ruleTargetType )
            {
            // InternalSeleniumDSL.g:1216:4: (lv_targetType_3_0= ruleTargetType )
            // InternalSeleniumDSL.g:1217:5: lv_targetType_3_0= ruleTargetType
            {

            					newCompositeNode(grammarAccess.getSetAccess().getTargetTypeTargetTypeParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_13);
            lv_targetType_3_0=ruleTargetType();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSetRule());
            					}
            					set(
            						current,
            						"targetType",
            						lv_targetType_3_0,
            						"org.xtext.selenium.SeleniumDSL.TargetType");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalSeleniumDSL.g:1234:3: ( (lv_condition_4_0= ruleCondition ) )
            // InternalSeleniumDSL.g:1235:4: (lv_condition_4_0= ruleCondition )
            {
            // InternalSeleniumDSL.g:1235:4: (lv_condition_4_0= ruleCondition )
            // InternalSeleniumDSL.g:1236:5: lv_condition_4_0= ruleCondition
            {

            					newCompositeNode(grammarAccess.getSetAccess().getConditionConditionParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_2);
            lv_condition_4_0=ruleCondition();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSetRule());
            					}
            					set(
            						current,
            						"condition",
            						lv_condition_4_0,
            						"org.xtext.selenium.SeleniumDSL.Condition");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSet"


    // $ANTLR start "entryRuleIsElementPresent"
    // InternalSeleniumDSL.g:1257:1: entryRuleIsElementPresent returns [EObject current=null] : iv_ruleIsElementPresent= ruleIsElementPresent EOF ;
    public final EObject entryRuleIsElementPresent() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIsElementPresent = null;


        try {
            // InternalSeleniumDSL.g:1257:57: (iv_ruleIsElementPresent= ruleIsElementPresent EOF )
            // InternalSeleniumDSL.g:1258:2: iv_ruleIsElementPresent= ruleIsElementPresent EOF
            {
             newCompositeNode(grammarAccess.getIsElementPresentRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleIsElementPresent=ruleIsElementPresent();

            state._fsp--;

             current =iv_ruleIsElementPresent; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIsElementPresent"


    // $ANTLR start "ruleIsElementPresent"
    // InternalSeleniumDSL.g:1264:1: ruleIsElementPresent returns [EObject current=null] : (otherlv_0= 'isElementPresent' ( (lv_target_1_0= ruleTargetType ) ) ( (lv_condition_2_0= ruleCondition ) ) ) ;
    public final EObject ruleIsElementPresent() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_target_1_0 = null;

        EObject lv_condition_2_0 = null;



        	enterRule();

        try {
            // InternalSeleniumDSL.g:1270:2: ( (otherlv_0= 'isElementPresent' ( (lv_target_1_0= ruleTargetType ) ) ( (lv_condition_2_0= ruleCondition ) ) ) )
            // InternalSeleniumDSL.g:1271:2: (otherlv_0= 'isElementPresent' ( (lv_target_1_0= ruleTargetType ) ) ( (lv_condition_2_0= ruleCondition ) ) )
            {
            // InternalSeleniumDSL.g:1271:2: (otherlv_0= 'isElementPresent' ( (lv_target_1_0= ruleTargetType ) ) ( (lv_condition_2_0= ruleCondition ) ) )
            // InternalSeleniumDSL.g:1272:3: otherlv_0= 'isElementPresent' ( (lv_target_1_0= ruleTargetType ) ) ( (lv_condition_2_0= ruleCondition ) )
            {
            otherlv_0=(Token)match(input,42,FOLLOW_14); 

            			newLeafNode(otherlv_0, grammarAccess.getIsElementPresentAccess().getIsElementPresentKeyword_0());
            		
            // InternalSeleniumDSL.g:1276:3: ( (lv_target_1_0= ruleTargetType ) )
            // InternalSeleniumDSL.g:1277:4: (lv_target_1_0= ruleTargetType )
            {
            // InternalSeleniumDSL.g:1277:4: (lv_target_1_0= ruleTargetType )
            // InternalSeleniumDSL.g:1278:5: lv_target_1_0= ruleTargetType
            {

            					newCompositeNode(grammarAccess.getIsElementPresentAccess().getTargetTargetTypeParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_13);
            lv_target_1_0=ruleTargetType();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getIsElementPresentRule());
            					}
            					set(
            						current,
            						"target",
            						lv_target_1_0,
            						"org.xtext.selenium.SeleniumDSL.TargetType");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalSeleniumDSL.g:1295:3: ( (lv_condition_2_0= ruleCondition ) )
            // InternalSeleniumDSL.g:1296:4: (lv_condition_2_0= ruleCondition )
            {
            // InternalSeleniumDSL.g:1296:4: (lv_condition_2_0= ruleCondition )
            // InternalSeleniumDSL.g:1297:5: lv_condition_2_0= ruleCondition
            {

            					newCompositeNode(grammarAccess.getIsElementPresentAccess().getConditionConditionParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_condition_2_0=ruleCondition();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getIsElementPresentRule());
            					}
            					set(
            						current,
            						"condition",
            						lv_condition_2_0,
            						"org.xtext.selenium.SeleniumDSL.Condition");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIsElementPresent"


    // $ANTLR start "entryRuleCount"
    // InternalSeleniumDSL.g:1318:1: entryRuleCount returns [EObject current=null] : iv_ruleCount= ruleCount EOF ;
    public final EObject entryRuleCount() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCount = null;


        try {
            // InternalSeleniumDSL.g:1318:46: (iv_ruleCount= ruleCount EOF )
            // InternalSeleniumDSL.g:1319:2: iv_ruleCount= ruleCount EOF
            {
             newCompositeNode(grammarAccess.getCountRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCount=ruleCount();

            state._fsp--;

             current =iv_ruleCount; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCount"


    // $ANTLR start "ruleCount"
    // InternalSeleniumDSL.g:1325:1: ruleCount returns [EObject current=null] : (otherlv_0= 'count' ( (lv_targetType_1_0= ruleTargetType ) ) ( (lv_condition_2_0= ruleCondition ) ) otherlv_3= 'in' ( (lv_variableName_4_0= ruleVariableName ) ) ) ;
    public final EObject ruleCount() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_3=null;
        EObject lv_targetType_1_0 = null;

        EObject lv_condition_2_0 = null;

        EObject lv_variableName_4_0 = null;



        	enterRule();

        try {
            // InternalSeleniumDSL.g:1331:2: ( (otherlv_0= 'count' ( (lv_targetType_1_0= ruleTargetType ) ) ( (lv_condition_2_0= ruleCondition ) ) otherlv_3= 'in' ( (lv_variableName_4_0= ruleVariableName ) ) ) )
            // InternalSeleniumDSL.g:1332:2: (otherlv_0= 'count' ( (lv_targetType_1_0= ruleTargetType ) ) ( (lv_condition_2_0= ruleCondition ) ) otherlv_3= 'in' ( (lv_variableName_4_0= ruleVariableName ) ) )
            {
            // InternalSeleniumDSL.g:1332:2: (otherlv_0= 'count' ( (lv_targetType_1_0= ruleTargetType ) ) ( (lv_condition_2_0= ruleCondition ) ) otherlv_3= 'in' ( (lv_variableName_4_0= ruleVariableName ) ) )
            // InternalSeleniumDSL.g:1333:3: otherlv_0= 'count' ( (lv_targetType_1_0= ruleTargetType ) ) ( (lv_condition_2_0= ruleCondition ) ) otherlv_3= 'in' ( (lv_variableName_4_0= ruleVariableName ) )
            {
            otherlv_0=(Token)match(input,43,FOLLOW_14); 

            			newLeafNode(otherlv_0, grammarAccess.getCountAccess().getCountKeyword_0());
            		
            // InternalSeleniumDSL.g:1337:3: ( (lv_targetType_1_0= ruleTargetType ) )
            // InternalSeleniumDSL.g:1338:4: (lv_targetType_1_0= ruleTargetType )
            {
            // InternalSeleniumDSL.g:1338:4: (lv_targetType_1_0= ruleTargetType )
            // InternalSeleniumDSL.g:1339:5: lv_targetType_1_0= ruleTargetType
            {

            					newCompositeNode(grammarAccess.getCountAccess().getTargetTypeTargetTypeParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_13);
            lv_targetType_1_0=ruleTargetType();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCountRule());
            					}
            					set(
            						current,
            						"targetType",
            						lv_targetType_1_0,
            						"org.xtext.selenium.SeleniumDSL.TargetType");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalSeleniumDSL.g:1356:3: ( (lv_condition_2_0= ruleCondition ) )
            // InternalSeleniumDSL.g:1357:4: (lv_condition_2_0= ruleCondition )
            {
            // InternalSeleniumDSL.g:1357:4: (lv_condition_2_0= ruleCondition )
            // InternalSeleniumDSL.g:1358:5: lv_condition_2_0= ruleCondition
            {

            					newCompositeNode(grammarAccess.getCountAccess().getConditionConditionParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_11);
            lv_condition_2_0=ruleCondition();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCountRule());
            					}
            					set(
            						current,
            						"condition",
            						lv_condition_2_0,
            						"org.xtext.selenium.SeleniumDSL.Condition");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,32,FOLLOW_18); 

            			newLeafNode(otherlv_3, grammarAccess.getCountAccess().getInKeyword_3());
            		
            // InternalSeleniumDSL.g:1379:3: ( (lv_variableName_4_0= ruleVariableName ) )
            // InternalSeleniumDSL.g:1380:4: (lv_variableName_4_0= ruleVariableName )
            {
            // InternalSeleniumDSL.g:1380:4: (lv_variableName_4_0= ruleVariableName )
            // InternalSeleniumDSL.g:1381:5: lv_variableName_4_0= ruleVariableName
            {

            					newCompositeNode(grammarAccess.getCountAccess().getVariableNameVariableNameParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_2);
            lv_variableName_4_0=ruleVariableName();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCountRule());
            					}
            					set(
            						current,
            						"variableName",
            						lv_variableName_4_0,
            						"org.xtext.selenium.SeleniumDSL.VariableName");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCount"


    // $ANTLR start "entryRuleArgument"
    // InternalSeleniumDSL.g:1402:1: entryRuleArgument returns [EObject current=null] : iv_ruleArgument= ruleArgument EOF ;
    public final EObject entryRuleArgument() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleArgument = null;


        try {
            // InternalSeleniumDSL.g:1402:49: (iv_ruleArgument= ruleArgument EOF )
            // InternalSeleniumDSL.g:1403:2: iv_ruleArgument= ruleArgument EOF
            {
             newCompositeNode(grammarAccess.getArgumentRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleArgument=ruleArgument();

            state._fsp--;

             current =iv_ruleArgument; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleArgument"


    // $ANTLR start "ruleArgument"
    // InternalSeleniumDSL.g:1409:1: ruleArgument returns [EObject current=null] : ( (lv_name_0_0= RULE_ID ) ) ;
    public final EObject ruleArgument() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;


        	enterRule();

        try {
            // InternalSeleniumDSL.g:1415:2: ( ( (lv_name_0_0= RULE_ID ) ) )
            // InternalSeleniumDSL.g:1416:2: ( (lv_name_0_0= RULE_ID ) )
            {
            // InternalSeleniumDSL.g:1416:2: ( (lv_name_0_0= RULE_ID ) )
            // InternalSeleniumDSL.g:1417:3: (lv_name_0_0= RULE_ID )
            {
            // InternalSeleniumDSL.g:1417:3: (lv_name_0_0= RULE_ID )
            // InternalSeleniumDSL.g:1418:4: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            				newLeafNode(lv_name_0_0, grammarAccess.getArgumentAccess().getNameIDTerminalRuleCall_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getArgumentRule());
            				}
            				setWithLastConsumed(
            					current,
            					"name",
            					lv_name_0_0,
            					"org.eclipse.xtext.common.Terminals.ID");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleArgument"


    // $ANTLR start "entryRuleFunctionName"
    // InternalSeleniumDSL.g:1437:1: entryRuleFunctionName returns [EObject current=null] : iv_ruleFunctionName= ruleFunctionName EOF ;
    public final EObject entryRuleFunctionName() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFunctionName = null;


        try {
            // InternalSeleniumDSL.g:1437:53: (iv_ruleFunctionName= ruleFunctionName EOF )
            // InternalSeleniumDSL.g:1438:2: iv_ruleFunctionName= ruleFunctionName EOF
            {
             newCompositeNode(grammarAccess.getFunctionNameRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFunctionName=ruleFunctionName();

            state._fsp--;

             current =iv_ruleFunctionName; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFunctionName"


    // $ANTLR start "ruleFunctionName"
    // InternalSeleniumDSL.g:1444:1: ruleFunctionName returns [EObject current=null] : ( (lv_name_0_0= RULE_ID ) ) ;
    public final EObject ruleFunctionName() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;


        	enterRule();

        try {
            // InternalSeleniumDSL.g:1450:2: ( ( (lv_name_0_0= RULE_ID ) ) )
            // InternalSeleniumDSL.g:1451:2: ( (lv_name_0_0= RULE_ID ) )
            {
            // InternalSeleniumDSL.g:1451:2: ( (lv_name_0_0= RULE_ID ) )
            // InternalSeleniumDSL.g:1452:3: (lv_name_0_0= RULE_ID )
            {
            // InternalSeleniumDSL.g:1452:3: (lv_name_0_0= RULE_ID )
            // InternalSeleniumDSL.g:1453:4: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            				newLeafNode(lv_name_0_0, grammarAccess.getFunctionNameAccess().getNameIDTerminalRuleCall_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getFunctionNameRule());
            				}
            				setWithLastConsumed(
            					current,
            					"name",
            					lv_name_0_0,
            					"org.eclipse.xtext.common.Terminals.ID");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFunctionName"


    // $ANTLR start "entryRuleFunction"
    // InternalSeleniumDSL.g:1472:1: entryRuleFunction returns [EObject current=null] : iv_ruleFunction= ruleFunction EOF ;
    public final EObject entryRuleFunction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFunction = null;


        try {
            // InternalSeleniumDSL.g:1472:49: (iv_ruleFunction= ruleFunction EOF )
            // InternalSeleniumDSL.g:1473:2: iv_ruleFunction= ruleFunction EOF
            {
             newCompositeNode(grammarAccess.getFunctionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFunction=ruleFunction();

            state._fsp--;

             current =iv_ruleFunction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFunction"


    // $ANTLR start "ruleFunction"
    // InternalSeleniumDSL.g:1479:1: ruleFunction returns [EObject current=null] : (otherlv_0= 'define' ( (lv_functionName_1_0= ruleFunctionName ) ) otherlv_2= '(' ( (lv_arguments_3_0= ruleArgument ) )* otherlv_4= ')' ( (lv_instructions_5_0= ruleSeleniumInstruction ) )* otherlv_6= ';' ) ;
    public final EObject ruleFunction() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_functionName_1_0 = null;

        EObject lv_arguments_3_0 = null;

        EObject lv_instructions_5_0 = null;



        	enterRule();

        try {
            // InternalSeleniumDSL.g:1485:2: ( (otherlv_0= 'define' ( (lv_functionName_1_0= ruleFunctionName ) ) otherlv_2= '(' ( (lv_arguments_3_0= ruleArgument ) )* otherlv_4= ')' ( (lv_instructions_5_0= ruleSeleniumInstruction ) )* otherlv_6= ';' ) )
            // InternalSeleniumDSL.g:1486:2: (otherlv_0= 'define' ( (lv_functionName_1_0= ruleFunctionName ) ) otherlv_2= '(' ( (lv_arguments_3_0= ruleArgument ) )* otherlv_4= ')' ( (lv_instructions_5_0= ruleSeleniumInstruction ) )* otherlv_6= ';' )
            {
            // InternalSeleniumDSL.g:1486:2: (otherlv_0= 'define' ( (lv_functionName_1_0= ruleFunctionName ) ) otherlv_2= '(' ( (lv_arguments_3_0= ruleArgument ) )* otherlv_4= ')' ( (lv_instructions_5_0= ruleSeleniumInstruction ) )* otherlv_6= ';' )
            // InternalSeleniumDSL.g:1487:3: otherlv_0= 'define' ( (lv_functionName_1_0= ruleFunctionName ) ) otherlv_2= '(' ( (lv_arguments_3_0= ruleArgument ) )* otherlv_4= ')' ( (lv_instructions_5_0= ruleSeleniumInstruction ) )* otherlv_6= ';'
            {
            otherlv_0=(Token)match(input,44,FOLLOW_18); 

            			newLeafNode(otherlv_0, grammarAccess.getFunctionAccess().getDefineKeyword_0());
            		
            // InternalSeleniumDSL.g:1491:3: ( (lv_functionName_1_0= ruleFunctionName ) )
            // InternalSeleniumDSL.g:1492:4: (lv_functionName_1_0= ruleFunctionName )
            {
            // InternalSeleniumDSL.g:1492:4: (lv_functionName_1_0= ruleFunctionName )
            // InternalSeleniumDSL.g:1493:5: lv_functionName_1_0= ruleFunctionName
            {

            					newCompositeNode(grammarAccess.getFunctionAccess().getFunctionNameFunctionNameParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_20);
            lv_functionName_1_0=ruleFunctionName();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getFunctionRule());
            					}
            					set(
            						current,
            						"functionName",
            						lv_functionName_1_0,
            						"org.xtext.selenium.SeleniumDSL.FunctionName");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,45,FOLLOW_21); 

            			newLeafNode(otherlv_2, grammarAccess.getFunctionAccess().getLeftParenthesisKeyword_2());
            		
            // InternalSeleniumDSL.g:1514:3: ( (lv_arguments_3_0= ruleArgument ) )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==RULE_ID) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalSeleniumDSL.g:1515:4: (lv_arguments_3_0= ruleArgument )
            	    {
            	    // InternalSeleniumDSL.g:1515:4: (lv_arguments_3_0= ruleArgument )
            	    // InternalSeleniumDSL.g:1516:5: lv_arguments_3_0= ruleArgument
            	    {

            	    					newCompositeNode(grammarAccess.getFunctionAccess().getArgumentsArgumentParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_21);
            	    lv_arguments_3_0=ruleArgument();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getFunctionRule());
            	    					}
            	    					add(
            	    						current,
            	    						"arguments",
            	    						lv_arguments_3_0,
            	    						"org.xtext.selenium.SeleniumDSL.Argument");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

            otherlv_4=(Token)match(input,46,FOLLOW_22); 

            			newLeafNode(otherlv_4, grammarAccess.getFunctionAccess().getRightParenthesisKeyword_4());
            		
            // InternalSeleniumDSL.g:1537:3: ( (lv_instructions_5_0= ruleSeleniumInstruction ) )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==RULE_ID||LA16_0==16||LA16_0==30||(LA16_0>=34 && LA16_0<=35)||(LA16_0>=37 && LA16_0<=38)||LA16_0==40||(LA16_0>=42 && LA16_0<=43)||LA16_0==48) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalSeleniumDSL.g:1538:4: (lv_instructions_5_0= ruleSeleniumInstruction )
            	    {
            	    // InternalSeleniumDSL.g:1538:4: (lv_instructions_5_0= ruleSeleniumInstruction )
            	    // InternalSeleniumDSL.g:1539:5: lv_instructions_5_0= ruleSeleniumInstruction
            	    {

            	    					newCompositeNode(grammarAccess.getFunctionAccess().getInstructionsSeleniumInstructionParserRuleCall_5_0());
            	    				
            	    pushFollow(FOLLOW_22);
            	    lv_instructions_5_0=ruleSeleniumInstruction();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getFunctionRule());
            	    					}
            	    					add(
            	    						current,
            	    						"instructions",
            	    						lv_instructions_5_0,
            	    						"org.xtext.selenium.SeleniumDSL.SeleniumInstruction");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);

            otherlv_6=(Token)match(input,47,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getFunctionAccess().getSemicolonKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFunction"


    // $ANTLR start "entryRuleFunctionCall"
    // InternalSeleniumDSL.g:1564:1: entryRuleFunctionCall returns [EObject current=null] : iv_ruleFunctionCall= ruleFunctionCall EOF ;
    public final EObject entryRuleFunctionCall() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFunctionCall = null;


        try {
            // InternalSeleniumDSL.g:1564:53: (iv_ruleFunctionCall= ruleFunctionCall EOF )
            // InternalSeleniumDSL.g:1565:2: iv_ruleFunctionCall= ruleFunctionCall EOF
            {
             newCompositeNode(grammarAccess.getFunctionCallRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFunctionCall=ruleFunctionCall();

            state._fsp--;

             current =iv_ruleFunctionCall; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFunctionCall"


    // $ANTLR start "ruleFunctionCall"
    // InternalSeleniumDSL.g:1571:1: ruleFunctionCall returns [EObject current=null] : ( ( (lv_functionName_0_0= ruleFunctionName ) ) otherlv_1= '(' ( (lv_values_2_0= ruleValue ) )* otherlv_3= ')' ) ;
    public final EObject ruleFunctionCall() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_functionName_0_0 = null;

        EObject lv_values_2_0 = null;



        	enterRule();

        try {
            // InternalSeleniumDSL.g:1577:2: ( ( ( (lv_functionName_0_0= ruleFunctionName ) ) otherlv_1= '(' ( (lv_values_2_0= ruleValue ) )* otherlv_3= ')' ) )
            // InternalSeleniumDSL.g:1578:2: ( ( (lv_functionName_0_0= ruleFunctionName ) ) otherlv_1= '(' ( (lv_values_2_0= ruleValue ) )* otherlv_3= ')' )
            {
            // InternalSeleniumDSL.g:1578:2: ( ( (lv_functionName_0_0= ruleFunctionName ) ) otherlv_1= '(' ( (lv_values_2_0= ruleValue ) )* otherlv_3= ')' )
            // InternalSeleniumDSL.g:1579:3: ( (lv_functionName_0_0= ruleFunctionName ) ) otherlv_1= '(' ( (lv_values_2_0= ruleValue ) )* otherlv_3= ')'
            {
            // InternalSeleniumDSL.g:1579:3: ( (lv_functionName_0_0= ruleFunctionName ) )
            // InternalSeleniumDSL.g:1580:4: (lv_functionName_0_0= ruleFunctionName )
            {
            // InternalSeleniumDSL.g:1580:4: (lv_functionName_0_0= ruleFunctionName )
            // InternalSeleniumDSL.g:1581:5: lv_functionName_0_0= ruleFunctionName
            {

            					newCompositeNode(grammarAccess.getFunctionCallAccess().getFunctionNameFunctionNameParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_20);
            lv_functionName_0_0=ruleFunctionName();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getFunctionCallRule());
            					}
            					set(
            						current,
            						"functionName",
            						lv_functionName_0_0,
            						"org.xtext.selenium.SeleniumDSL.FunctionName");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_1=(Token)match(input,45,FOLLOW_23); 

            			newLeafNode(otherlv_1, grammarAccess.getFunctionCallAccess().getLeftParenthesisKeyword_1());
            		
            // InternalSeleniumDSL.g:1602:3: ( (lv_values_2_0= ruleValue ) )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( ((LA17_0>=RULE_STRING && LA17_0<=RULE_ID)) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // InternalSeleniumDSL.g:1603:4: (lv_values_2_0= ruleValue )
            	    {
            	    // InternalSeleniumDSL.g:1603:4: (lv_values_2_0= ruleValue )
            	    // InternalSeleniumDSL.g:1604:5: lv_values_2_0= ruleValue
            	    {

            	    					newCompositeNode(grammarAccess.getFunctionCallAccess().getValuesValueParserRuleCall_2_0());
            	    				
            	    pushFollow(FOLLOW_23);
            	    lv_values_2_0=ruleValue();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getFunctionCallRule());
            	    					}
            	    					add(
            	    						current,
            	    						"values",
            	    						lv_values_2_0,
            	    						"org.xtext.selenium.SeleniumDSL.Value");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);

            otherlv_3=(Token)match(input,46,FOLLOW_2); 

            			newLeafNode(otherlv_3, grammarAccess.getFunctionCallAccess().getRightParenthesisKeyword_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFunctionCall"


    // $ANTLR start "entryRuleAssert"
    // InternalSeleniumDSL.g:1629:1: entryRuleAssert returns [EObject current=null] : iv_ruleAssert= ruleAssert EOF ;
    public final EObject entryRuleAssert() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssert = null;


        try {
            // InternalSeleniumDSL.g:1629:47: (iv_ruleAssert= ruleAssert EOF )
            // InternalSeleniumDSL.g:1630:2: iv_ruleAssert= ruleAssert EOF
            {
             newCompositeNode(grammarAccess.getAssertRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAssert=ruleAssert();

            state._fsp--;

             current =iv_ruleAssert; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssert"


    // $ANTLR start "ruleAssert"
    // InternalSeleniumDSL.g:1636:1: ruleAssert returns [EObject current=null] : (otherlv_0= 'assert' ( (lv_value1_1_0= ruleValue ) ) ( ( (lv_negate_2_0= 'not' ) )? otherlv_3= 'equals' ) ( (lv_value2_4_0= ruleValue ) ) ) ;
    public final EObject ruleAssert() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_negate_2_0=null;
        Token otherlv_3=null;
        EObject lv_value1_1_0 = null;

        EObject lv_value2_4_0 = null;



        	enterRule();

        try {
            // InternalSeleniumDSL.g:1642:2: ( (otherlv_0= 'assert' ( (lv_value1_1_0= ruleValue ) ) ( ( (lv_negate_2_0= 'not' ) )? otherlv_3= 'equals' ) ( (lv_value2_4_0= ruleValue ) ) ) )
            // InternalSeleniumDSL.g:1643:2: (otherlv_0= 'assert' ( (lv_value1_1_0= ruleValue ) ) ( ( (lv_negate_2_0= 'not' ) )? otherlv_3= 'equals' ) ( (lv_value2_4_0= ruleValue ) ) )
            {
            // InternalSeleniumDSL.g:1643:2: (otherlv_0= 'assert' ( (lv_value1_1_0= ruleValue ) ) ( ( (lv_negate_2_0= 'not' ) )? otherlv_3= 'equals' ) ( (lv_value2_4_0= ruleValue ) ) )
            // InternalSeleniumDSL.g:1644:3: otherlv_0= 'assert' ( (lv_value1_1_0= ruleValue ) ) ( ( (lv_negate_2_0= 'not' ) )? otherlv_3= 'equals' ) ( (lv_value2_4_0= ruleValue ) )
            {
            otherlv_0=(Token)match(input,48,FOLLOW_6); 

            			newLeafNode(otherlv_0, grammarAccess.getAssertAccess().getAssertKeyword_0());
            		
            // InternalSeleniumDSL.g:1648:3: ( (lv_value1_1_0= ruleValue ) )
            // InternalSeleniumDSL.g:1649:4: (lv_value1_1_0= ruleValue )
            {
            // InternalSeleniumDSL.g:1649:4: (lv_value1_1_0= ruleValue )
            // InternalSeleniumDSL.g:1650:5: lv_value1_1_0= ruleValue
            {

            					newCompositeNode(grammarAccess.getAssertAccess().getValue1ValueParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_24);
            lv_value1_1_0=ruleValue();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAssertRule());
            					}
            					set(
            						current,
            						"value1",
            						lv_value1_1_0,
            						"org.xtext.selenium.SeleniumDSL.Value");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalSeleniumDSL.g:1667:3: ( ( (lv_negate_2_0= 'not' ) )? otherlv_3= 'equals' )
            // InternalSeleniumDSL.g:1668:4: ( (lv_negate_2_0= 'not' ) )? otherlv_3= 'equals'
            {
            // InternalSeleniumDSL.g:1668:4: ( (lv_negate_2_0= 'not' ) )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==49) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalSeleniumDSL.g:1669:5: (lv_negate_2_0= 'not' )
                    {
                    // InternalSeleniumDSL.g:1669:5: (lv_negate_2_0= 'not' )
                    // InternalSeleniumDSL.g:1670:6: lv_negate_2_0= 'not'
                    {
                    lv_negate_2_0=(Token)match(input,49,FOLLOW_25); 

                    						newLeafNode(lv_negate_2_0, grammarAccess.getAssertAccess().getNegateNotKeyword_2_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAssertRule());
                    						}
                    						setWithLastConsumed(current, "negate", lv_negate_2_0, "not");
                    					

                    }


                    }
                    break;

            }

            otherlv_3=(Token)match(input,28,FOLLOW_6); 

            				newLeafNode(otherlv_3, grammarAccess.getAssertAccess().getEqualsKeyword_2_1());
            			

            }

            // InternalSeleniumDSL.g:1687:3: ( (lv_value2_4_0= ruleValue ) )
            // InternalSeleniumDSL.g:1688:4: (lv_value2_4_0= ruleValue )
            {
            // InternalSeleniumDSL.g:1688:4: (lv_value2_4_0= ruleValue )
            // InternalSeleniumDSL.g:1689:5: lv_value2_4_0= ruleValue
            {

            					newCompositeNode(grammarAccess.getAssertAccess().getValue2ValueParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_2);
            lv_value2_4_0=ruleValue();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAssertRule());
            					}
            					set(
            						current,
            						"value2",
            						lv_value2_4_0,
            						"org.xtext.selenium.SeleniumDSL.Value");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssert"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000100000004000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x00010D6C40018020L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000003800L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000002000010L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000018000000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000022000000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000001001FE0000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000004000002L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000002000030L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000400000000020L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x00018D6C40010020L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000400000000030L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0002000010000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000010000000L});

}