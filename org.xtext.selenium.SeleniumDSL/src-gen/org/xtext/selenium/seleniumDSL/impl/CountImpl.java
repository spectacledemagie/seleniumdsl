/**
 * generated by Xtext 2.12.0
 */
package org.xtext.selenium.seleniumDSL.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.xtext.selenium.seleniumDSL.Condition;
import org.xtext.selenium.seleniumDSL.Count;
import org.xtext.selenium.seleniumDSL.SeleniumDSLPackage;
import org.xtext.selenium.seleniumDSL.TargetType;
import org.xtext.selenium.seleniumDSL.VariableName;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Count</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.selenium.seleniumDSL.impl.CountImpl#getTargetType <em>Target Type</em>}</li>
 *   <li>{@link org.xtext.selenium.seleniumDSL.impl.CountImpl#getCondition <em>Condition</em>}</li>
 *   <li>{@link org.xtext.selenium.seleniumDSL.impl.CountImpl#getVariableName <em>Variable Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CountImpl extends SeleniumInstructionImpl implements Count
{
  /**
   * The cached value of the '{@link #getTargetType() <em>Target Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTargetType()
   * @generated
   * @ordered
   */
  protected TargetType targetType;

  /**
   * The cached value of the '{@link #getCondition() <em>Condition</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCondition()
   * @generated
   * @ordered
   */
  protected Condition condition;

  /**
   * The cached value of the '{@link #getVariableName() <em>Variable Name</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVariableName()
   * @generated
   * @ordered
   */
  protected VariableName variableName;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected CountImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SeleniumDSLPackage.Literals.COUNT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TargetType getTargetType()
  {
    return targetType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTargetType(TargetType newTargetType, NotificationChain msgs)
  {
    TargetType oldTargetType = targetType;
    targetType = newTargetType;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SeleniumDSLPackage.COUNT__TARGET_TYPE, oldTargetType, newTargetType);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTargetType(TargetType newTargetType)
  {
    if (newTargetType != targetType)
    {
      NotificationChain msgs = null;
      if (targetType != null)
        msgs = ((InternalEObject)targetType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SeleniumDSLPackage.COUNT__TARGET_TYPE, null, msgs);
      if (newTargetType != null)
        msgs = ((InternalEObject)newTargetType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SeleniumDSLPackage.COUNT__TARGET_TYPE, null, msgs);
      msgs = basicSetTargetType(newTargetType, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SeleniumDSLPackage.COUNT__TARGET_TYPE, newTargetType, newTargetType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Condition getCondition()
  {
    return condition;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetCondition(Condition newCondition, NotificationChain msgs)
  {
    Condition oldCondition = condition;
    condition = newCondition;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SeleniumDSLPackage.COUNT__CONDITION, oldCondition, newCondition);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCondition(Condition newCondition)
  {
    if (newCondition != condition)
    {
      NotificationChain msgs = null;
      if (condition != null)
        msgs = ((InternalEObject)condition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SeleniumDSLPackage.COUNT__CONDITION, null, msgs);
      if (newCondition != null)
        msgs = ((InternalEObject)newCondition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SeleniumDSLPackage.COUNT__CONDITION, null, msgs);
      msgs = basicSetCondition(newCondition, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SeleniumDSLPackage.COUNT__CONDITION, newCondition, newCondition));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VariableName getVariableName()
  {
    return variableName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetVariableName(VariableName newVariableName, NotificationChain msgs)
  {
    VariableName oldVariableName = variableName;
    variableName = newVariableName;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SeleniumDSLPackage.COUNT__VARIABLE_NAME, oldVariableName, newVariableName);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVariableName(VariableName newVariableName)
  {
    if (newVariableName != variableName)
    {
      NotificationChain msgs = null;
      if (variableName != null)
        msgs = ((InternalEObject)variableName).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SeleniumDSLPackage.COUNT__VARIABLE_NAME, null, msgs);
      if (newVariableName != null)
        msgs = ((InternalEObject)newVariableName).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SeleniumDSLPackage.COUNT__VARIABLE_NAME, null, msgs);
      msgs = basicSetVariableName(newVariableName, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SeleniumDSLPackage.COUNT__VARIABLE_NAME, newVariableName, newVariableName));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case SeleniumDSLPackage.COUNT__TARGET_TYPE:
        return basicSetTargetType(null, msgs);
      case SeleniumDSLPackage.COUNT__CONDITION:
        return basicSetCondition(null, msgs);
      case SeleniumDSLPackage.COUNT__VARIABLE_NAME:
        return basicSetVariableName(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case SeleniumDSLPackage.COUNT__TARGET_TYPE:
        return getTargetType();
      case SeleniumDSLPackage.COUNT__CONDITION:
        return getCondition();
      case SeleniumDSLPackage.COUNT__VARIABLE_NAME:
        return getVariableName();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case SeleniumDSLPackage.COUNT__TARGET_TYPE:
        setTargetType((TargetType)newValue);
        return;
      case SeleniumDSLPackage.COUNT__CONDITION:
        setCondition((Condition)newValue);
        return;
      case SeleniumDSLPackage.COUNT__VARIABLE_NAME:
        setVariableName((VariableName)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case SeleniumDSLPackage.COUNT__TARGET_TYPE:
        setTargetType((TargetType)null);
        return;
      case SeleniumDSLPackage.COUNT__CONDITION:
        setCondition((Condition)null);
        return;
      case SeleniumDSLPackage.COUNT__VARIABLE_NAME:
        setVariableName((VariableName)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case SeleniumDSLPackage.COUNT__TARGET_TYPE:
        return targetType != null;
      case SeleniumDSLPackage.COUNT__CONDITION:
        return condition != null;
      case SeleniumDSLPackage.COUNT__VARIABLE_NAME:
        return variableName != null;
    }
    return super.eIsSet(featureID);
  }

} //CountImpl
