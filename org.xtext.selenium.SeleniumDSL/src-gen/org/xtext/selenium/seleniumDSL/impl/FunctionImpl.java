/**
 * generated by Xtext 2.12.0
 */
package org.xtext.selenium.seleniumDSL.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.xtext.selenium.seleniumDSL.Argument;
import org.xtext.selenium.seleniumDSL.Function;
import org.xtext.selenium.seleniumDSL.FunctionName;
import org.xtext.selenium.seleniumDSL.SeleniumDSLPackage;
import org.xtext.selenium.seleniumDSL.SeleniumInstruction;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Function</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.selenium.seleniumDSL.impl.FunctionImpl#getFunctionName <em>Function Name</em>}</li>
 *   <li>{@link org.xtext.selenium.seleniumDSL.impl.FunctionImpl#getArguments <em>Arguments</em>}</li>
 *   <li>{@link org.xtext.selenium.seleniumDSL.impl.FunctionImpl#getInstructions <em>Instructions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FunctionImpl extends MinimalEObjectImpl.Container implements Function
{
  /**
   * The cached value of the '{@link #getFunctionName() <em>Function Name</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFunctionName()
   * @generated
   * @ordered
   */
  protected FunctionName functionName;

  /**
   * The cached value of the '{@link #getArguments() <em>Arguments</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getArguments()
   * @generated
   * @ordered
   */
  protected EList<Argument> arguments;

  /**
   * The cached value of the '{@link #getInstructions() <em>Instructions</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getInstructions()
   * @generated
   * @ordered
   */
  protected EList<SeleniumInstruction> instructions;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected FunctionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SeleniumDSLPackage.Literals.FUNCTION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FunctionName getFunctionName()
  {
    return functionName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetFunctionName(FunctionName newFunctionName, NotificationChain msgs)
  {
    FunctionName oldFunctionName = functionName;
    functionName = newFunctionName;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SeleniumDSLPackage.FUNCTION__FUNCTION_NAME, oldFunctionName, newFunctionName);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFunctionName(FunctionName newFunctionName)
  {
    if (newFunctionName != functionName)
    {
      NotificationChain msgs = null;
      if (functionName != null)
        msgs = ((InternalEObject)functionName).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SeleniumDSLPackage.FUNCTION__FUNCTION_NAME, null, msgs);
      if (newFunctionName != null)
        msgs = ((InternalEObject)newFunctionName).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SeleniumDSLPackage.FUNCTION__FUNCTION_NAME, null, msgs);
      msgs = basicSetFunctionName(newFunctionName, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SeleniumDSLPackage.FUNCTION__FUNCTION_NAME, newFunctionName, newFunctionName));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Argument> getArguments()
  {
    if (arguments == null)
    {
      arguments = new EObjectContainmentEList<Argument>(Argument.class, this, SeleniumDSLPackage.FUNCTION__ARGUMENTS);
    }
    return arguments;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<SeleniumInstruction> getInstructions()
  {
    if (instructions == null)
    {
      instructions = new EObjectContainmentEList<SeleniumInstruction>(SeleniumInstruction.class, this, SeleniumDSLPackage.FUNCTION__INSTRUCTIONS);
    }
    return instructions;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case SeleniumDSLPackage.FUNCTION__FUNCTION_NAME:
        return basicSetFunctionName(null, msgs);
      case SeleniumDSLPackage.FUNCTION__ARGUMENTS:
        return ((InternalEList<?>)getArguments()).basicRemove(otherEnd, msgs);
      case SeleniumDSLPackage.FUNCTION__INSTRUCTIONS:
        return ((InternalEList<?>)getInstructions()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case SeleniumDSLPackage.FUNCTION__FUNCTION_NAME:
        return getFunctionName();
      case SeleniumDSLPackage.FUNCTION__ARGUMENTS:
        return getArguments();
      case SeleniumDSLPackage.FUNCTION__INSTRUCTIONS:
        return getInstructions();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case SeleniumDSLPackage.FUNCTION__FUNCTION_NAME:
        setFunctionName((FunctionName)newValue);
        return;
      case SeleniumDSLPackage.FUNCTION__ARGUMENTS:
        getArguments().clear();
        getArguments().addAll((Collection<? extends Argument>)newValue);
        return;
      case SeleniumDSLPackage.FUNCTION__INSTRUCTIONS:
        getInstructions().clear();
        getInstructions().addAll((Collection<? extends SeleniumInstruction>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case SeleniumDSLPackage.FUNCTION__FUNCTION_NAME:
        setFunctionName((FunctionName)null);
        return;
      case SeleniumDSLPackage.FUNCTION__ARGUMENTS:
        getArguments().clear();
        return;
      case SeleniumDSLPackage.FUNCTION__INSTRUCTIONS:
        getInstructions().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case SeleniumDSLPackage.FUNCTION__FUNCTION_NAME:
        return functionName != null;
      case SeleniumDSLPackage.FUNCTION__ARGUMENTS:
        return arguments != null && !arguments.isEmpty();
      case SeleniumDSLPackage.FUNCTION__INSTRUCTIONS:
        return instructions != null && !instructions.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //FunctionImpl
