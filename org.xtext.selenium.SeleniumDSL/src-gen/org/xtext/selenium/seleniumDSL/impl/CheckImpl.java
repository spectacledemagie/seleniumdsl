/**
 * generated by Xtext 2.12.0
 */
package org.xtext.selenium.seleniumDSL.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.xtext.selenium.seleniumDSL.Check;
import org.xtext.selenium.seleniumDSL.Condition;
import org.xtext.selenium.seleniumDSL.SeleniumDSLPackage;
import org.xtext.selenium.seleniumDSL.TargetType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Check</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.selenium.seleniumDSL.impl.CheckImpl#getToCheck <em>To Check</em>}</li>
 *   <li>{@link org.xtext.selenium.seleniumDSL.impl.CheckImpl#getAll <em>All</em>}</li>
 *   <li>{@link org.xtext.selenium.seleniumDSL.impl.CheckImpl#getTargetType <em>Target Type</em>}</li>
 *   <li>{@link org.xtext.selenium.seleniumDSL.impl.CheckImpl#getCondition <em>Condition</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CheckImpl extends SeleniumInstructionImpl implements Check
{
  /**
   * The default value of the '{@link #getToCheck() <em>To Check</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getToCheck()
   * @generated
   * @ordered
   */
  protected static final String TO_CHECK_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getToCheck() <em>To Check</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getToCheck()
   * @generated
   * @ordered
   */
  protected String toCheck = TO_CHECK_EDEFAULT;

  /**
   * The default value of the '{@link #getAll() <em>All</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAll()
   * @generated
   * @ordered
   */
  protected static final String ALL_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getAll() <em>All</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAll()
   * @generated
   * @ordered
   */
  protected String all = ALL_EDEFAULT;

  /**
   * The cached value of the '{@link #getTargetType() <em>Target Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTargetType()
   * @generated
   * @ordered
   */
  protected TargetType targetType;

  /**
   * The cached value of the '{@link #getCondition() <em>Condition</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCondition()
   * @generated
   * @ordered
   */
  protected Condition condition;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected CheckImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SeleniumDSLPackage.Literals.CHECK;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getToCheck()
  {
    return toCheck;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setToCheck(String newToCheck)
  {
    String oldToCheck = toCheck;
    toCheck = newToCheck;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SeleniumDSLPackage.CHECK__TO_CHECK, oldToCheck, toCheck));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getAll()
  {
    return all;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAll(String newAll)
  {
    String oldAll = all;
    all = newAll;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SeleniumDSLPackage.CHECK__ALL, oldAll, all));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TargetType getTargetType()
  {
    return targetType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTargetType(TargetType newTargetType, NotificationChain msgs)
  {
    TargetType oldTargetType = targetType;
    targetType = newTargetType;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SeleniumDSLPackage.CHECK__TARGET_TYPE, oldTargetType, newTargetType);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTargetType(TargetType newTargetType)
  {
    if (newTargetType != targetType)
    {
      NotificationChain msgs = null;
      if (targetType != null)
        msgs = ((InternalEObject)targetType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SeleniumDSLPackage.CHECK__TARGET_TYPE, null, msgs);
      if (newTargetType != null)
        msgs = ((InternalEObject)newTargetType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SeleniumDSLPackage.CHECK__TARGET_TYPE, null, msgs);
      msgs = basicSetTargetType(newTargetType, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SeleniumDSLPackage.CHECK__TARGET_TYPE, newTargetType, newTargetType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Condition getCondition()
  {
    return condition;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetCondition(Condition newCondition, NotificationChain msgs)
  {
    Condition oldCondition = condition;
    condition = newCondition;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SeleniumDSLPackage.CHECK__CONDITION, oldCondition, newCondition);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCondition(Condition newCondition)
  {
    if (newCondition != condition)
    {
      NotificationChain msgs = null;
      if (condition != null)
        msgs = ((InternalEObject)condition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SeleniumDSLPackage.CHECK__CONDITION, null, msgs);
      if (newCondition != null)
        msgs = ((InternalEObject)newCondition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SeleniumDSLPackage.CHECK__CONDITION, null, msgs);
      msgs = basicSetCondition(newCondition, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SeleniumDSLPackage.CHECK__CONDITION, newCondition, newCondition));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case SeleniumDSLPackage.CHECK__TARGET_TYPE:
        return basicSetTargetType(null, msgs);
      case SeleniumDSLPackage.CHECK__CONDITION:
        return basicSetCondition(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case SeleniumDSLPackage.CHECK__TO_CHECK:
        return getToCheck();
      case SeleniumDSLPackage.CHECK__ALL:
        return getAll();
      case SeleniumDSLPackage.CHECK__TARGET_TYPE:
        return getTargetType();
      case SeleniumDSLPackage.CHECK__CONDITION:
        return getCondition();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case SeleniumDSLPackage.CHECK__TO_CHECK:
        setToCheck((String)newValue);
        return;
      case SeleniumDSLPackage.CHECK__ALL:
        setAll((String)newValue);
        return;
      case SeleniumDSLPackage.CHECK__TARGET_TYPE:
        setTargetType((TargetType)newValue);
        return;
      case SeleniumDSLPackage.CHECK__CONDITION:
        setCondition((Condition)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case SeleniumDSLPackage.CHECK__TO_CHECK:
        setToCheck(TO_CHECK_EDEFAULT);
        return;
      case SeleniumDSLPackage.CHECK__ALL:
        setAll(ALL_EDEFAULT);
        return;
      case SeleniumDSLPackage.CHECK__TARGET_TYPE:
        setTargetType((TargetType)null);
        return;
      case SeleniumDSLPackage.CHECK__CONDITION:
        setCondition((Condition)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case SeleniumDSLPackage.CHECK__TO_CHECK:
        return TO_CHECK_EDEFAULT == null ? toCheck != null : !TO_CHECK_EDEFAULT.equals(toCheck);
      case SeleniumDSLPackage.CHECK__ALL:
        return ALL_EDEFAULT == null ? all != null : !ALL_EDEFAULT.equals(all);
      case SeleniumDSLPackage.CHECK__TARGET_TYPE:
        return targetType != null;
      case SeleniumDSLPackage.CHECK__CONDITION:
        return condition != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (toCheck: ");
    result.append(toCheck);
    result.append(", all: ");
    result.append(all);
    result.append(')');
    return result.toString();
  }

} //CheckImpl
