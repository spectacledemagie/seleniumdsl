/**
 * generated by Xtext 2.12.0
 */
package org.xtext.selenium.seleniumDSL;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Count</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.selenium.seleniumDSL.Count#getTargetType <em>Target Type</em>}</li>
 *   <li>{@link org.xtext.selenium.seleniumDSL.Count#getCondition <em>Condition</em>}</li>
 *   <li>{@link org.xtext.selenium.seleniumDSL.Count#getVariableName <em>Variable Name</em>}</li>
 * </ul>
 *
 * @see org.xtext.selenium.seleniumDSL.SeleniumDSLPackage#getCount()
 * @model
 * @generated
 */
public interface Count extends SeleniumInstruction
{
  /**
   * Returns the value of the '<em><b>Target Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Target Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Target Type</em>' containment reference.
   * @see #setTargetType(TargetType)
   * @see org.xtext.selenium.seleniumDSL.SeleniumDSLPackage#getCount_TargetType()
   * @model containment="true"
   * @generated
   */
  TargetType getTargetType();

  /**
   * Sets the value of the '{@link org.xtext.selenium.seleniumDSL.Count#getTargetType <em>Target Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Target Type</em>' containment reference.
   * @see #getTargetType()
   * @generated
   */
  void setTargetType(TargetType value);

  /**
   * Returns the value of the '<em><b>Condition</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Condition</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Condition</em>' containment reference.
   * @see #setCondition(Condition)
   * @see org.xtext.selenium.seleniumDSL.SeleniumDSLPackage#getCount_Condition()
   * @model containment="true"
   * @generated
   */
  Condition getCondition();

  /**
   * Sets the value of the '{@link org.xtext.selenium.seleniumDSL.Count#getCondition <em>Condition</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Condition</em>' containment reference.
   * @see #getCondition()
   * @generated
   */
  void setCondition(Condition value);

  /**
   * Returns the value of the '<em><b>Variable Name</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Variable Name</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Variable Name</em>' containment reference.
   * @see #setVariableName(VariableName)
   * @see org.xtext.selenium.seleniumDSL.SeleniumDSLPackage#getCount_VariableName()
   * @model containment="true"
   * @generated
   */
  VariableName getVariableName();

  /**
   * Sets the value of the '{@link org.xtext.selenium.seleniumDSL.Count#getVariableName <em>Variable Name</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Variable Name</em>' containment reference.
   * @see #getVariableName()
   * @generated
   */
  void setVariableName(VariableName value);

} // Count
