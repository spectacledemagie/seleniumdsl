/**
 * generated by Xtext 2.12.0
 */
package org.xtext.selenium.seleniumDSL;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Target Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.selenium.seleniumDSL.TargetType#getTargetType <em>Target Type</em>}</li>
 * </ul>
 *
 * @see org.xtext.selenium.seleniumDSL.SeleniumDSLPackage#getTargetType()
 * @model
 * @generated
 */
public interface TargetType extends EObject
{
  /**
   * Returns the value of the '<em><b>Target Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Target Type</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Target Type</em>' attribute.
   * @see #setTargetType(String)
   * @see org.xtext.selenium.seleniumDSL.SeleniumDSLPackage#getTargetType_TargetType()
   * @model
   * @generated
   */
  String getTargetType();

  /**
   * Sets the value of the '{@link org.xtext.selenium.seleniumDSL.TargetType#getTargetType <em>Target Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Target Type</em>' attribute.
   * @see #getTargetType()
   * @generated
   */
  void setTargetType(String value);

} // TargetType
