/**
 * generated by Xtext 2.12.0
 */
package org.xtext.selenium.seleniumDSL;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Set</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.selenium.seleniumDSL.Set#getValue <em>Value</em>}</li>
 *   <li>{@link org.xtext.selenium.seleniumDSL.Set#getTargetType <em>Target Type</em>}</li>
 *   <li>{@link org.xtext.selenium.seleniumDSL.Set#getCondition <em>Condition</em>}</li>
 * </ul>
 *
 * @see org.xtext.selenium.seleniumDSL.SeleniumDSLPackage#getSet()
 * @model
 * @generated
 */
public interface Set extends SeleniumInstruction
{
  /**
   * Returns the value of the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' containment reference.
   * @see #setValue(Value)
   * @see org.xtext.selenium.seleniumDSL.SeleniumDSLPackage#getSet_Value()
   * @model containment="true"
   * @generated
   */
  Value getValue();

  /**
   * Sets the value of the '{@link org.xtext.selenium.seleniumDSL.Set#getValue <em>Value</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' containment reference.
   * @see #getValue()
   * @generated
   */
  void setValue(Value value);

  /**
   * Returns the value of the '<em><b>Target Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Target Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Target Type</em>' containment reference.
   * @see #setTargetType(TargetType)
   * @see org.xtext.selenium.seleniumDSL.SeleniumDSLPackage#getSet_TargetType()
   * @model containment="true"
   * @generated
   */
  TargetType getTargetType();

  /**
   * Sets the value of the '{@link org.xtext.selenium.seleniumDSL.Set#getTargetType <em>Target Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Target Type</em>' containment reference.
   * @see #getTargetType()
   * @generated
   */
  void setTargetType(TargetType value);

  /**
   * Returns the value of the '<em><b>Condition</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Condition</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Condition</em>' containment reference.
   * @see #setCondition(Condition)
   * @see org.xtext.selenium.seleniumDSL.SeleniumDSLPackage#getSet_Condition()
   * @model containment="true"
   * @generated
   */
  Condition getCondition();

  /**
   * Sets the value of the '{@link org.xtext.selenium.seleniumDSL.Set#getCondition <em>Condition</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Condition</em>' containment reference.
   * @see #getCondition()
   * @generated
   */
  void setCondition(Condition value);

} // Set
